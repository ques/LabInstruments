# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 14:13:35 2021

@author: FTNK-LocalAdm
"""
import os,sys
sys.path.append(os.path.dirname(__file__))

import numpy as np 
from VisaInstrument import VisaInstrument

class Cobrite_DX4(VisaInstrument):
    def __init__(self,addr,name='Cobrite_DX4'):
        super().__init__(addr)