# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 12:09:49 2021

@author: FTNK-LocalAdm
"""
import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument


class SantecLaser(VisaInstrument):
    def __init__(self,addr="GPIB0::1::INSTR",name="Santec Laser",debug=False):
        super().__init__(addr,debug=debug)
         
        
    def setWavelength(self,wl):
        self.write(f'WAV {wl:1.3f}')
    def getWavelength(self):
        return np.float(self.query('WAV?'))
        
    def setPower(self,tsl_power):
        self.write(f'POW:LEV {tsl_power:1.3f}')
        
    def getPower(self):
        return float(self.query("POW:LEV?"))
        
        
        
    def setWaveSweep(self,start_wl,stop_wl,step_wl,sweep_speed,mode=1,dwell=1):
        self.write("POW:STAT 1")
        self.write(f'WAV:SWE:STAR {start_wl:1.4f}') # starting wavelength 
        self.write(f'WAV:SWE:STOP {stop_wl:1.4f}') # ending wavelength
        self.write(f'WAV:SWE:SPE {sweep_speed:1.3f}') # sweep speed (minimum value is 1nm/s)
        self.write(f'WAV:SWE:STEP {step_wl:1.4f}')
        self.write('WAV:SWE:DELAY 2') # delay before sweep starts
        self.write(f'WAV:SWE:MOD {mode:.0f}')
        self.write(f'WAV:SWE:DWEL {dwell}')
        
        self.write('TRIG:OUTP 3') # trigger at each wavelength
        self.write(f'TRIG:OUTP:STEP {step_wl:1.4f}')
        
        print(f"Step wl {step_wl}   Speed {sweep_speed}   Mode {mode}")
        
    def getSweepState(self):
        tsl_state = self.query_ascii_values('wav:swe?')
        tsl_state = np.int(tsl_state[0])
        return tsl_state
        
    def startSweep(self):
        self.write('WAV:SWE 1')
        
        
        
        
    def enableShutter(self):
        self.write('POW:SHUTTER 1')
        out = np.float(self.query('POW:SHUTTER?'))
        if out!=1:
            print("Could not enable SHUTTER !!")
        
    def disableShutter(self):
        self.write('POW:SHUTTER 0')
        out = np.float(self.query('POW:SHUTTER?'))
        if out!=0:
            print("Could not enable SHUTTER !!") 
            
            
            
class SantecPowerMeter(VisaInstrument):
    def __init__(self,addr="GPIB0::16::INSTR",name="SantecMPM",channels=[1,2],offsets=[0,0],channel_names=None,debug=False):
        super().__init__(addr,read_termination='',debug=debug,name=name) 
        if len(channels)!=len(offsets):
            raise RuntimeError("in SantecPowerMeter.__init__ : different channels({len(channels):.0f}) and offsets({len(offsets):.0f}) size")
        
        self._max_channels=4
        self.channels = np.array([])
        self.offsets = np.array([])
        self.channel_names = []
        
        if channel_names is None:
            channel_names = [str(i) for i in channels]
                        
        for chan,off,name in zip(channels,offsets,channel_names):
            self.addChannel(chan,name,off)
        

        
        self.state = "None"
        
    # def write(self,cmd,*args):
    #     print("PowerMeter Command : "+cmd)
    #     super().write(cmd)
        
        
        
    def addChannel(self,channel_id,channel_name,offset):
        if channel_id<=self._max_channels:
            self.channels = np.append(self.channels,channel_id)
            self.offsets = np.append(self.offsets,offset)
            self.channel_names.append(channel_name)
        else:
            raise RuntimeError(f"in SantecPowerMeter.addChannel : cannot add channel id '{str(channel_id):s}' greater than 4")
            
    def getMPMState(self):
        mpm_state = self.query_ascii_values('STAT?')
        mpm_state = np.int(mpm_state[0])
        return mpm_state
    
    def stopAction(self,sleep=0.1):
        stopped = False
        while not stopped:
            self.write('STOP')
            state = self.query_ascii_values('STAT?')
            if state[0] == 0:
                stopped=False
            else:
                stopped=True
            time.sleep(0.5)
                
    def waitAction(self,sleep=0.1):
        running = True
        while running:
            state = self.query_ascii_values('STAT?')
            if state[0] != -1:
                running=True
            else:
                running=False
            time.sleep(sleep)
            
        
            
            
    def setMeasPower(self,wl):
        print("setMeasPower")
        self.stopAction()

        self.write("wmod const1")
        self.write(f'wav {wl}')
        self.write('avg 0.01')
        self.write('avg 10')
        self.write('lev 2') # measurements below -10 dBm
        self.write('logn 1') # one measurement point
        self.write('trig 0') # internal trigger
        
        self.state = "SinglePower"
        
        
    def setWaveSweep(self,wl_start,wl_end,wl_inc,sweep_speed,lev,trigger=1,mode=1):
#        stopped = False
#        while not stopped
#        self.write("stop")
        # self.setMeasPower((wl_start+wl_end)/2)
        
        self.write(f'WAV {(wl_start+wl_end)/2}')
        if mode==1:
            self.write('WMOD SWEEP1') # set to sweep 1 mode (continuous sweep, high accuracy)
        else:
            self.write("WMOD CONST1")
        self.write(f'WSET {wl_start:1.4f},{wl_end:1.4f},{wl_inc:1.4f}') # set the wavelength parameters
        self.write(f'SPE {sweep_speed:1.3f}') # set sweep speed    
        self.write(f'LEV {lev:.0f}') # set TIA gain 
        self.write(f'TRIG {trigger:.0f}')
        self.write("MEAS")
        
        self.state = "WavelengthSweep"
        
    def getValueWaveSweep(self):
        pass
                
    
    def measurePower(self,wl,channels=None,offsets=None):
        
        if self.state != "SinglePower":
            self.setMeasPower(wl)
        if channels is None and offsets is None:
            channels = self.channels
            offsets = self.offsets
        
        sample = np.array(self.query_ascii_values('read? 0'))
        lev = 5
        if any(sample > -30):
            lev = 4
        if any(sample > -20):
            lev = 3    
        if any(sample > -10):
            lev = 2
        if any(sample >   0):
            lev = 1
        if any(sample >  10):
            print('ERROR: power into the MPM is above 10 dBm!')
            return
        self.write(f'lev {lev}')
        sample = np.array(self.query_ascii_values('read? 0'))
        
        results = np.zeros(len(channels))
        for i,val in enumerate(channels):
            results[i] = sample[np.int(val)-1]+offsets[i]
        
        
        return results
        
    def getPower(self,chan):
        if chan in self.channels:
            i = np.argmin(np.abs(chan-self.channels))
        return self.measurePower(1550,channels=[chan],offsets=[self.offsets[i]])[0]
    

    
    
    
def getWaveSweep(laser,power,wl_start,wl_end,wl_inc,sweep_speed,lev,mode=1):
    
    laser.setWavelength(wl_start)
    time.sleep(2)
    laser.setWaveSweep(wl_start,wl_end,wl_inc,sweep_speed,mode)
    power.stopAction()
    power.setWaveSweep(wl_start,wl_end,wl_inc,sweep_speed,lev,mode)
    mpm_state = power.getMPMState()
    if  mpm_state !=0:
        raise RuntimeError("in takeSpectrum : MPM not ready")
    
    
    #Start Sweep
    laser.startSweep()
    #Get Laser Ready
    tsl_state = laser.getSweepState()
    
    if mode == 1:
        test_val = 1
    elif mode==0:
        test_val=0
    while tsl_state == 4:
        tsl_state = laser.getSweepState()
        time.sleep(0.1)
    running=False
    counter=0
    while not running or counter<10:
        tsl_state = laser.getSweepState()
        if tsl_state == test_val:
            running = True
        else:
            counter+=1
            time.sleep(1)
    if not running:
        raise RuntimeError(f"in takeSpectrum : Laser not sweeping. Laser state : {tsl_state:.0f} PowerMPM state : {mpm_state:.0f}")
        
    mpm_state = power.getMPMState()
    tsl_state = laser.getSweepState()
    while np.logical_and(mpm_state != 1,tsl_state != 0):
        print("Waiting")
        time.sleep(2)
        mpm_state = power.getMPMState()
        tsl_state = laser.getSweepState()
        
    
    if tsl_state != 0:
        raise RuntimeError(f"in takeSpectrum : Laser could not do sweep. Laser state : {tsl_state:.0f} PowerMPM state : {mpm_state:.0f}")
    
    if mpm_state != 1:
        raise RuntimeError(f"in takeSpectrum : MPM could not do sweep. Laser state : {tsl_state:.0f} PowerMPM state : {mpm_state:.0f}")    
    else:
        results = []
        wl = laser.instr.query_binary_values(':READ:DAT?',datatype='i')
        wl = np.array(wl)/10000
        # results.append(np.arange(wl_start,wl_end+wl_inc/2,wl_inc))
        results.append(wl)
        
        for i,chan in enumerate(power.channels):
            data1 = power.query_binary_values(f'LOGG? 0,{chan:.0f}')
            data1 = np.array(data1)+power.offsets[i]
            results.append(data1)
    return results

def getSpectrum(laser,power,wl_start,wl_end,wl_inc,sweep_speed,mode=1):
    
    res = getWaveSweep(laser,power,wl_start,wl_end,(wl_end-wl_start)/10,(wl_end-wl_start),1,mode)
    
    
    maxPower = -np.inf
    for i,chan in enumerate(power.channels):
        tmp_max = np.max(res[i+1])-power.offsets[i]
        if tmp_max>maxPower:
            maxPower = tmp_max
            
    lev = 5
    if maxPower > -30:
        lev = 4
    if maxPower > -20:
        lev = 3    
    if maxPower > -10:
        lev = 2
    if maxPower >   0:
        lev = 1
    if maxPower >  10:
        print('ERROR: power into the MPM is above 10 dBm!')
        return
    print(f"Setting level to {lev}")
    
    time.sleep(2)
    
    res = getWaveSweep(laser,power,wl_start,wl_end,wl_inc,sweep_speed,lev,mode)
    
    return res
        
        
        
if __name__=='__main__':
    laser = SantecLaser(debug=True)   
    
    power = SantecPowerMeter(debug=True)