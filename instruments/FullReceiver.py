# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 09:33:35 2021

@author: ques
"""

import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument
from HP_PowerMeter import HP_PowerMeter
from JDS_Cassette import JDS_Cassette


import numpy as np
import time



        
        
class RX:
    def __init__(self,addr_pm='GPIB0::22::INSTR',addr_attn='GPIB0::12::INSTR'):
        self.pm = HP_PowerMeter(addr_pm)
        self.attn = JDS_Cassette(addr_attn)
        self.max_pd_power = 10
    
    def closeRX(self):
        self.attn.outOff(1)
    def closePD(self):
        self.attn.outOff(2)
        
    def openRX(self):
        self.attn.outOn(1)
    def openPD(self,pdPower=None):
        if pdPower is not None:
            self.attn.setAttn(40, 2)
            self.attn.outOn(2)
            self.setPowerPD(pdPower)
        else:
            self.attn.outOn(2)
        
    def getPowerRX(self):
        return self.pm.getPower(2)
    def getPowerPD(self):
        return self.pm.getPower(1)
    
    def setMaxPowerPD(self,val):
        self.max_pd_power = val
        
    
    def setPowerRX(self,target,maxiter=40,pd_power=None,safe=True):
        #Protect the PD
        self.openRX()
        if pd_power is None:
            pd_power = self.getPowerPD()
        delta0 = target-self.getPowerRX()
        if safe and delta0>3:
            self.closePD()
        counter=0
        while np.abs(target-self.getPowerRX())>0.1 and counter<maxiter:
            cur_pw_1 = self.getPowerRX()
            
            delta_pw = target-cur_pw_1
            if delta_pw > 10:
                delta_pw = 10
            fact = 0.5
            self.attn.setAttn(self.attn.getAttn(1)-delta_pw*fact,1)
            time.sleep(0.5)
            counter+=1
            
        if counter>=maxiter-1:
            print(f"Max iter reached : check values. TargetRX : {target:.2f} TargetPD : {pd_power:0.2f}")
        
        if safe:
            if delta0 >5:
                self.attn.setAttn(30,2)
            elif delta0 > 2:
                self.attn.setAttn(self.attn.getAttn(2)+6,2)
            elif delta0>0:
                self.attn.setAttn(self.attn.getAttn(2)+3,2)
            else:
                pass
        self.openPD()
        self.setPowerPD(pd_power)
        
    def hold(self,sleep=0.5):
        rx_power = self.getPowerRX()
        pd_power = self.getPowerPD()
        running = True
        
        while running:
            try:
                
                self.setPowerRX(rx_power,pd_power=pd_power,safe=False)
                time.sleep(0.5)
            except:
                running=False
                
                
    def holdPD(self,pd_power=None,sleep=0.5):
        if pd_power is None:
            pd_power = self.getPowerPD()
        else:
            self.setPowerPD(pd_power)
            pd_power = self.getPowerPD()
        running = True
        
        while running:
            try:
                
                self.setPowerPD(pd_power)
                time.sleep(0.5)
            except Exception as e:
                print(e)
                running=False        

        
        
        
    def setPowerPD(self,target,maxiter=20):
        counter=0
        while np.abs(target-self.getPowerPD())>0.05 and counter<maxiter:
            cur_pw = self.getPowerPD()
            
            delta_pw = target-cur_pw
            if delta_pw > 10:
                delta_pw = 10
            fact = 0.5
            self.attn.setAttn(self.attn.getAttn(2)-delta_pw*fact,2)
            time.sleep(0.5)
            counter+=1
        if counter>=maxiter-1:
            print(f"Max iter reached : check values. TargetPD : {target:0.2f}") 
            
            
            
if __name__=="__main__":
    rx = RX()
