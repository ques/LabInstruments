# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 15:32:46 2021

@author: ques
"""

import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument

class JDS_Cassette(VisaInstrument):
    def __init__(self,addr='GPIB0::12::INSTR'):
        super().__init__(addr)
        self.instr.timeout=10000
        
        
    def outOn(self,chan):  
        self.selectChan(chan)
        self.instr.write(':OUTP ON')
        self.wait()
        
    def outOff(self,chan):
        self.selectChan(chan)
        self.instr.write(":OUTP OFF")
        self.wait()
        
        
    def setAttn(self,attn,chan):
        try:
            self.selectChan(chan)
            self.instr.write(f':INP:ATT {attn}')
            self.wait()
        except:
            print("Timeout : trying again")
            self.setAttn(attn, chan)
        
        
    def getAttn(self,chan):
        self.selectChan(chan)
        return self.instr.query_ascii_values(f':INP:ATT?')[0]
         

        
    def selectChan(self,chan):
        self.instr.write(f':INST:NSEL {chan}')
        self.wait()
        
        
if __name__=="__main__":
    jds = JDS_Cassette()
            