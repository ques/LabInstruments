# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 15:53:35 2021

@author: FTNK-LocalAdm
"""

import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument
import numpy as np
import time
import re


class RS_SignalGenerator(VisaInstrument):
    def __init__(self,addr="GPIB0::27::INSTR",name='Rohde&Schwarz Signal Generator SMX'):
        super().__init__(addr) 
        
        
        
    def getRF(self):
        ans = self.query("RF?")
        
        m = re.search("(\d+).(\d+)E\+(\d+)", ans)
        
        return np.float(m.group(1)+'.'+m.group(2))*10**np.float(m.group(3))
        
    
    def getLev(self):
        
        
        ans = self.query("LEV?")
        m = re.search("(\d+).(\d+)", ans)
        return np.float(m.group(1)+'.'+m.group(2))
        
    def setRF(self,freq):
        
        self.write(f"RF {freq:.10e}")
        
    def setLev(self,lev):
        self.write(f"LEV {lev:.10e}")
        
        
        
        
        
        
        
        
        
        
        
        
        
        
if __name__=="__main__":
    
    signal = RS_SignalGenerator()   