# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 13:12:01 2021

@author: FTNK-LocalAdm
"""

import numpy as np
import time

import os,sys
sys.path.append(os.path.dirname(__file__))


from VisaInstrument import VisaInstrument

class Agilent_E3646(VisaInstrument):
    def __init__(self,addr="GPIB0::8::INSTR",name="Agilent_PSU_E3646",unique_channel=None):
        super().__init__(addr)
        
        self.unique_channel=unique_channel
        self.override_channel=None
        
    def setUniqueChannel(self,channel):
        self.unique_channel = channel

    def select(self,channel=None):
        if self.override_channel is not None and channel is None:
            self.write(f"INST:SEL OUT{self.unique_channel:.0f}")
        elif channel==1 or channel==2:
            self.write(f"INST:SEL OUT{channel:.0f}")
        else:
            raise RuntimeError("Agilent_E3646.select : channel needs to be 1 or 2, or with unique channel")
        
    def getVolt(self,channel=None):
        self.select(channel)
        return float(self.query("MEAS:VOLT?"))
    
    def getCur(self,channel=None):
        self.select(channel)
        return float(self.query("MEAS:CURR?"))
    
    def getSetVolt(self,channel=None):
        self.select(channel)
        return float(self.query("VOLT?"))
    
    def getSetCur(self,channel=None):
        self.select(channel)
        return float(self.query("CURR?"))
    
    def setVolt(self,volt,channel=None):
        self.select(channel)
        self.write(f"VOLT {volt:.2f}")
        
    def setCur(self,volt,channel=None):
        self.select(channel)
        self.write(f"CURR {volt:.2f}")
        
    def disableOut(self):
        self.write("OUTP OFF")
        
    def enableOut(self):
        self.write("OUTP ON")
        
        
        
    def IV_Curve(self,start_volt,stop_volt,step_volt,channel,sleep=0.1,niter=1,disp=False):
        
        initV = self.getSetVolt(channel) 
        
        setV = np.arange(start_volt,stop_volt+step_volt/2,step_volt)
        I = np.zeros(setV.shape)
        V = np.zeros(setV.shape)
        
        stdI = np.zeros(setV.shape)
        stdV = np.zeros(setV.shape)   
        
        for indx,v in enumerate(setV):
            self.setVolt(v,channel)
            if disp:
                print(30*'\b'+f"SetVolt : {v:.2e} V",end='')
            time.sleep(sleep)
            tmp_V = []
            tmp_I = []
            for i in np.arange(niter):
                tmp_V.append(self.getVolt(channel))
                tmp_I.append(self.getCur(channel))
                time.sleep(0.1)
                
            tmp_V= np.array(tmp_V)
            tmp_I = np.array(tmp_I)
                
                
            V[indx] = np.mean(tmp_V)
            I[indx] = np.mean(tmp_I)
            stdV[indx]=np.std(tmp_V)
            stdI[indx] = np.std(tmp_I)
            
            
        self.setVolt(initV, channel)
        if disp:
            print("",end='\n')
            
        return I,V,stdI,stdV,setV
            
        
        
        
        
        
        
        
        
        
        
        
if __name__=='__main__':
    pw = Agilent_E3646()
    
    # if True:
    #     print("Testing on a 50Ohm resistor")
    #     pw.setVolt(1, 1)
    #     cur = pw.getCur(1)
    #     print("Resistance : "+str(1/cur))
    
    