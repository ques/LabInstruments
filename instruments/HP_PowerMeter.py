# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 15:41:34 2019

@author: ques
"""

import os,sys
sys.path.append(os.path.dirname(__file__))
import numpy as np

from VisaInstrument import VisaInstrument

class HP_PowerMeter(VisaInstrument):
    def __init__(self,addr='GPIB0::22::INSTR',channels=[1,2],channel_names=None,offsets=None):
        super().__init__(addr)
        
        self.channels = channels
        if channel_names is None or len(channels)!=len(channel_names):
            channel_names = [str(val) for val in self.channels]
        self.channel_names = channel_names
        
        if offsets is None:
            self.offsets = np.zeros(len(channels))
        
        
    def getPower(self,channel):
        if channel in self.channels:
            if channel==1:
                out =  float(self.instr.query(':READ1:POW?'))+self.offsets[self.channels.index(channel)]
            elif channel==2:
                out =  float(self.instr.query(':READ2:POW?'))+self.offsets[self.channels.index(channel)]
                
            if out <1e10:
                return out
            else:
                return np.nan
        else:
            raise RuntimeError("in HP_PowerMeter.getPower : wrong channel ID")
        
        
        
        
if __name__=="__main__":
    
    p = HP_PowerMeter()
    
    