# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 09:34:56 2021

@author: ques
"""
import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument
import numpy as np

class AgilentDSO(VisaInstrument):
    def __init__(self,addr="GPIB0::6::INSTR",name='AgilentDSO'):
        super().__init__(addr)  
        
    def run(self,acq=None):
        self.instr.write(":RUN")
        
    def single(self):
        self.instr.write(":SINGLE")
        
    def stop(self):
        self.instr.write(":STOP")
        
    def wait(self):
        self.instr.write("*WAI")
        
    def clearScreen(self):
        self.instr.write(":CDISplay")
        
    def getWaveform(self,chan,npoints):
        self.write(f":ACQuire:POINts:ANALog {npoints:.0f}")
        self.write(":WAVeform:FORMat WORD")
        self.write(":SYSTEM:HEADER OFF")
        self.write(":WAVEFORM:STREAMING ON")
        self.write(":WAVeform:BYTeorder LSBF")
        self.write(f":WAVeform:SOURce CHANnel{chan:.0f}")

# =============================================================================
# Check if values were changed
# =============================================================================
        points = self.instr.query_ascii_values(":ACQuire:POINts:DIGital?")[0]
        if points != npoints:
            print("Error in setting points to"+str(npoints))
        streaming = self.instr.query_ascii_values(":WAVEFORM:STREAMING?")[0]
        if streaming != 1:
            print("Error in setting streaming on")
        header = self.instr.query_ascii_values(":SYSTEM:HEADER?")[0]
        if header != 0:
            print("Error in setting header off")
        fmt = self.query(":WAVEFORM:FORMAT?")
        if 'WORD' not in fmt:
            print("Error in setting format to WORD")
        

        
        
        self.write(':DIGITIZE')
        self.wait()
        
        
        y_offset = self.instr.query_ascii_values(':WAVeform:YOR?')[0]
        y_scale = self.instr.query_ascii_values(':WAV:YINC?')[0]
        x_offset = self.instr.query_ascii_values(':WAV:XOR?')[0]
        x_scale = self.instr.query_ascii_values(':WAV:XINC?')[0]
#        
        tmp = self.instr.query_binary_values(":WAV:DATA?","h")
        
        if len(tmp) == 0:
            print("Could not get waveform")
         
        ydata = y_offset + np.array(tmp) * y_scale
        xdata = x_offset + (np.arange(0,len(tmp)) * x_scale)
        
       
        return xdata,ydata
        
        
        
        
        
    def saveWaveDisk(self,fname,chan):
        print(f":DISK:SAVE:WAV CHAN{chan:.0f},\""+fname+"\",CSV,ON")
        self.instr.write(f":DISK:SAVE:WAV CHAN{chan:.0f},\""+fname+"\",CSV,ON")