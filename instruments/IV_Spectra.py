# -*- coding: utf-8 -*-
"""
Created on Mon Mar  4 10:33:10 2019

@author: ques
"""

from OSA import OSA
from Keithley import Keithley

import numpy as np
import time


class IVSpcMeasurement:
    def __init__(self,delay=0.1,center_wl=1530,span_wl=100):
        self.osa = OSA()
        self.osa.stop()
        self.osa.setCenter(center_wl)
        self.osa.setSpan(span_wl)
        self.psu = Keithley()
        self.delay = delay
        
    def takeSingleShot(self,file,header='',trace='A',span=100,center=1530):
        
        self.osa.stop()
        self.osa.setSpan(span)
        self.osa.setCenter(center)
        self.osa.single()
        print('Fake measurement',end='')
        time.sleep(10)
        self.osa.stop()
        self.osa.single()
        print(30*'\b'+'Real measurement',end='')
        time.sleep(25)
        
        self.saveSpectrum(file,header,trace,span,center)
        
    def saveSpectrum(self,file,header='',trace='A',span=100,center=1530):
        X = self.osa.getX(trace)
        Y = self.osa.getY(trace)
        print(30*'\b'+'Saving in '+file,end='')
      
        np.savetxt(file,np.array([X,Y]).T,header=header+'\n '+'Wavelength [nm]'+8*' '+'Intensity in dBm')        

        
    def measureSPCs(self,min_V,max_V,step_V,file,header='',span=100,center=1530):
        
        self.fakedelay=20
        self.realdelay=35
        
        V = np.arange(min_V,max_V+step_V,step_V)
        I = np.array([])
        stdI = np.array([])
        
        self.psu.enable_trigger()
        
        self.psu.setV(V[0])
        self.psu.enable()
        self.psu.trigger()
        time.sleep(10)
        self.osa.setSpan(span)
        self.osa.setCenter(center)
        self.osa.single()
        time.sleep(self.fakedelay)
        self.osa.stop()
        self.osa.single()
        time.sleep(self.realdelay)
        X = self.osa.getX("A")
        
        array = np.array(X)
        array = np.expand_dims(X,axis=0)
        
        for volt in V:
            #Set the voltage and wait for a while
            self.psu.setV(volt)
            self.psu.trigger()
            print('Voltage : '+str(volt)+'V : ',end='')
            time.sleep(self.delay)
            
            #Perform a first dummy measurement 
            self.osa.single()
            print('Fake measurement',end='')
            time.sleep(self.fakedelay)
            self.osa.stop()
            
            #Perform the real measurement while reading current
            self.osa.single()
            start = time.time()
            
            print(16*'\b'+'Real measurement')
            tmp_I = np.array([])
            while time.time()-start<self.realdelay:
                try:
                    tmp_I = np.append(tmp_I,self.psu.getIDC())
                    time.sleep(0.5)
                except:
                    print('Could not read current')
                
            I  = np.append(I,np.mean(tmp_I))
            stdI = np.append(stdI,np.std(tmp_I))
            
            #Try to read the value
            check=True
            while check:
                try:
                    Y = self.osa.getY("A")
                    check=False
                except:
                    check=True
    
            array = np.vstack((array,Y))            
        #Disable PSU
        self.psu.disable()
        
        Vsave = np.append(np.nan,V)
        Isave = np.append(np.nan,I)
        stdIsave = np.append(np.nan,stdI)
        arraysave = np.vstack((stdIsave,array.T)).T
        arraysave = np.vstack((Isave,arraysave.T)).T
        arraysave = np.vstack((Vsave,arraysave.T)).T
        np.savetxt(file,arraysave.T,header=header+'\nFirst line = voltage[V] - Second line = current[A] - Third line = current deviation[A]')
            
        return array,V,I
    
    def measureIntensity(self,min_V,max_V,step_V,centerWl,numSweep,file,header=''):
        self.osa.setSpan(0)
        self.osa.setCenter(centerWl)
        
        P = np.array([])
        V = np.arange(min_V,max_V+step_V,step_V)
        I = np.array([])
        sigI = np.array([])
        sigP = np.array([])
        
        for volt in V:
            self.psu.setV(volt)
            self.psu.enable()
            self.psu.trigger()
            
            time.sleep(5)
            
            tmp_I = np.array([])
            tmp_P = np.array([])
            for i in range(numSweep):
                tmp_I = np.append(tmp_I,self.psu.getIDC())
                self.osa.single()
                time.sleep(3)
                check=True
                while check:
                    try:
                        Y = self.osa.getY("A")
                        check=False
                    except:
                        check=True
                tmp_P = np.append(tmp_P,np.mean(Y))
            
            P = np.append(P,np.mean(tmp_P))
            I = np.append(I,np.mean(tmp_I))
            sigP = np.append(sigP,np.std(tmp_P))
            sigI = np.append(sigI,np.std(tmp_I))
            
        self.psu.disable()
        
        #Saving of the data
        
        array = np.vstack((V,I,sigI,P,sigP))
        array = array
        try:
            np.savetxt(file,array.T,header=header+"\n"+15*' '+'V[V]'+15*' '+'I[A]'+15*' '+'sigI[A]'+15*' '+'P[dBm]'+15*' '+'sigP[dBm]')
        except:
            print('Could not save data')
        return I,sigI,P,sigP,V
                
                
        
        
        
    
    
    
if __name__=='__main__':
    meas = IVSpcMeasurement()
    
            
        
            
            
            