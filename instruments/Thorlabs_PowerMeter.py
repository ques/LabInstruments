# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 15:41:34 2019

@author: ques
"""

import os,sys
sys.path.append(os.path.dirname(__file__))
import numpy as np
import time

from VisaInstrument import VisaInstrument

class PM320(VisaInstrument):
    def __init__(self,addr='USB0::0x1313::0x8022::M00606447::INSTR',channels=[1,2],channel_names=None,offsets=None):
        super().__init__(addr)
        
        self.instr.timeout=10000
        
        self.channels = channels
        if channel_names is None or len(channels)!=len(channel_names):
            channel_names = [str(val) for val in self.channels]
        self.channel_names = channel_names
        
        if offsets is None:
            self.offsets = np.zeros(len(channels))
        
        
    def getPower(self,channel):
        if channel in self.channels:
            if channel==1:
                self.write(":MEAS:INIT1")
                time.sleep(0.1)
                out =  10*np.log10(float(self.instr.query(':FETCH:POW1:VAL?'))*1e3)+self.offsets[self.channels.index(channel)]
            elif channel==2:
                self.write(":MEAS:INIT2")
                time.sleep(0.1)
                out =  10*np.log10(float(self.instr.query(':FETCH:POW2:VAL?'))*1e3)+self.offsets[self.channels.index(channel)]
                
            if out < 1e10 and out !=-np.inf:
                return out
            else:
                return self.getPower(channel)
        else:
            raise RuntimeError("in HP_PowerMeter.getPower : wrong channel ID")
        
        
        
class PM100D(VisaInstrument):
    def __init__(self,addr='USB0::0x1313::0x8078::P0024464::INSTR'):
        super().__init__(addr)
        
        self.instr.timeout=10000
        self.write("POW:UNIT W")
        
        
    def getPower(self):
        self.write("MEAS:SCAL:POW")
        self.write("INIT:IMM")
        time.sleep(0.1)
        out =  10*np.log10(float(self.instr.query(':FETC?'))*1e3)
        
        if out < 1e10 and out !=-np.inf:
            return out
        else:
            return self.getPower()
    
        
        
        
if __name__=="__main__":
    
    # p = PM320()
    
    p = PM100D()
    
    