# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 13:16:46 2021

@author: FTNK-LocalAdm
"""
import visa

#from PyQt5.QtCore import QObject



class VisaInstrument:
    def __init__(self,addr,name='VisaInstrument',read_termination=None,debug=False):
        self.addr=addr
        self.debug = debug
        self.name=name
        self.read_termination=read_termination

        self.instr=None
        self.connect(maxit=0)
        
        
    def disp(self,msg):
        if self.debug:
            print(self.name+' : '+msg)
        
        
        
    def connect(self,maxit=3):
        connected=False
        counter=0
        while not connected:        
            try:
                rm = visa.ResourceManager()
                if self.read_termination is not None:
                    self.instr = rm.open_resource(self.addr,read_termination=self.read_termination)
                else:
                    self.instr = rm.open_resource(self.addr)
                try:
                    ans = self.instr.query("*IDN?")
                    print("Connected to : ",ans,' at '+self.addr)
                except:
                    print("Could not get identification")
                connected=True
            except Exception as e:
                counter+=1
                if counter<=maxit:
                    print(f"Could not connect on {counter:.0f} attempt")
                    
                else:
                    raise RuntimeError("Could not connect : "+str(e)) 
        
        
    def write(self,cmd,*args):
        self.disp(cmd)
        if self.instr is not None:
            self.instr.write(cmd,*args)
        else:
            raise RuntimeError(f"In {self.name:s}.write : Instrument not connected")
            
            
    def query(self,cmd,*args):
        self.disp(cmd)
        if self.instr is not None:
            return self.instr.query(cmd,*args)
        else:
            raise RuntimeError(f"In {self.name:s}.query : Instrument not connected")
       
    def query_ascii_values(self,cmd,*args):
        self.disp(cmd)
        if self.instr is not None:
            return self.instr.query_ascii_values(cmd,*args)
        else:
            raise RuntimeError(f"In {self.name:s}.query_ascii_values : Instrument not connected")
    def query_binary_values(self,cmd,*args):
        self.disp(cmd)
        if self.instr is not None:
            return self.instr.query_binary_values(cmd,*args)
        else:
            raise RuntimeError(f"In {self.name:s}.query_binary_values : Instrument not connected")       
    def read(self,*args):
        if self.instr is not None:
            return self.instr.read(*args)
        else:
            raise RuntimeError(f"In {self.name:s}.read : Instrument not connected")
        
    def close(self):
        if self.instr is not None:
            self.instr.close()
        else:
            raise RuntimeError(f"In {self.name:s}.close : Instrument not connected")
            
            
            
    def wait(self):
        try:
            self.query("*OPC?")
        except:
            self.wait()
        
        
    def __del__(self):
        self.close()