# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 15:25:54 2022

@author: FTNK-LocalAdm
"""

import wsapi as ws
import numpy as np
import matplotlib.pyplot as plt
import os,sys
from scipy.special import erf
sys.path.append(os.path.dirname(__file__))

class WaveShaper_4000A:
    def __init__(self,serial_number="SN201211"):
        self.name = "WaveShaper"
        self.serial_number = serial_number
        
        self.start_freq = 187.2750
        self.stop_freq = 196.2740
        self.step_freq = 0.001
        
        self.start_dir = os.path.dirname(__file__)
        
        ans = ws.ws_delete_waveshaper(self.serial_number)
        
        self.current_filter = None
        
            
    
    def connect(self):
        ans = ws.ws_create_waveshaper(self.serial_number, self.start_dir+"\\"+f"WaveShaper_Config\{self.serial_number:s}.wsconfig")        
        if ans[0]!=0:
            print("Could not connect to Waveshaper"+self.serial_number)
        
    def disconnect(self):
        ans = ws.ws_delete_waveshaper(self.serial_number)
        if ans!=0:
            print("Could not disconnect to Waveshaper"+self.serial_number)
            
                
        
    def setBandPass(self,center_freq,span_freq,output_port):
        """
        Applies a rectangular bandpass filter
        
        Parameters
        ----------
        center_freq : float
            The center frequency in THz of the bandpass filter.
        span_freq : float
            The span in frequency in THz of the bandpass filter.
        output_port : int
            The output port of the bandpass filter.

        Returns
        -------
        None.

        """
        freq = np.arange(self.start_freq,self.stop_freq+self.step_freq/2,self.step_freq)
        phase = np.zeros(freq.shape)
        port = np.zeros(freq.shape)
        att = np.zeros(freq.shape)+60
        
        center_start = np.argmin(np.abs(freq-(center_freq-span_freq/2)))
        center_stop = np.argmin(np.abs(freq-(center_freq+span_freq/2)))
        
        
        port[center_start:center_stop+1] = output_port
        att[center_start:center_stop+1] = 0
        
        
        self.applyFilter(freq, phase, port, att)


    def setGaussPass(self,center_freq,span_freq,output_port,skew=0):
        """
        Applies a 1st gaussian bandpass filter
        Parameters
        ----------
        center_freq : float
            The center frequency in THz of the bandpass filter.
        span_freq : float
            The span in frequency in THz of the bandpass filter.
        output_port : int
            The output port of the bandpass filter.

        Returns
        -------
        None.

        """
        self.setSkewedGaussPass(center_freq, span_freq, output_port,skew=0)        

        
    def setSkewedGaussPass(self,center_freq,span_freq,output_port,skew=0,chirp=0):
        """
        Applies a 1st gaussian bandpass filter with skewing parameter
        Parameters
        ----------
        center_freq : float
            The center frequency in THz of the bandpass filter.
        span_freq : float
            The span in frequency in THz of the bandpass filter.
        output_port : int
            The output port of the bandpass filter.

        Returns
        -------
        None.

        """
        
        freq = np.arange(self.start_freq,self.stop_freq+self.step_freq/2,self.step_freq)
        port = np.zeros(freq.shape)+output_port
        
        ##Attenuation Change
        FWHM = span_freq
        sigma = FWHM/2/np.sqrt(2*np.log(2))
        
        gauss = lambda x: np.exp(-(x-center_freq)**2/2/sigma**2)
        phi = lambda x: 0.5*(1+erf(skew*(x-center_freq)/sigma))
        
        fct = lambda x: 2*gauss(x)*phi(x)
        
        att = fct(freq)/np.max(fct(freq))
        att[att<1e-7] = 1e-7
        att = -10*np.log10(att)
        att[att>60] = 60
        
        ##Addition of the chirp in the phase
        if chirp==0:
            phase = np.zeros(freq.shape)
        else:
            phase = np.zeros(freq.shape)
            

    
        self.applyFilter(freq, phase, port, att)


        
        
    def applyFilter(self,freq,phase,port,att):
        profiletext = ""
        for f,ph,p,a in zip(freq,phase,port,att):
            profiletext+=f"{f:0.4f}\t{a:0.6f}\t{ph:0.6f}\t{p:0.0f}\n"
    
        self.connect()
        rc = ws.ws_load_profile(self.serial_number, profiletext)
        if rc !=0:
            print("Could not apply filter")
        else:
            self.current_filter = np.vstack((freq,phase,port,att)).T
        self.disconnect()        
        

    
if __name__=="__main__":
    waveshaper = WaveShaper_4000A()
        