# -*- coding: utf-8 -*-
"""
Created on Wed Jun 23 15:28:37 2021

@author: FTNK-LocalAdm
"""

import numpy as np
import requests
import time



class WaveAnalyser:
    def __init__(self,addr="192.168.2.8"):
        self.addr = addr        
        
        
    def getSpectrum(self,center):
        c = 299792458
        center_f = c/center*1000
        
        
        
        cmd = f"http://{self.addr:s}/wanl/scan/{center_f:.0f}/1000000/HighSens"
        ans = requests.get(cmd)
        print(ans.text)
        
        time.sleep(1)
        
        cmd = f"http://{self.addr:s}/wanl/data/json"
        ans = requests.get(cmd)
        foo = ans.json()['data'] 
        measData = np.array(foo) 
        freq = measData[:,0]/1e6 
        Pwr = measData[:,3]/1e3 
        
        return c/freq/1000,Pwr
        
        
        
        
        
if __name__=="__main__":
    
    spec = WaveAnalyser()
        
        
        
        