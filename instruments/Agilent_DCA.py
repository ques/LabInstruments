# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 09:35:55 2021

@author: ques
"""
import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument
import numpy as np
import time



class AgilentDCA(VisaInstrument):
    def __init__(self,addr="GPIB0::7::INSTR",name='AgilentDCA'):
        super().__init__(addr)   
        
    def run(self,acq=None):
        self.instr.write(":RUN")
        
    def single(self):
        self.instr.write(":SINGLE")
        
    def stop(self):
        self.instr.write(":STOP")
        
    def wait(self):
        self.instr.write("*WAI")
        
        
        
    def clearScreen(self):
        self.instr.write(":CDISplay")
        
    def eyeMode(self):
        self.instr.write(":SYSTem:MODE EYE")
    def oscMode(self):
        self.instr.write(":SYSTem:MODE OSC")
        
        
    def saveScreenImage(self,filename):
        screenimage = '%USER_DATA_DIR%\\Screen Images\\test_image.jpg'
        self.write(':DISK:SIMage:FNAMe "' + screenimage + '"')
        self.write(":DISK:SIMage:SAVE")
        self.query("*OPC?")
        filepath = self.query(':DISK:SIMage:FNAMe?')
        data = b''
        message = ':DISK:FILE:READ? "' + screenimage + '"'
        data = self.instr.query_binary_values(message,
                                        datatype='B',
                                        header_fmt='ieee',
                                        container=bytes)
        
        with open(filename, 'wb') as fout:
            fout.write(data)
        
        
    def getEyeMask(self,channel,intTime=10):
        ydata = []
        xdata = []
        
        self.stop()
        self.eyeMode()
        self.clearScreen()
        self.instr.write(f":WAV:SOUR CHAN{channel:.0f}")
        
        self.instr.write(':WAV:FORM WORD')
        self.instr.write(':WAV:BYT LSBFirst')
        self.instr.write("SYSTEM:HEADER OFF")
        

        
        self.run()
        time.sleep(intTime)
        self.stop()
        
        eyedata = self.instr.query_binary_values(':WAVeform:EYE:INTeger:DATa?',
                                          datatype='L',
                                          container=list,
                                          is_big_endian=False,
                                          header_fmt='ieee')
        
        
        #Get info on axis
        y_offset = self.instr.query_ascii_values(':WAV:EYE:YOR?')[0]
        y_scale = self.instr.query_ascii_values(':WAV:EYE:YINC?')[0]
        x_offset = self.instr.query_ascii_values(':WAV:EYE:XOR?')[0]
        x_scale = self.instr.query_ascii_values(':WAV:EYE:XINC?')[0]
        
        rows = self.instr.query_ascii_values(':WAVeform:EYE:ROWS?')[0]
        columns = self.instr.query_ascii_values(':WAVeform:EYE:COL?')[0]
        
        
        n=0
        col=0
        while col < columns:
            row = 0
            while row < rows:
                point = eyedata[n]
                if point:  # point has hits
                    for i in np.arange(point):
                        xdata.append(round(col,3)*x_scale)  # scaled x location
                        ydata.append(round(row, 3)*y_scale+y_offset)  # scaled y location
                row += 1
                n += 1
            col += 1
            
        return np.array(xdata),np.array(ydata)
        
    def getEye(self,channel,intTime=10):
        
        ydata = np.array([])
        xdata = np.array([])
        
        self.stop()
        self.clearScreen()
        self.instr.write(f":WAV:SOUR CHAN{channel:.0f}")
        
        self.instr.write(':WAV:FORM WORD')
        self.instr.write(':WAV:BYT LSBFirst')
        self.instr.write("SYSTEM:HEADER OFF")
        
        #Get info on axis
        y_offset = self.instr.query_ascii_values(':WAV:YOR?')[0]
        y_scale = self.instr.query_ascii_values(':WAV:YINC?')[0]
        x_offset = self.instr.query_ascii_values(':WAV:XOR?')[0]
        x_scale = self.instr.query_ascii_values(':WAV:XINC?')[0]
        
        #Got in eye mode for acquisition
        self.eyeMode()
        start = time.time()
        while (time.time()-start)<intTime:
            self.single()
            self.wait()
            tmp = self.instr.query_binary_values(":WAV:DATA?","h")
            tmp = np.array(tmp,dtype=float)
            indx = np.where(np.bitwise_and(tmp<=30720,tmp>=-32736))[0]
            #Get info on axis
            tmpy = y_offset + tmp * y_scale
            tmpx = x_offset + (np.arange(0,len(tmpy)) * x_scale)
            ydata = np.concatenate((ydata,tmpy[indx]))
            xdata = np.concatenate((xdata,tmpx[indx]))
            
            
        

        self.stop()

        self.oscMode()
        self.run()
        
        # xdata = xdata-np.min(xdata)
        return xdata,ydata
    
    
    
    def saveEye(self,filename,channel,intTime=10):
        x,y = self.getEyeMask(channel,intTime)
        np.savetxt(filename+'.txt',np.vstack((x,y)).T)
        
        self.saveScreenImage(filename+'.png')
        
        self.oscMode()
        self.run()
        
    
    
    
if __name__ == '__main__':
    dca =AgilentDCA()
    
