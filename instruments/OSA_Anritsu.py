# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 15:20:42 2019

@author: ques
"""
import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument
import numpy as np
import time

class OSA_Anritsu(VisaInstrument):
    
    def __init__(self,address = "TCPIP0::169.254.38.156::inst0::INSTR"):
        super().__init__(address)
    
    def setTrace(self,trace="A"):
        self.write("TRAC:ACT "+trace)
        self.write("TRAC:ATTR:TR"+trace+" WRIT")
        time.sleep(0.5)
    def disableTrace(self,trace="B"):
        self.write("TRAC:ATTR:TR"+trace+" FIX")
        time.sleep(0.5)
    def setCenter(self,center):
        self.write("CENT {:.2f}nm".format(center))
        time.sleep(0.5)
        
    def setRes(self,res):
        self.write(f"BAND {res:.2f}nm")
        time.sleep(0.5)
        
        
    def setBandwidth(self,bw):
        self.write(f"BAND:VID {bw:.2f}")
        time.sleep(0.5)
    def setSpan(self,span):
        self.write("SPAN {:.3f} nm".format(span))
        time.sleep(0.5)
        
    def setSampPoints(self,npoints):
        self.write(f"SWE:POIN {npoints:.0f}")
        time.sleep(0.5)
            
    def getX(self,trace="A",timeout=1000):
        wl = self.query_ascii_values('TRAC:DATA:Y:DC'+trace+"?")
        time.sleep(0.5)
        return np.linspace(wl[0], wl[1], int(wl[2]))
    
    def getY(self,trace="A",timeout=1000):
        data_level = self.query('TRAC:DATA:Y:Dm'+trace+"?")
        data_level = np.array(data_level.strip().split('\r\n'), dtype=float)
        return data_level
    
    def getTrace(self,trace="A",timeout=1000):
        
        return self.getX(trace,timeout),self.getY(trace,timeout)
    
    def single(self):
        self.write("INIT:CONT OFF")
        self.write("INIT")
    def repeat(self):
        self.write("INIT:CONT ON")
        self.write("INIT")
        
    def stop(self):
        self.write("ABOR")
   
        
    # def setOptMode(self,center,trace='A',sens='HIGH1'):
        
    #     self.stop()
    #     self.setTrace(trace)
    #     self.setSensitivity(sens)
    #     self.setRes(2)
    #     self.setCenter(center)
    #     self.setSpan(0)
    #     self.enableSync()
    #     self.repeat()
    
    def takeContinuous(self,center,span,sync=False,res=1,trace='A',rbw=10e3,npoints=1001):
        self.stop()
        self.setBandwidth(rbw)
        self.setCenter(center)
        self.setSpan(span)
        self.setSampPoints(npoints)
        self.setRes(res)
        self.setTrace(trace)
        self.repeat()
        
    def takeSingle(self,center,span,sync=False,res=1,trace="A",rbw=10e3,npoints=1001):
        
        self.stop()
        self.setBandwidth(rbw)
        self.setCenter(center)
        self.setSpan(span)
        self.setSampPoints(npoints)
        self.setRes(res)

                
        self.setTrace(trace)
        
        self.single()
        self.query("ESR2?")
        self.wait()
        
        X,Y = self.getTrace(trace)
        
        return X,Y
    
    
    def wait(self,delay=0.5):
        
        
        while(float(self.query("ESR2?")) !=3):
            time.sleep(delay)
            
            
        
    
    
    # def wait(self):
    #     stop = False
    #     while not stop:
    #         try:
    #             bit = self.query_ascii_values("SWEEP ?")
    #             if bit[0] == 0:
    #                 stop = True
    #                 time.sleep(0.1)
    #         except KeyboardInterrupt as e:
    #             print('Keyboard interruption')
    #             stop = True
    #         except:
    #             stop = False
    #             time.sleep(1)
    
    

if __name__=="__main__":
    osa = OSA_Anritsu()
    