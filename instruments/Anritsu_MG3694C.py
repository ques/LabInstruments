# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 13:22:36 2021

@author: FTNK-LocalAdm
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 15:53:35 2021

@author: FTNK-LocalAdm
"""

import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument
import numpy as np
import time
import re


class Anritsu_MG3694C(VisaInstrument):
    def __init__(self,addr="GPIB0::30::INSTR",name='Anritsu Signal Generator 1-40GHz',debug=False):
        super().__init__(addr,debug=debug) 
        
        
        
    def getRF(self):
        ans = self.query("OF0")
                
        return float(ans)
        
    
    def getLev(self):
        
        
        ans = self.query("OL1")
        return float(ans)
        
    def setRF(self,freq):
        
        self.write(f"F0 {freq/1e9:.10f} GH")
        
    def setLev(self,lev):
        self.write(f"L1 {lev:.10f} DM")
        
        
        
        
        
        
        
        
        
        
        
        
        
        
if __name__=="__main__":
    
    signal_high = Anritsu_MG3694C()   