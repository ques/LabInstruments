# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 15:01:48 2021

@author: FTNK-LocalAdm
"""
import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument
import numpy as np
import time



class AgilentE4446(VisaInstrument):
    def __init__(self,addr="GPIB0::18::INSTR",name='Agilent Series Spectrum Analyser'):
        super().__init__(addr,name) 
        
    def continuous(self):
        self.write("INITiate:CONTinuous ON")
    
    def single(self):
        self.write(":INITiate:CONTinuous OFF")
        self.write(":INIT:IMM")
        
    def setSpan(self,span,unit='Hz'):
        self.write(f":FREQ:SPAN {span:.10e} {unit:s}")
    def getSpan(self):
        return float(self.query(":FREQ:SPAN?"))
        
        
    def setCenter(self,freq,unit='Hz'):
        self.write(f":FREQ:CENT {freq:.10e} {unit:s}")
    def getCenter(self):
        return float(self.query(":FREQ:CENTER?"))
    
    def getStartFreq(self):
        return float(self.query(":FREQ:STAR?"))
    def getStopFreq(self):
        return float(self.query(":FREQ:STOP?"))
    
    def setBW(self,bw,unit='Hz'):
        self.write(f":BAND:BWID:RES {bw:.10e} {unit:s}")  
    def getBW(self):
        return float(self.query(":BAND:BWID:RES?"))
    
    def setRMSAvg(self,count):
        if count>1:
            self.write(":AVER:TYPE RMS")
            self.write(":AVER ON")
            self.write(f":AVER:COUN {count:.0f}")
            self.write(":AVER:CLE")
        else:
            self.write(":AVER OFF")
            self.write(":AVER:CLE")
            
    def setPWRAvg(self,count):
        if count>1:
            self.write(":AVER:TYPE LOG")
            self.write(":AVER ON")
            self.write(f":AVER:COUN {count:.0f}")
            self.write(":AVER:CLE")
        else:
            self.write(":AVER OFF")
            self.write(":AVER:CLE")
        
    def getYData(self,trace=1):
        self.write(":FORMat:TRACe:DATA ASCii")
        ans = self.query_ascii_values(f"TRAC:DATA? TRACE{trace:.0f}")
        return np.array(ans)
    
    
    def getSpectrum(self,center,span,avg=10):
           self.setCenter(center)
           self.setSpan(span)
           self.setPWRAvg(avg)
           
           self.single()
           while float(self.query("STATUS:OPERation?"))>0:
               time.sleep(0.5)
           
           Y = self.getYData(1)
           
           X = np.linspace(center-span/2,center+span/2,len(Y))
           
           return X,Y








if __name__=="__main__":
    
    spec = AgilentE4446()
    
    