# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 15:53:35 2021

@author: FTNK-LocalAdm
"""

import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument



class RS_40_SignalGenerator(VisaInstrument):
    def __init__(self,addr="GPIB0::28::INSTR",name='Rohde&Schwarz Signal Generator 40GHz'):
        super().__init__(addr) 
        
        
        
    def getRF(self):
        return float(self.query("FREQ?"))
        
    
    def getLev(self):
        return float(self.query("POW?"))
        
    def setRF(self,freq):
        if freq >=1e9:
            freq = freq/1e9
        
        self.write(f"FREQ {freq:.10e}GHz")
        
    def setLev(self,lev):
        self.write(f"POW {lev:.10e}dBm")
        
    def outOn(self):
        self.write("OUTP:STAT ON")
        
    def outOff(self):
        self.write("OUTP:STAT OFF")
        
        
        
        
        
        
        
        
        
        
        
        
        
        
if __name__=="__main__":
    
    signal = RS_40_SignalGenerator()   