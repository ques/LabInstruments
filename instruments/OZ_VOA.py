# -*- coding: utf-8 -*-
"""
Created on Thu May 20 09:13:48 2021

@author: FTNK-LocalAdm
"""


from VisaInstrument import VisaInstrument
import time
import numpy as np
import re


class OZ_VOA(VisaInstrument):
    def __init__(self,addr="ASRL4::INSTR",name='OZ_VOA'):
        super().__init__(addr)  
        self.clearBuffer()
        
        
        
    def wait(self,sleep=1):
        done = False
        while not done:
            buffer_length = self.instr.bytes_in_buffer
            if buffer_length > 0:
                ans = self.read()
                if "Done" in ans:
                    done=True
                else:
                    done=False
                time.sleep(1)
                
            else:
                time.sleep(sleep)
        
    def home(self):
        self.clearBuffer()
        self.write("H")
        self.wait()
        self.clearBuffer()
        
    def setAttn(self,attn):
        self.clearBuffer()
        self.write(f"A{attn:.2f}")
        self.wait()
        self.clearBuffer()
        
    def getAttn(self):
        self.clearBuffer()
        ans = self.query("A?")
        self.wait()
        self.clearBuffer()
        
        m = re.search("Atten:(\d+).(\d+)", ans)
        
        return np.float(m.group(1)+'.'+m.group(2))
    
    
    
    def addAttn(self,attn):
        cur_attn = self.getAttn()
        self.setAttn(cur_attn+attn)
        
        
        
        
        
        
        
        
    def clearBuffer(self):
        buffer_length=self.instr.bytes_in_buffer
        time.sleep(0.1)
        self.instr.read_bytes(buffer_length)
        
        
        
        
if __name__=="__main__":
    voa = OZ_VOA()
        