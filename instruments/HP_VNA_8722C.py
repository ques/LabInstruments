# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 09:28:09 2021

@author: FTNK-LocalAdm
"""

import os,sys
sys.path.append(os.path.dirname(__file__))

import numpy as np
import time
import visa


from VisaInstrument import VisaInstrument

class HP_VNA_8722C(VisaInstrument):
    def __init__(self,addr="GPIB0::20::INSTR",name='HP_VNA',debug = False):
        super().__init__(addr,debug)
        self.instr.timeout=10000
        
        
        

        
    def single(self,wait=True,maxit=30):
        self.write("OPC?;SING")
        done = False
        counter=0
        while not done:
            try:
                out = float(self.read())
                if out==1:
                    done=True
            except visa.VisaIOError as e:
                if counter<=maxit:
                    counter+=1
                else:
                    raise e
                    
    def setLogFreq(self):
        self.write("LOGFREQ")
        
    def setLinFreq(self):
        self.write("LINFREQ")
        
        
    # def setRFPower(self,power):
    #     self.write("POWE {power:.2f}")
        
    def setStart(self,start):
        self.write(f"STAR {start:.5e}")
    def setStop(self,stop):
        self.write(f"STOP f{stop:.5e}")
        
    def getStart(self):
        return float(self.query("STAR?"))
    
    def getStop(self):
        return float(self.query("STOP?"))
    
    def setPoints(self,npoints):
        allowed_points = np.array([3,11,21,51,101,201,401,801,1601])
        if npoints not in allowed_points:
            npoints = allowed_points[np.argmin(np.abs(npoints-allowed_points))]
        self.write(f"POIN {npoints:.0f}")
    
    def getPoints(self):
        return int(float(self.query("POIN?")))
        
    
    def getFreq(self):
        start = self.getStart()
        stop = self.getStop()
        n = self.getPoints()
        
        if float(self.query("LINFREQ?"))==1:
            return np.linspace(start,stop,n)
        elif float(self.query("LOGFREQ?"))==1:
            return np.logspace(np.log10(start), np.log10(stop),n)
    
    
    def getPowerData(self,npoints=201,log=False):
        
        #Set the amount of points
        self.setPoints(npoints)
        
        #Set the frequency axis and get it
        if log:
            self.setLogFreq()
        else:
            self.setLinFreq()
        freq = self.getFreq()
        
        dat = np.zeros(freq.shape)
        
        #Set the Format
        self.write("FORM4")
        #Launch acquisition
        self.single()
        #Get data and parse it
        raw_data = self.query("OUTPFORM")
        #First split by '\n' and remove last one
        raw_data = raw_data.split('\n')
        raw_data = raw_data[:-1]
        
        
        for i,string in enumerate(raw_data):
            dat[i] = float(string.split(',')[0])

        
        return freq,dat
    
    
    def setS11(self):
        self.write("S11")
    
    def setS21(self):
        self.write("S21")
        
    def setS12(self):
        self.write("S12")
    
    def setS22(self):
        self.write("S22")
        
        
        
        
        
        
        
        
if __name__=="__main__":
    
    vna = HP_VNA_8722C()    