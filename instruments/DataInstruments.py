
import os,sys
sys.path.append(os.path.dirname(__file__))

import visa
import logging
import time
import numpy as np

class MP176xC:
    def __init__(self, address, dry_run=False):
        self._address = address
        self.connected = False
        self._dry_run = dry_run

        self._logger = logging.getLogger(self.name + "_{}".format(address))

    def connect(self):
        if self._dry_run:
            self.connected = True
            return

        self._rm = visa.ResourceManager()

        self._inst = self._rm.open_resource(self._address)
        print(self._inst)

        self.instrument_id = self.query("*IDN?")

        if "ANRITSU" in self.instrument_id:
            self.connected = True

    def write(self, msg):
        self._logger.debug(msg)
        if not self._dry_run:
            self._inst.write(msg)

    def query(self, msg):
        self._logger.debug(msg)
        if not self._dry_run:
            response = self._inst.query(msg)
            self._logger.debug(response)
            return response
        return ""
    
    

    def set_byte_array(self, data):
        """Transfer bytes to instrument.
        
        The method switches the instrument into data pattern mode [MP1764C, PTS, p. 9-54], sets the appropriate frame size [MP1764C, DLN, p. 9-61] and then transfers bytes [MP1764C, WRT, p. 9-78].

        The upper limit on the data length is 2^23 = 8 388 608 [MP1764C, p. 9-61].

        Above 524 288 bits (65 536 bytes) a step size constraint is imposed. The limitations are:

        - From 65 536 bytes: 2 bytes per step
        - From 131 072 bytes: 4 bytes per step
        - From 262 144 bytes: 8 bytes per step
        - From 524 288 bytes: 16 bytes per step
        
        See also the MP176x Appnote on data transfer."""

        N_bits = len(data) * 8

        if N_bits > 2**23:
            raise ValueError("Data is limited to 2**23 bits")

        # Step size limitations

        if   524_288 < N_bits and not (N_bits % 16 == 0):
            raise ValueError("Bit length must be a multiple of 16")

        if 1_048_576 < N_bits and not (N_bits % 32 == 0):
            raise ValueError("Bit length must be a multiple of 32")

        if 2_097_152 < N_bits and not (N_bits % 64 == 0):
            raise ValueError("Bit length must be a multiple of 64")

        if 4_194_304 < N_bits and not (N_bits % 128 == 0):
            raise ValueError("Bit length must be a multiple of 128")

        self.write("PTS 1")
        self.write("DLN {N_bits}".format(N_bits=N_bits))
        self.write("WRT {N_bytes},0".format(N_bytes=len(data)))
        self._logger.debug("Writing {N_bytes} bytes".format(N_bytes=len(data)))
        self._inst.write_raw(data)

class MP1764C(MP176xC):
    def __init__(self, address="GPIB0::10::INSTR", dry_run=False):
        self.name = "MP1764C"

        super().__init__(address=address, dry_run=dry_run)

    def set_data_threshold_voltage(self, voltage):
        """Set the data input threshold voltage.
        
        See page 9-26 of GPIB manual (162 of PDF).
        
        - voltage: voltage of threshold, must be between -3 and 1.875. All values are rounded to 0.001."""
        if not (-3 <= voltage <= 1.875):
            raise ValueError

        self.write("DTH {:1.3f}".format(float(voltage)))

    def set_clock_phase_delay(self, delay):
        """Set the clock phase delay.
        
        See page 9-28 of GPIB manual (164 of PDF).
        
        - delay: delay in picoseconds, must be between -500 and 500. All values are rounded to 1."""
        if not (-500 <= delay <= 500):
            raise ValueError

        self.write("CPA {:g}".format(round(delay)))
        
    def get_clock_phase_delay(self):
        return float(self.query("CPA?")[3:])

    def get_data_threshold_voltage(self):
        return float(self.query("DTH?")[3:])
    
    def single(self):
        self.write("MOD 1")
    def continuous(self):
        self.write("MOD 0")
    def currentdata(self):
        self.write("CUR")
    
        
    def stop(self):
        """
        Stop acqusition
        """
        self.write("STO")
        
    def start(self):
        """
        Start acquisition
        """
        self.write("STA")
        
    def set_integration_time(self,m,s):
        """
        Sets the integration time on the EA
        """
        if s>=60:
            m = m + np.floor(s/60)
            s = np.fmod(s,60)
            
        self.write(f"PRD 0,0,{m:.0f},{s:.0f}")
        
        
    def get_error_count(self):
        
        running = True
        while running:
            response = self.query("MSR?")
            if "MSR 0" in response:
                running = False
            else:
                time.sleep(0.5)
                
        ans = self.query("EC?")
        
        if 'EC' in ans:
            EC = float(ans[5:])
            
        else:
            raise RuntimeError("get_error_count : could not get error count")
            
        return EC
        
        
    def get_error_ratio(self):
        """Get error ratio.
        
        See page 9-108 of GPIB manual (244 of PDF).
        
        Returns the error ratio as a float."""

        if self._dry_run:
            return 0
        running = True
        while running:
            response = self.query("MSR?")
            if "MSR 0" in response:
                running = False
            else:
                time.sleep(0.5)
        

        response = self.query("ER?")

        if "ER  " in response:
            BER = float(response[4:])
            
            if BER == 0.0:
                response = self.query("SLI?")
                
                if "SLI 1" in response:
                    BER = 1
        return BER
    
    def optimize_BER(self,volt_step=0.001,time_step=1,int_seq=1):
        
        cur_BER,int_sec = self.get_coarse_BER(int_seq)
# =============================================================================
#         Voltage first optimization
# =============================================================================
        
        old_BER = cur_BER
        old_volt = self.get_data_threshold_voltage()
        
        cur_volt = old_volt-volt_step
        self.set_data_threshold_voltage(cur_volt)
        cur_BER,int_sec = self.get_coarse_BER(int_sec)
        
        while cur_BER<old_BER:
            old_BER = cur_BER
            old_volt = cur_volt
            
            cur_volt = old_volt-volt_step
            self.set_data_threshold_voltage(cur_volt)
            cur_BER,int_sec = self.get_coarse_BER(int_sec)


        old_BER = cur_BER
        old_volt = self.get_data_threshold_voltage()
        
        cur_volt = old_volt+volt_step
        self.set_data_threshold_voltage(cur_volt)
        cur_BER,int_sec = self.get_coarse_BER(int_sec)

        while cur_BER<old_BER:
            old_BER = cur_BER
            old_volt = cur_volt
            
            cur_volt = old_volt+volt_step
            self.set_data_threshold_voltage(cur_volt)
            cur_BER,int_sec = self.get_coarse_BER(int_sec)  
            
        self.set_data_threshold_voltage(old_volt)

        print(f"Best BER : {old_BER:.5e} at {old_volt:.3}V")
        
        
# =============================================================================
#         Time optimization
# =============================================================================
        old_BER = cur_BER
        old_time = self.get_clock_phase_delay()
        
        cur_time = old_time-time_step
        self.set_clock_phase_delay(cur_time)
        cur_BER,int_sec = self.get_coarse_BER(int_sec)
        
        while cur_BER<old_BER:
            old_BER = cur_BER
            old_time = cur_time
            
            cur_time = old_time-time_step
            self.set_clock_phase_delay(cur_time)
            cur_BER,int_sec = self.get_coarse_BER(int_sec)


        old_BER = cur_BER
        old_time = self.get_clock_phase_delay()
        
        cur_time = old_time+time_step
        self.set_clock_phase_delay(cur_time)
        cur_BER,int_sec = self.get_coarse_BER(int_sec)

        while cur_BER<old_BER:
            old_BER = cur_BER
            old_time = cur_time
            
            cur_time = old_time+time_step
            self.set_clock_phase_delay(cur_time)
            cur_BER,int_sec = self.get_coarse_BER(int_sec)  
            
        self.set_clock_phase_delay(old_time)

        print(f"Best BER : {old_BER:.5e} at {old_time:.3}ps")        

            
        
            
        
            
    def get_coarse_BER(self,int_sec=1,max_int_sec=30):
        self.set_integration_time(0,int_sec)  
        self.single()
        self.start()
        cur_BER = self.get_error_ratio()
        
        while cur_BER==0 and int_sec<max_int_sec:
            int_sec = np.min([max_int_sec,int_sec+5])
            self.set_integration_time(0,int_sec)
            self.start()
            cur_BER = self.get_error_ratio()
            
        if cur_BER ==0:
            print("optimize_BER : Could not find an error")
        self.continuous()
        self.start()
        
        return cur_BER,int_sec
    
    
    def get_accurate_BER(self,min_error=10,int_sec=1,max_int_sec=60*60):
        self.single()
        EC = -1
        while (EC<min_error and int_sec<max_int_sec) or EC==-1:
            if EC ==0:
                int_sec = np.min([max_int_sec,np.int(int_sec*2)])
            elif EC == -1:
                int_sec = int_sec
            else:
                int_sec = np.min([max_int_sec,np.int(int_sec*min_error/EC)])
                
            
            self.set_integration_time(0,int_sec)
            self.start()
            cur_BER = self.get_error_ratio()
            EC = self.get_error_count()
            print('  ',int_sec,EC)
                
        return cur_BER,int_sec
    
    def measure_BER(self, m, s):
        if m == 0 and s == 0:
            s = 1
        self.stop()
        self.single()
        self.set_integration_time(m,s)
        self.start()
        
#        print("Waiting for measurement ({m}m:{s}s)".format(m=m, s=s))
        
        resp = self.query("MSR?")
        if "MSR 1" in resp:
            print("Measurement not done")
            return 1
        
        BER = self.get_error_ratio()
            
        self.stop()
        self.continuous()
        self.set_integration_time(0,1)
        self.start()
        self.currentdata()
        
        return BER



class MP1763C(MP176xC):
    def __init__(self, address="GPIB0::3::INSTR", dry_run=False):
        self.name = "MP1763C"

        super().__init__(address=address, dry_run=dry_run)
        
        


BIT_FLIP_TABLE = [None] * 2**8

for _i in range(2**8):
    BIT_FLIP_TABLE[_i] = int('{:08b}'.format(_i)[::-1], 2)

import random
from itertools import zip_longest

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)

def rearrange_bit_pattern(data):
    """Rearrange bit pattern to match the transmission bit pattern. See app note on high-speed pattern loading."""
    length = len(data)
    output = [None] * length

    _i = 0
    for subset in grouper(data, 2, b'\x00'):
        for d in subset[::-1]:
            output[_i] = BIT_FLIP_TABLE[d]
            _i = _i + 1

    return bytes(output)



def generate_bit_pattern(n_bits, period=1,q=0.5, run_length_limit=None):
    tmp_bits = random.choices([0, 1], k=np.int(n_bits/period), weights=[q, 1-q])
    bits = []
    for b in tmp_bits:
        bits.append(b)
        bits= bits + [0 for i in range(0,period-1)]
        

    if run_length_limit is not None:
        bits = run_length_limiter(bits, run_length_limit)

    # assuming that `bits` is your array of bits (0 or 1)
    # ordered from MSB to LSB in consecutive bytes they represent
    # e.g. bits = [1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1] will give you bytes = [128,255]
    return bytes([sum([byte[b] << abs(7 - b) for b in range(0,8)])
                for byte in zip(*(iter(bits),) * 8)
            ])
    
    
def run_length_limiter(bits, limit):
    counter = 0
    prev = None
    for _i, b in enumerate(bits):
        if b != prev:
            counter = 1
            prev = b
        elif counter == limit:
            if b == 0:
                bits[_i] = 1
                prev = 1
            else:
                bits[_i] = 0
                prev = 0
            counter = 1
        else:
            counter += 1

    return bits




if __name__=="__main__":
    ea = MP1764C()
    bpg = MP1763C()
    ea.connect()
    bpg.connect()