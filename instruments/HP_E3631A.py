# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 13:12:01 2021

@author: FTNK-LocalAdm
"""

import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))


from VisaInstrument import VisaInstrument

class HP_E3631A(VisaInstrument):
    def __init__(self,addr="GPIB0::5::INSTR",name='HP_PSU_E3631A'):
        super().__init__(addr)

        
    def getVolt(self):
        return float(self.query("MEAS:VOLT?"))
    
    def getCur(self):
        return float(self.query("MEAS:CURR?"))
    
    def getSetVolt(self):
        return float(self.query("VOLT?"))
    
    def getSetCur(self):
        return float(self.query("CURR?"))
    
    def setVolt(self,volt):
        self.write(f"VOLT {volt:.2f}")
        
    def setCur(self,volt):
        self.write(f"CURR {volt:.2f}")
        
    def disableOut(self):
        self.write("OUTP OFF")
        
    def enableOut(self):
        self.write("OUTP ON")
        
        
        

        
        
        
        
        
        
        
        
        
        
if __name__=='__main__':
    pw = HP_E3631A()
    

    
    