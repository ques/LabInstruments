# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 15:20:42 2019

@author: ques
"""
import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument
import numpy as np
import time

class OSA(VisaInstrument):
    
    def __init__(self,address = "GPIB0::21::INSTR"):
        super().__init__(address)
    
    def setTrace(self,trace="A"):
        self.write("WRT"+trace)
        self.write("DSP"+trace)
        time.sleep(0.5)
    def disableTrace(self,trace="B"):
        self.write("FIX"+trace)
        self.write("BLK"+trace)
        time.sleep(0.5)
    def setCenter(self,center):
        self.write("CTRWL{:.2f}".format(center))
        time.sleep(0.5)
        
    def setRes(self,res):
        self.write(f"RESLN{res:.2f}")
        time.sleep(0.5)
    
    def setSpan(self,span):
        self.write("SPAN{:.3f}".format(span))
        time.sleep(0.5)
        
    def setSampPoints(self,npoints):
        self.write(f"SMPL{npoints:.0f}")
        time.sleep(0.5)
    def setSensitivity(self,mode):
        if mode == "MID":
            self.write("SMID")
        elif mode == "HIGH1":
            self.write("SHI1")
        elif mode == "HIGH2":
            self.write("SHI2")
        elif mode == "HIGH3":
            self.write("SHI3")
            
        else:
            print(mode)
            raise RuntimeError(f"OSA.setSensitivitiy : wrong sensitivity name : {mode:s}. Should be MID/HIGH1/HIGH2/HIGH3")
        time.sleep(0.5)
            
    def getX(self,trace="A"):
        data_wavelength = self.query_ascii_values('WDAT '+trace)
        data_wavelength = data_wavelength[1:-1]
        time.sleep(0.5)
        return np.array(data_wavelength)
    
    def getY(self,trace="A"):
        data_level = self.query_ascii_values('LDAT '+trace)
        data_level = data_level[1:-1]
        time.sleep(0.5)
        return np.array(data_level)
    
    def getTrace(self,trace="A"):
        
        return self.getX(trace),self.getY(trace)
    
    def single(self):
        try:
            self.write("SGL")
        except:
            pass
    
    def repeat(self):
        try:
            self.write("RPT")
        except:
            pass    
    def stop(self):
        try:
            self.write("STP")
        except:
            pass   
        
    def enableSync(self):
        self.write("TLSSYNC1")
    
    def disableSync(self):
        self.write("TLSSYNC0")
        
    def setOptMode(self,center,trace='A',sens='HIGH1'):
        
        self.stop()
        self.setTrace(trace)
        self.setSensitivity(sens)
        self.setRes(2)
        self.setCenter(center)
        self.setSpan(0)
        self.enableSync()
        self.repeat()
        
    def takeSingle(self,center,span,sync=False,res=2,trace="A",sens="MID",npoints=1000):
        
        self.stop()
        self.setSensitivity(sens)
        self.setCenter(center)
        self.setSpan(span)
        self.setSampPoints(npoints)
        
        if sync:
            self.enableSync()
            res=2
        else:
            self.disableSync()
        self.wait()
        self.setRes(res)
        
        self.setTrace(trace)
        
        self.single()
        self.wait()
        
        X,Y = self.getTrace(trace)
        
        return X,Y
            
            
        
    
    
    def wait(self):
        stop = False
        while not stop:
            try:
                bit = self.query_ascii_values("SWEEP ?")
                if bit[0] == 0:
                    stop = True
                    time.sleep(0.1)
            except KeyboardInterrupt as e:
                print('Keyboard interruption')
                stop = True
            except:
                stop = False
                time.sleep(1)
    
    
if __name__=="__main__":
    osa = OSA()
    
    
    