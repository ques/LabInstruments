# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 15:01:48 2021

@author: FTNK-LocalAdm
"""
import os,sys
sys.path.append(os.path.dirname(__file__))

from VisaInstrument import VisaInstrument
import numpy as np
import time



class Anritsu_MS2830A(VisaInstrument):
    def __init__(self,addr="GPIB0::18::INSTR",name='Anritsu Signal Analyser'):
        super().__init__(addr) 
        
    def continuous(self):
        self.write("INIT:MODE:CONT")
    
    def single(self):
        self.write("INIT:MODE:SING")
        
    def setSpan(self,span):
        self.write(f"FREQ:SPAN {span:.0f}") 
    def getSpan(self):
        return float(self.query("FREQ:SPAN?"))
        
        
    def setCenter(self,freq):
        self.write(f"FREQ:CENT {freq:.0f}")
    def getCenter(self):
        return float(self.query("FREQ:CENT?"))
    
    def getStartFreq(self):
        return float(self.query("FREQ:STAR?"))
    def getStopFreq(self):
        return float(self.query("FREQ:STOP?"))
    
    def setBW(self,bw):
        self.write(f"BAND {bw:.0f}")  
    def getBW(self):
        return float(self.query("BAND?"))
    
    def getSweepTime(self):
        return float(self.query("SWE:TIME?"))
    
    def getAvgPoint(self):
        return int(self.query('AVER:COUN?'))
    def setAvgPoint(self,n):
        self.write(f"AVER:COUN {n:.0f}")
        
    def setAvgType(self,avgtype):
        self.write(f"TRAC1:STOR:MODE {avgtype:s}")
    
    def getAvgType(self):
        return self.query("TRAC1:STOR:MODE?")
    
    
    
    
    # def setRMSAvg(self,count):
    #     if count>1:
    #         self.write(":AVER:TYPE RMS")
    #         self.write(":AVER ON")
    #         self.write(f":AVER:COUN {count:.0f}")
    #         self.write(":AVER:CLE")
    #     else:
    #         self.write(":AVER OFF")
    #         self.write(":AVER:CLE")
            
    # def setPWRAvg(self,count):
    #     if count>1:
    #         self.write(":AVER:TYPE LOG")
    #         self.write(":AVER ON")
    #         self.write(f":AVER:COUN {count:.0f}")
    #         self.write(":AVER:CLE")
    #     else:
    #         self.write(":AVER OFF")
    #         self.write(":AVER:CLE")
        
    def getYData(self,trace=1):
        # self.write(":FORMat:TRACe:DATA ASCii")
        ans = self.query_ascii_values(f"TRAC? TRAC{trace:.0f}")
        return np.array(ans)
    
    
    def getSpectrum(self,center,span,bw,avg=1):
            self.setCenter(center)
            self.setSpan(span)
            self.setBW(bw)
            self.setAvgPoint(avg)
            
            tmp_time = self.getSweepTime()
            avg = self.getAvgPoint()
            # bool_avg = "OFF"!=self.getAvgType()
           
            self.single()
            time.sleep(tmp_time*1.5*avg)
           
            Y = self.getYData(1)
           
            X = np.linspace(center-span/2,center+span/2,len(Y))
           
            return X,Y








if __name__=="__main__":
    
    spec = Anritsu_MS2830A()
    
    