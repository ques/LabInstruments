# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 15:09:55 2021

@author: ques
"""
import os,sys
sys.path.append(os.path.dirname(__file__))

from HP_E3631A import HP_E3631A

class Agilent_E3631A(HP_E3631A):
    def __init__(self,addr="GPIB0::5::INSTR",name='Agilent_PSU_E3631A'):
        super().__init__(addr,name=name)