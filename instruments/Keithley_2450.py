# -*- coding: utf-8 -*-
"""
Created on Tue May 11 14:37:47 2021

@author: FTNK-LocalAdm
"""

import numpy as np
import time

import os,sys
sys.path.append(os.path.dirname(__file__))


from VisaInstrument import VisaInstrument

class Keithley_2450(VisaInstrument):
    def __init__(self,addr="GPIB0::23::INSTR",name="Keithley_2450",debug=False):
        super().__init__(addr,debug=debug)
        
        self.clearBuffer()
        
    
    #Set continuous
    def continuous(self,delay=0.1):
        self.write(":TRAC:CLEAR \"defbuffer1\"")
        self.write(f"TRIG:LOAD \"SimpleLoop\", {10000:.0f}, {delay:.2e}, \"defbuffer1\"")
        self.init()
        
    def stop(self):
        self.write(":ABORT")
    
    def init(self):
        self.write("INIT")
        
    def clearBuffer(self):
        cleared = False
        while not cleared:
            try:
                ans = self.read()
                print("Not Cleared")
            except:
                cleared=True
                
                
    
                
                
    def wait(self,waitime=1000000):
        old_timeout = self.instr.timeout
        self.instr.timeout=waitime
        self.query("*OPC?")
        self.instr.timeout=old_timeout
    
    #Set output on/off
    def outOn(self):
        self.write(":OUTPut:STATe ON")
        
    def outOff(self):
        self.write(":OUTPut:STATe OFF")
        
    
        
    #Set the measurement mode
    def sensCurrent(self):
        self.write(":SENS:FUNC \"CURR\"")
    def sensVoltage(self):
        self.write(":SENS:FUNC \"VOLT\"")
    def sensResistance(self):
        self.write(":SENS:FUNC \"RES\"")
        
    #Set the source mode
    def sourceCurrent(self):
        self.write("SOUR:FUNC CURR")
    def sourceVoltage(self,cur_lim):
        self.write("SOUR:FUNC VOLT")
        self.write("SOUR:VOLT:READ:BACK ON")
        self.write(f"SOUR:VOLT:ILIM {cur_lim:.5e}")
        
    #Set Value for the voltage
    def setVoltage(self,volt):
        self.write(f"SOUR:VOLT {volt:.5f}")
        self.measureVolt(1,0)
    def setCurrent(self,curr):
        self.write(f"SOUR:CURR {curr:.5f}")
        self.measureCurr(1,0)
        
    def setCount(self,count):
        self.write(f":COUN {count:.0f}")

    def clearTrace(self,buffer_name=None):
        if buffer_name is None:
            self.write(":TRAC:CLEAR")
        else:
            self.write(":TRAC:CLEAR \"{buffer_name:s}\"")
            
            
            
    def makeBuffer(self,buffer_name,npoint):
        self.write(f"TRAC:MAKE \"{buffer_name:s}\", {npoint*1.1:.0f}")
        self.write(f"TRAC:CLEAR \"{buffer_name:s}\"")   
        
    def delBuffer(self,buffer_name):
        self.write(f"TRAC:DEL \"{buffer_name:s}\"")
        
        
    def measureVolt(self,count=1,delay=0):    
        if count==1:
            ans = float(self.query(":MEAS:VOLT?"))
        else:
            
            buffer = "voltageCount"
            self.makeBuffer(buffer, count)
            self.sensVoltage()
            self.write(f"TRIG:LOAD \"SimpleLoop\", {count:.0f}, {delay:.2e}, \"{buffer:s}\"")
            self.init()
            self.wait()
            ans = self.query(f"TRAC:DATA? 1, {count:.0f}, \"{buffer:s}\"")
            self.delBuffer(buffer)
            
            ans = ans.split(',')
            ans = np.array(ans,dtype=float)
            
        return ans  
    
    
    
    def measureCurr(self,count=1,delay=0):
        if count==1:
            ans = float(self.query(":MEAS:CURR?"))
        else:
            
            buffer = "voltageCount"
            self.makeBuffer(buffer, count)
            self.sensCurrent()
            self.write(f"TRIG:LOAD \"SimpleLoop\", {count:.0f}, {delay:.2e}, \"{buffer:s}\"")
            self.init()
            self.wait()
            ans = self.query(f"TRAC:DATA? 1, {count:.0f}, \"{buffer:s}\"")
            self.delBuffer(buffer)
            
            ans = ans.split(',')
            ans = np.array(ans,dtype=float)
            
        return ans  

    def measureRes(self,count=1,delay=0):
        if count==1:
            ans = float(self.query(":MEAS:RES?"))
        else:
            
            buffer = "voltageCount"
            self.makeBuffer(buffer, count)
            self.sensResistance()()
            self.write(f"TRIG:LOAD \"SimpleLoop\", {count:.0f}, {delay:.2e}, \"{buffer:s}\"")
            self.init()
            self.wait()
            ans = self.query(f"TRAC:DATA? 1, {count:.0f}, \"{buffer:s}\"")
            self.delBuffer(buffer)
            
            ans = ans.split(',')
            ans = np.array(ans,dtype=float)
            
        return ans           
        
                
    def voltageSweep(self,start,stop,step,cur_lim,cur_res=None,delay=-1,count=1,dual=False):
        # self.clearBuffer()
        self.write("*CLS")
        self.write("*RST")
        self.wait()
        
        if dual:
            dual = "ON"
        else:
            dual = "OFF"
        
        setvolt = np.arange(start,stop+step/2,step)
        npoint = len(setvolt)

            
        buffer = "voltageSweep"
        self.write(f"TRAC:MAKE \"{buffer:s}\", {npoint*1.1:.0f}")
        self.write(f"TRAC:CLEAR \"{buffer:s}\"")
        self.wait()
        
        
        self.sourceVoltage(cur_lim)
        self.sensCurrent()
        self.write(f"SOUR:VOLT:RANGE {np.max(np.abs([start,stop])):.5e}")
        if cur_res is None:
            self.write("SENS:CURR:RANG:AUTO ON")
        else:
            self.write("SENS:CURR:RANG:AUTO OFF")
            self.write(f"SENS:CURR:RANG {cur_res:.5e}")
        # levels_forward = [f"{lev:.5e}, " for lev in setvolt]
        # levels_backward = [f"{lev:.5e}, " for lev in np.flip(setvolt)]
        # if dual :
        #     level = np.concatenate((levels_forward,levels_backward))
        # else:
        #     level = levels_forward
            
        # level = ''.join(level)
        # level=level[:-2]
        # # print(level)
        
        # self.write(f":SOUR:LIST:VOLT "+level)
        # self.write(f":SOUR:SWE:VOLT:LIST 1, {delay:.5e}, {count:.0f}, ON, \"{buffer:s}\"")
            
        
        self.write(f":SOUR:SWE:VOLT:LIN:STEP {start:.5e}, {stop:.5e}, {np.abs(step):.5e}, {delay:.5e}, {count:.0f}, BEST, ON, {dual:s}, \"{buffer:s}\"")
        self.wait()
        self.init()
        
        self.wait()
        
        
        npoint = float(self.query(f"TRAC:ACT? \"{buffer:s}\""))
        volt = self.query(f"TRAC:DATA? 1, {npoint:.0f}, \"{buffer:s}\", SOUR")
        volt = np.array(volt.split(','),dtype=float)
        cur = self.query(f"TRAC:DATA? 1, {npoint:.0f}, \"{buffer:s}\", READ")
        cur = np.array(cur.split(','),dtype=float)

        self.write(f"TRAC:DEL \"{buffer:s}\"")
        
        return volt,cur
    
    
    def saveVoltageSweep(self,filename,start,stop,step,cur_lim,cur_res=None,delay=0.1):
        volt,cur = self.voltageSweep(start,stop,step,cur_lim,cur_res,delay)
        
        np.savetxt(filename,np.vstack((volt,cur)).T,header=f'Delay={delay:.3e}\nCurrent Limit={cur_lim:.5e}A\n Voltage [V]    Current [A]')
      
        return volt,cur
        
        
if __name__=="__main__":
    keith = Keithley_2450(debug=True)