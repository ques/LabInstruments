# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 09:37:28 2021

@author: ques
"""
import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))


from VisaInstrument import VisaInstrument


class SCFiberSwitch(VisaInstrument):
    def __init__(self,addr="GPIB0::8::INSTR",name="FiberSwitch"):
        super().__init__(addr)

    def switchTo(self,chan):
        self.instr.write(f"A{chan:.0f}E")
        time.sleep(0.5)