# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 09:11:58 2019

@author: ques
"""
import os,sys
sys.path.append(os.path.dirname(__file__))


from VisaInstrument import VisaInstrument
import numpy as np
import matplotlib.pyplot as plt

import time


class Keithley(VisaInstrument):
    def __init__(self,address="GPIB0::16::INSTR",name='Keithley_PSU'):
        super().__init__(address)
        
        self.running=False
        
        
        
    def setV(self,volt):
        #Set DC mode
        self.write("F0,0X")
        self.write("B{:.3f},0,500X".format(volt))

        
    
    def setI(self,current):
        pass
    
    def getVDC(self):
        """
        Get Voltage in DC mode
        """
        self.write("G1,2,0X")
        return np.float(self.query(""))
        
    def getIDC(self):
        """
        Get current in DC mode
        """
        self.write("G4,2,0X")
        return np.float(self.query(""))
    
    
    def monitor(self,volt,sleeptime):
        
        self.running = True
        current = np.array([])
        voltage = np.array([])
        duration = np.array([])
        
        def handle_close(evt):
            self.disable()
            self.running = False
            
        self.setV(volt)
        self.enable()
        self.trigger()
        fig,ax1 = plt.subplots()
        ax1.set_xlabel('Time [s]',fontsize=18)
        ax1.set_ylabel('Current [A]',fontsize=18)
        ax2 = ax1.twinx()
        ax2.set_ylabel('Voltage [V]',fontsize=18)
        for label in ax1.get_yticklabels():
            label.set_fontsize(18)
        for label in ax1.get_xticklabels():
            label.set_fontsize(18)
        for label in ax2.get_yticklabels():
            label.set_fontsize(18)
        fig.canvas.mpl_connect('close_event',handle_close)
        
        deltatime = 0
        start = time.time()
        while self.running:
            duration = np.append(duration,deltatime)
            current = np.append(current,self.getIDC())
            voltage = np.append(voltage,self.getVDC())
            p = ax1.get_lines()
            for el in p:
                el.remove()
            p = ax2.get_lines()
            for el in p:
                el.remove()
            ax1.plot(duration,current,'b',label='Current [mA]')
            ax2.plot(duration,voltage,'r',label='Voltage [V]')

            plt.pause(sleeptime)
            stop = time.time()
            deltatime = np.float(stop-start)
            
        return duration,current,voltage
    
    def linSweepV(self,start,stop,step,delay=5):
        #Disable the operation
        self.write("N0X")
        #Set in sweep voltage mode
        self.write("F0,1X") 
        #Set the integration time to 4msec
        self.write("S1X")
        #Set the type of sweep : linear stair from start to stop with step of step        
        self.write("Q1,{:.2f},{:.2f},{:.2f},0,{:.0f}X".format(start,stop,step,delay))
        
        
        #Activate the trigger
        self.write("R1X")
        #Enable the operation
        self.write("N1X")
        #Send trigger for measurement
        self.write("H0X")
        
        
        #Loop to wait for the sweep to be finished
        timeout = True
        self.write("G4,2,2X")
        while timeout:
            try:
                self.query("")
                timeout = False
            except:
                timeout = True
            
            
        
        
        
        #Specify the output for source and measure and get the ASCII data
        self.write("G1,2,2X")
        voltString = self.query("")
        self.write("G4,2,2X")
        currString = self.query("")
        
        #Go in standby
        self.write("N0X")
        
        volt = np.array([np.float(el) for el in voltString.split(',')])
        curr = np.array([np.float(el) for el in currString.split(',')])
        
        return curr,volt
    
    def cstSweepVolt(self,volt,count,delay):
        #Disable the operation
        self.write("N0X")
        #Set in sweep voltage mode
        self.write("F0,1X") 
        #Set the integration time to 4msec
        self.write("S1X")
        #Set the type of sweep : linear stair from start to stop with step of step        
        self.write("Q0,{:.2f},0,{:.0f},{:.0f}X".format(volt,delay,count))
        
        
        #Activate the trigger
        self.write("R1X")
        #Enable the operation
        self.write("N1X")
        #Send trigger for measurement
        self.write("H0X")
        
        
        #Loop to wait for the sweep to be finished
        timeout = True
        self.write("G4,2,2X")
        while timeout:
            try:
                self.query("")
                timeout = False
            except:
                timeout = True
            
            
        
        
        
        #Specify the output for source and measure and get the ASCII data
        self.write("G1,2,2X")
        voltString = self.query("")
        self.write("G4,2,2X")
        currString = self.query("")
        
        #Go in standby
        self.write("N0X")
        
        volt = np.array([np.float(el) for el in voltString.split(',')])
        curr = np.array([np.float(el) for el in currString.split(',')])
        
        return curr,volt
    
    
    def getMeasureType(self):
        pass
    def getMeasureFunction(self):
        pass
    
    def switchMeasureType(self):
        pass
        
    def switchMeasureFunction(self):
        pass
    
    def enable(self):
        self.write("N1X")    
    def disable(self):
        self.write("N0X")
    def trigger(self):
        self.write("H0X")
        
    def enable_trigger(self):
        #Activate the trigger
        self.write("R1X")
    def disable_trigger(self):
        self.write("R0X")
    
    
    
    
if __name__=="__main__":
    keith = Keithley()
    