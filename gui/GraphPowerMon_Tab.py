# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 11:01:55 2021

@author: ques
"""

import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))



from PyQt5.QtWidgets import QGridLayout,QLCDNumber,QPushButton,QLabel,QGroupBox
from PyQt5.QtWidgets import QDoubleSpinBox,QRadioButton,QVBoxLayout,QHBoxLayout
from PyQt5.QtCore import pyqtSignal


import threading

from Basic_Tab import BasicTab
from MatPlotLib_Widget import FigureCanvas


class GraphPowerMonitorTab(BasicTab):
    
    stop_mon_sig = pyqtSignal(name="Stop Monitoring")
    start_mon_sig = pyqtSignal(name="Start Monitoring")
    
    
    def __init__(self,powermeter,title="Graph Power Monitoring",nx=100,sleep=0.01):
        if powermeter is None:
            title+=" Virtual"
        super().__init__(title)
        self.powermeter = powermeter
        self.sleep=sleep
        self.nx=nx

        self.thread = None
        
        self.initUI()

    def initUI(self):
          
        self.main_layout = QGridLayout()
        self.setLayout(self.main_layout)
          
        self.toggle_mon_button = QPushButton("Start Monitoring")
        self.clear_button = QPushButton("Clear Plot")
        self.main_layout.addWidget(self.toggle_mon_button,0,0)
        self.main_layout.addWidget(self.clear_button,0,1)
        
        if self.powermeter is None:
            self.plot_canvas = FigureCanvas(3,nx=self.nx)
        else:
            self.plot_canvas = FigureCanvas(len(self.powermeter.channels),nx=self.nx,yrange=(-70,10))
        self.main_layout.addWidget(self.plot_canvas,1,0,1,2)
          
        self.toggle_mon_button.pressed.connect(self.toggle_mon)
        self.clear_button.pressed.connect(lambda :self.plot_canvas.clear_plot())   
        
          
            
       
        
    def toggle_mon(self):
        if self.thread is None or self.thread.running==False:
            self.start_mon()
        else:
            self.stop_mon()       
       
    def start_mon(self):
        self.toggle_mon_button.setChecked(True)
        self.toggle_mon_button.setText("Stop Monitoring")
        self.toggle_mon_button.repaint()
        
        def fct(powermeter,canvas,sleep):
            counter=0
            thread = threading.currentThread()
            while thread.running:
                counter+=1
                if powermeter is not None:
                    pwrs = []
                    for i,chan in enumerate(self.powermeter.channels):
                        pwrs.append(powermeter.getPower(chan))
                    canvas.update_lines(pwrs)
                else:
                    pwr = np.random.uniform(0.1,0.9,size=3)
                    canvas.update_lines(pwr)
                    
                time.sleep(sleep)
        
        self.thread = threading.Thread(target=fct,name="PowerMonitor",args=(self.powermeter,self.plot_canvas,self.sleep,))
        self.thread.running = True
        self.thread.start()
        self.start_mon_sig.emit()
        
        
       
       
    def stop_mon(self):
        self.thread.running = False
        self.thread.join()
        
        self.toggle_mon_button.setChecked(False)
        self.toggle_mon_button.setText("Start Monitoring")
        self.toggle_mon_button.repaint()
        
        self.stop_mon_sig.emit()  
        
        
       
       
if __name__=="__main__":
    from PyQt5.QtWidgets import QApplication
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    ex = GraphPowerMonitorTab(powermeter=None)
    ex.show()
    if any('SPYDER' in name for name in os.environ):
        pass
    else:
        sys.exit(app.exec_())       
       
       