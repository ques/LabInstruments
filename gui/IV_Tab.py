# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 08:49:06 2021

@author: ques
"""

import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))


from PyQt5.QtWidgets import QPushButton,QDoubleSpinBox,QLabel,QGroupBox,QSpinBox
from PyQt5.QtWidgets import QVBoxLayout,QHBoxLayout,QComboBox,QLineEdit,QFileDialog
from PyQt5.QtCore import pyqtSignal

from Basic_Tab import BasicTab


       
class IVTab(BasicTab):
    stop_IV = pyqtSignal(name="Stop IV Sweep")
    start_IV = pyqtSignal(name="Start IV Sweep")
    
    
    def __init__(self,psu,osatab,santectab,title="IV Sweep",sleep=0.5):
        if psu is None or osatab is None:
            title+=" Virtual"
        super().__init__(title)
        self.psu= psu
        self.sleep=sleep
        self.osatab = osatab
        self.santectab=santectab
        self.initUI()
        
        self.running=False
        
        
    def initUI(self):
        self.main_layout = QVBoxLayout()
        self.setLayout(self.main_layout)

        
        #IV Buttons
        self.IV_button_layout = QHBoxLayout()
        
        self.IV_button = QPushButton("Start IV")
        self.IV_button.setCheckable(True)
        self.IV_cancel = QPushButton("Cancel !!")
        
        self.IV_button_layout.addWidget(self.IV_button)
        self.IV_button_layout.addWidget(self.IV_cancel)
        
        
        
        
        #IV params
        self.IV_layout = QHBoxLayout()

        self.start_V = QDoubleSpinBox()
        self.start_V.setSuffix(" V")
        self.start_V.setRange(-25,25)
        self.stop_V = QDoubleSpinBox()
        self.stop_V.setSuffix(" V")
        self.stop_V.setRange(-25,25)
        self.step_V = QDoubleSpinBox()
        self.step_V.setSuffix(" V")
        self.step_V.setRange(-25,25)
        self.sleep_but = QDoubleSpinBox()
        self.sleep_but.setSuffix(" s")
        self.sleep_but.setRange(0.1,100000)
        self.average = QSpinBox()
        self.average.setRange(1,100)
        self.IV_layout.addWidget(QLabel("Start : "))
        self.IV_layout.addWidget(self.start_V)
        self.IV_layout.addWidget(QLabel("Stop : "))
        self.IV_layout.addWidget(self.stop_V)
        self.IV_layout.addWidget(QLabel("Step : "))
        self.IV_layout.addWidget(self.step_V)
        self.IV_layout.addWidget(QLabel("Sleep : "))
        self.IV_layout.addWidget(self.sleep_but)
        self.IV_layout.addWidget(QLabel("Averages : "))
        self.IV_layout.addWidget(self.average)

        
        self.spec_mode = QComboBox()
        self.spec_mode.addItem("No Spec")
        self.spec_mode.addItem("Single Wavelength")
        self.spec_mode.addItem("Full Spectrum - TLS Synch")
        self.spec_mode.addItem("Full Spectrum - No TLS Synch")
        self.spec_mode.addItem( "Full Spectrum - Santec Laser")
        
        
        ##Path Block
        self.filename = QLineEdit()
        self.filename.setText("IV")
        self.path = QLineEdit()
        self.path.setText("None")
        self.getPath = QPushButton("Set Save Path")
        
        self.path_layout = QHBoxLayout()
        self.path_layout.addWidget(self.path)
        self.path_layout.addWidget(self.getPath)
        self.path_layout.addWidget(self.filename)
        
        #Main Layout Adding
        self.main_layout.addLayout(self.IV_button_layout)
        self.main_layout.addLayout(self.IV_layout)
        self.main_layout.addWidget(self.spec_mode)
        self.main_layout.addLayout(self.path_layout)
        
        self.spec_mode.currentIndexChanged.connect(self.swapMode)
        self.IV_button.pressed.connect(self.takeIV)
        self.IV_cancel.pressed.connect(self.cancelIV)
        self.getPath.pressed.connect(self.updatePath)
        
        
        
        
        self.start_V.setValue(0)
        self.stop_V.setValue(1)
        self.step_V.setValue(0.1)
        self.sleep_but.setValue(1)
        self.average.setValue(1)
        
        
    def swapMode(self):
        print("Swapping MOde")
        
    def updatePath(self):
        path = QFileDialog.getExistingDirectory(None, "Set Spectrum Path", "")
        print(path)
        
        self.path.setText(path)
        
        
    def cancelIV(self):
        print("Cancel not implemented yet. Cannot Cancel")
        
        
        
    def takeIV(self):
        self.IV_button.setChecked(True)
        self.IV_button.setText("IV Acquistion Ongoing")
        self.setEnabled(False)
        self.repaint()
        self.start_IV.emit()
        
        oldOSAFilename = self.osatab.filename.text()
        oldOSAPath = self.osatab.path.text()
        
        print("Starting IV")
        self.running=True
        
        if self.spec_mode.currentIndex()==0:
            pass
        elif self.spec_mode.currentIndex()==1:
            self.osatab.span.setValue(0)
            self.osatab.repaint()
        elif self.spec_mode.currentIndex()==2:
            self.osatab.tls_synch.setChecked(True)
            self.osatab.path.setText(self.path.text())
            self.osatab.repaint()
        elif self.spec_mode.currentIndex()==3:
            self.osatab.tls_synch.setChecked(False)
            self.osatab.path.setText(self.path.text())
            self.osatab.repaint()
        elif self.spec_mode.currentIndex()==4:
            self.santectab.path.setText(self.path.text())
            self.santectab.repaint()
        
        oldVolt = self.psu.getSetVolt()
        
        sweepV = np.arange(self.start_V.value(),self.stop_V.value()+self.step_V.value()/2,self.step_V.value())
        
        sleep_break = self.sleep_but.value()
        
        I = np.zeros(sweepV.shape)
        setV =  np.zeros(sweepV.shape)
        V =  np.zeros(sweepV.shape)
        if self.spec_mode.currentIndex()==1:
            P = np.zeros(sweepV.shape)
            
        try:
            
            for i,v in enumerate(sweepV):
                if self.psu is None:
                    print(f"Setting to {v:.3f}V")
                else:
                    self.psu.setVolt(v)
                
                
    
                self.IV_button.setText(f"Ongoing : Volt={v:.3f}V")
                self.IV_button.repaint()
                time.sleep(sleep_break)
                
                tmp_i = []
                tmp_v = []
                for average_iter in np.arange(0,self.average.value()):
                    tmp_i.append(self.psu.getCur())
                    tmp_v.append(self.psu.getVolt())
                    time.sleep(0.01)
                
                I[i] = np.mean(np.array(tmp_i))
                V[i] = np.mean(np.array(tmp_v))
                setV[i] = self.psu.getSetVolt()
                print(f"Volt : {V[i]:.2f}V  Current : {I[i]*1000:.3f}mA")
                
                if self.spec_mode.currentIndex()==2 or self.spec_mode.currentIndex()==3:
                    self.osatab.filename.setText(self.filename.text()+f'_Spectrum_{v:.3f}V')
                    self.osatab.repaint()
                    self.osatab.takeSpectrum()
                if self.spec_mode.currentIndex()==1:
                    X,Y = self.osatab.osa.takeSingle(self.osatab.center.value(),0,sync=False,res=2,trace="A",sens=self.osatab.sens.currentText(),npoints=101)
                    P[i]=np.mean(Y)
                    
                if self.spec_mode.currentIndex()==4:
                    self.santectab.filename.setText(self.filename.text()+f'_Spectrum_{v:.3f}V')
                    self.santectab.repaint()
                    self.santectab.startSpectrumThread(emitSignal=False)
                    self.santectab.thread.join()
                    self.santectab.repaint()
                    self.santectab.stopSpectrumThread()
                        
          
            
            self.psu.setVolt(oldVolt)
            print("Done IV")
            
            self.osatab.filename.setText(oldOSAFilename)
            self.osatab.path.setText(oldOSAPath)
            
            if os.path.exists(self.path.text()):
                if self.spec_mode.currentIndex()==1:
                    np.savetxt(self.path.text()+'\\'+self.filename.text()+'.tab',np.vstack((V,I,setV,P)).T,header=f'Voltage[V]  Current[A]   setVoltage[V]   OSAPower[dBm]\nAverage={self.average.value():.0f}\nSleep={self.sleep_but.value():.2f}')
                else:
                    np.savetxt(self.path.text()+'\\'+self.filename.text()+'.tab',np.vstack((V,I,setV)).T,header=f'Voltage[V]  Current[A]   setVoltage[V]\nAverage={self.average.value():.0f}\nSleep={self.sleep_but.value():.2f}')
            else:
                print("Could not save because of wrong path")
        except KeyboardInterrupt as e:
            print("Cancel by user")
        except Exception as e:
            print("Error in taking IV : "+str(e))
        self.IV_button.setChecked(False)
        self.IV_button.setText("Start IV")
        self.setEnabled(True)
        self.repaint()
        self.stop_IV.emit()