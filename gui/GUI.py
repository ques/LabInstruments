# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 09:35:56 2021

@author: ques
"""

import numpy as np
import matplotlib.pyplot as plt
import time

import sys,os
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from PyQt5.QtWidgets import QGridLayout,QMainWindow,QApplication,QWidget
from PyQt5.QtWidgets import QMenuBar,QMenu,QAction
from PyQt5.QtCore import Qt




from instruments.SCFiberSwitch import SCFiberSwitch
from instruments.OSA import OSA
from instruments.Agilent_E3646 import Agilent_E3646
from instruments.Agilent_E3631A import Agilent_E3631A
from instruments.SantecLaserPowerMeter import SantecLaser,SantecPowerMeter
from instruments.HP_PowerMeter import HP_PowerMeter

from IV_Tab import IVTab
from OSA_Tab import OSATab
from Switch_Tab import SwitchTab
from PSUMon_Tab import PSUMonitorTab
from PowerMon_Tab import LCDPowerMonitorTab
from TLS_Tab import TLSTab
from GraphPowerMon_Tab import GraphPowerMonitorTab
from LaserParam_Tab import LaserParamTab





class MainWindow(QMainWindow):
    def __init__(self,virtual=False,parent=None):
        
        super().__init__(parent=parent)
        self.virtual=virtual
        
        self.connect_instr()
        self.initUI()  
        self.initMenu()
        self.initConnections()
        
        
    def initUI(self):
        self.setWindowTitle("Receiver App")

                
#        
        self.switchtab = SwitchTab(self.switch,9,port_names=["WSS","Modulator","Input","Output"])
        self.osatab = OSATab(self.osa)
        self.psuMonTab = PSUMonitorTab(self.psu)
        self.MPMTab = LCDPowerMonitorTab(self.santec_mpm,title="Santec MPM")
        self.santecTab = TLSTab(self.santec_tls,self.santec_mpm,"Santec Spectrum")
        self.laserParamTabe = LaserParamTab(self.santec_tls,"Santec Laser")
        self.powerRXTab = LCDPowerMonitorTab(self.rx_powermeter,title="Receiver Power",sleep=0.01)
        self.powerInTab = LCDPowerMonitorTab(self.input_powermeter,title="Input Power",sleep=0.01)
        
        self.powerGraphTab = GraphPowerMonitorTab(self.santec_mpm,title="Santec Graph Monitoring",nx=1000,sleep=0.01)
        self.IVtab = IVTab(self.psu,self.osatab,self.santecTab)

        
        self.mainLayout = QGridLayout()
        self.mainLayout.addWidget(self.switchtab,1,1)
        self.mainLayout.addWidget(self.osatab,1,2)
        self.mainLayout.addWidget(self.IVtab,1,3)
        self.mainLayout.addWidget(self.psuMonTab,2,1)
        self.mainLayout.addWidget(self.MPMTab,2,2)
        self.mainLayout.addWidget(self.santecTab,2,3)
        self.mainLayout.addWidget(self.powerRXTab,3,1)
        self.mainLayout.addWidget(self.powerInTab,3,2)
        self.mainLayout.addWidget(self.powerGraphTab,3,3)
        
        
        
        
        self.mainLayout.setRowStretch(4,20)
        self.mainLayout.setColumnStretch(4,20)
        
        mainWidget = QWidget()
        mainWidget.setLayout(self.mainLayout)
        
        
        self.setCentralWidget(mainWidget)
        
        
        

        
        
        
    def initMenu(self):
        
        self.menu_bar = self.menuBar()
        self.file_menu = self.menu_bar.addMenu("&File")
        self.instrument_menu = self.menu_bar.addMenu("&Instruments")
        self.connect_action = QAction("Reconnect All Instruments")
        self.instrument_menu.addAction(self.connect_action)
        self.add_psu = QAction("Add Power Supply")
        self.add_powermeter = QAction("Add Powermeter")
        
    def initConnections(self):
        
        self.MPMTab.start_mon_sig.connect(lambda : self.santecTab.setEnabled(False))
        self.MPMTab.stop_mon_sig.connect(lambda : self.santecTab.setEnabled(True))
        self.MPMTab.start_mon_sig.connect(lambda : self.powerGraphTab.setEnabled(False))
        self.MPMTab.stop_mon_sig.connect(lambda : self.powerGraphTab.setEnabled(True))        
        
        self.santecTab.start_spectrum.connect(lambda : self.MPMTab.setEnabled(False))
        self.santecTab.stop_spectrum.connect(lambda : self.MPMTab.setEnabled(True))
        self.santecTab.start_spectrum.connect(lambda : self.powerGraphTab.setEnabled(False))
        self.santecTab.stop_spectrum.connect(lambda : self.powerGraphTab.setEnabled(True))
        # self.santecTab.start_spectrum.connect(lambda : self.powerGraphTab.stop_mon)
        # self.santecTab.start_spectrum.connect(lambda : self.MPMTab.stop_mon)


        self.powerGraphTab.start_mon_sig.connect(lambda : self.MPMTab.setEnabled(False))
        self.powerGraphTab.stop_mon_sig.connect(lambda : self.MPMTab.setEnabled(True))
        self.powerGraphTab.start_mon_sig.connect(lambda : self.santecTab.setEnabled(False))
        self.powerGraphTab.stop_mon_sig.connect(lambda : self.santecTab.setEnabled(True))

        
        self.psuMonTab.start_mon_sig.connect(lambda : self.IVtab.setEnabled(False))
        self.psuMonTab.stop_mon_sig.connect(lambda : self.IVtab.setEnabled(True))
        

        
        self.IVtab.start_IV.connect(lambda : self.psuMonTab.setEnabled(False))
        self.IVtab.stop_IV.connect(lambda : self.psuMonTab.setEnabled(True))
        self.IVtab.stop_IV.connect(lambda : self.MPMTab.setEnabled(True))
        self.IVtab.stop_IV.connect(lambda : self.powerGraphTab.setEnabled(True))


        self.connect_action.triggered.connect(self.connect_instr)
        
        
        self.santecTab.calibrating_MPM.connect(self.MPMTab.reset_offsets)
        
    def connect_instr(self):
        print("Connection to the instruments")
        
        try:
            if self.virtual:
                self.switch=None
                print("Virtual Connection to Switch")
            else:
                self.switch = SCFiberSwitch()
                print("Connected to FiberSwitch")
        except Exception as e:
            print("Could not connect to switch : "+str(e))
            self.switch = None
            
        try:
            if self.virtual:
                self.osa=None
                print("Virtual Connection to OSA")
            else:
                self.osa = OSA(address = "GPIB0::21::INSTR")
                print("Connected to OSA")
        except Exception as e:
            print("Could not connect to OSA : "+str(e))
            self.osa = None        
            
        try:
            if self.virtual:
                self.psu=None
                print("Virtual Connection to PSU")
            else:
                self.psu = Agilent_E3631A(addr="GPIB0::11::INSTR")
                print("Connected to PSU")
        except Exception as e:
            print("Could not connect to PSU : "+str(e))
            self.psu = None  
            
        try:
            if self.virtual:
                self.santec_tls = None
                print("Virtual Connection to Santec Laser")
            else:
                self.santec_tls = SantecLaser()
                print("Connected to Santec Laser")
        except Exception as e:
            print("Could not connect to Santec Laser : "+str(e))
            self.santec_tls = None  
             
        try:
            if self.virtual:
                self.santec_mpm = None
                print("Virtual Connection to Santec MPM")
            else:
                self.santec_mpm = SantecPowerMeter(addr="GPIB0::16::INSTR",channels=[1,2],offsets=[41.70,0])
                print("Connected to Santec PowerMeter")
        except Exception as e:
            print("Could not connect to Santec MPM : "+str(e))
            self.santec_mpm = None     
            
        try:
            if self.virtual:
                self.rx_powermeter = None
                print("Virtual Connection to RX PowerMeter")
            else:
                self.rx_powermeter = HP_PowerMeter(addr="GPIB0::22::INSTR",channels=[1,2])
                print("Connected to RX PowerMeter")
        except Exception as e:
            print("Could not connect to RX PowerMeter: "+str(e))
            self.rx_powermeter = None       
            
        try:
            if self.virtual:
                self.input_powermeter = None
                print("Virtual Connection to Input PowerMeter")
            else:
                self.input_powermeter = HP_PowerMeter(addr="GPIB0::13::INSTR",channels=[1,2])
                print("Connected to Input PowerMeter")
        except Exception as e:
            print("Could not connect to Input PowerMeter: "+str(e))
            self.input_powermeter = None   
    
    def closeEvent(self,event):
        print("Closing Instrument App")
        QApplication.quit()
        
        
        
        

        
        

        
                

        
 
        
        

    
        
       


        


















if __name__ == "__main__":
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    ex = MainWindow()
    ex.show()

    sys.exit(app.exec_())
