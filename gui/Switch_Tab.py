# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 08:52:43 2021

@author: ques
"""


import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


from PyQt5.QtWidgets import QPushButton,QGroupBox,QGridLayout,QApplication

from Basic_Tab import BasicTab
from instruments.SCFiberSwitch import SCFiberSwitch


class SwitchTab(BasicTab):
    
    def __init__(self,switch_instr,ports=8,port_names=[],title="FiberSwitch"):
        if switch_instr is None:
            title+=" Virtual"
        super().__init__(title)
        self.connected=None
        self.ports = ports
        self.switch_instr=switch_instr
        
        self.initUI(port=ports,port_names=port_names)

        
    def initUI(self,port=8,port_names=[]):
        
        self.buttons={}
        grid = QGridLayout()
        self.setLayout(grid)
        
        names = [str(i) for i in np.arange(1,port+1)]
        label_names=[str(i) for i in np.arange(1,port+1)]
        for i,n in enumerate(port_names):
            label_names[i] = label_names[i]+f'. {n:s}'
            
        
        max_line = np.int(np.ceil(port/3))
        
        positions = [(i,j) for i in range(max_line) for j in range(3)]
        
        for position,name,label in zip(positions,names,label_names):
            if name != '':
                self.buttons.update({name:QPushButton(label)})
                grid.addWidget(self.buttons[name], *position) 
                self.buttons[name].pressed.connect(lambda x=name : self.switchTo(x))
                self.buttons[name].setCheckable(True)
                
                
        self.move(300, 150)        
        
        
        
    def switchTo(self,ID):
        ID = str(ID)
        if self.connected is not None:
            self.buttons[self.connected].setChecked(False)
            self.buttons[self.connected].setStyleSheet('QPushButton {background-color: light gray;}')
            self.buttons[self.connected].repaint()
            
            
        self.connected = ID
        self.buttons[self.connected].setChecked(True)
        self.buttons[self.connected].setStyleSheet('QPushButton {background-color: green;}')
        self.buttons[self.connected].repaint()
        
        
        if self.switch_instr is not None:
            self.switch_instr.switchTo(np.float(ID))
        else:
            print("Switched to "+ID)
            
            
            
if __name__=="__main__":
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    switch = SCFiberSwitch()
    tab = SwitchTab(switch)
    

    tab.show()

    sys.exit(app.exec_())
        
            