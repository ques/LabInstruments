# -*- coding: utf-8 -*-
"""
Created on Mon Apr 19 09:30:08 2021

@author: FTNK-LocalAdm
"""

import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


from PyQt5.QtWidgets import QPushButton,QDoubleSpinBox,QLabel,QGroupBox,QCheckBox
from PyQt5.QtWidgets import QHBoxLayout,QComboBox,QLineEdit,QFileDialog,QGridLayout

from Basic_Tab import BasicTab




class LaserParamTab(BasicTab):
    def __init__(self,laser,title="Laser Tab"):
        super().__init__(title)
        
        self.laser = laser
        
        self.main_layout = QGridLayout()      
        self.setLayout(self.main_layout)
        
        
        
        self.wave = QDoubleSpinBox()    
        self.wave.setRange(1500,1630)
        self.wave.setSuffix(" nm")
        
        
        self.pow = QDoubleSpinBox()
        self.pow.setRange(-15,15)
        self.pow.setSuffix(" dBm")
        
        self.main_layout.addWidget(QLabel("Laser Wavelength : "), 0, 0)
        self.main_layout.addWidget(self.wave, 0, 1)
        
        self.main_layout.addWidget(QLabel("Laser Power : "), 0, 0)
        self.main_layout.addWidget(self.pow, 0, 1)
        
        
        self.pow.valueChanged.connect(lambda x : self.laser.setPower(x))
        self.wave.valueChanged.connect(lambda x : self.laser.setWavelength(x))
        
        
        
        
        
        
        