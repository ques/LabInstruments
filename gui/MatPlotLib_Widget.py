# -*- coding: utf-8 -*-
"""
Created on Thu Apr 15 08:29:12 2021

@author: ques
"""

import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
import matplotlib.pyplot as plt
import matplotlib.figure as mpl_fig

from PyQt5.QtWidgets import QWidget,QDoubleSpinBox,QGridLayout,QLabel,QHBoxLayout,QVBoxLayout,QRadioButton


        
class FigureCanvas(QWidget):
    def __init__(self,n_line,yrange=(0,1),nx=None):
        super().__init__()
        self.figure = mpl_fig.Figure()
        self.ax = self.figure.add_subplot(111)
        self.ax.set_ylim(yrange)
        self.lines=[]
        self.nx = nx
        self.yrange=yrange

            
        self.main_layout = QGridLayout()
        self.canvas = FigureCanvasQTAgg(self.figure)

        self.y_lim_layout = QHBoxLayout()
        self.min_y_lim = QDoubleSpinBox()
        self.min_y_lim.setRange(-200,40)
        self.min_y_lim.setSuffix(" dBm")
        self.min_y_lim.setValue(self.yrange[0])
        self.y_lim_layout.addWidget(QLabel("Min Y"))
        self.y_lim_layout.addWidget(self.min_y_lim)
        
        self.max_y_lim = QDoubleSpinBox()
        self.max_y_lim.setRange(-200,40)
        self.max_y_lim.setSuffix(" dBm")
        self.max_y_lim.setValue(self.yrange[1])
        self.y_lim_layout.addWidget(QLabel("Max Y"))
        self.y_lim_layout.addWidget(self.max_y_lim)
        
        self.plot_layout = QVBoxLayout()
        self.plot_on_button = []  
        
        for i in np.arange(n_line):
            self.plot([0],[0])
            
        
        
        self.main_layout.addWidget(self.canvas,0,0)
        self.main_layout.addLayout(self.y_lim_layout,1,0)
        self.main_layout.addLayout(self.plot_layout,0,1)
        self.setLayout(self.main_layout)
        
        
      
        
        
        self.min_y_lim.valueChanged.connect(lambda x:[self.ax.set_ylim(x,self.ax.get_ylim()[1]),self.figure.canvas.draw()])           
        self.max_y_lim.valueChanged.connect(lambda x:[self.ax.set_ylim(self.ax.get_ylim()[0],x),self.figure.canvas.draw()]) 

        
    def clear_plot(self):
        for l in self.lines:
            l.set_xdata([0])
            l.set_ydata([0])
        self.figure.canvas.draw()
        
    def auto_scale(self):
        max_x=-np.inf
        min_x=np.inf
        max_y=-np.inf
        min_y=np.inf
        for l in self.lines:
            if l.get_linestyle()!="None":
                
                max_x =np.max([max_x,np.max(l.get_xdata())])
                min_x =np.min([min_x,np.min(l.get_xdata())])
                max_y =np.max([max_y,np.max(l.get_ydata())])
                min_y =np.min([min_y,np.min(l.get_ydata())])  
        self.ax.set_xlim(min_x,max_x)
        self.ax.set_ylim(min_y,max_y)
        self.figure.canvas.draw() 
        self.min_y_lim.setValue(min_y)
        self.max_y_lim.setValue(max_y)
        self.repaint()
            
    def update_lines(self,vals):
        if np.isscalar(vals):
            vals = np.array([vals])
        x = self.lines[0].get_xdata()
        x = np.append(x,x[-1]+1)
        if self.nx is not None:
            x = x[-self.nx:]
            
        self.ax.set_xlim(x[0],x[-1])
        
        for i,val in enumerate(vals):
            
            y = self.lines[i].get_ydata()
            y = np.append(y,val)
            if self.nx is not None:
                y = y[-self.nx:]
            self.lines[i].set_xdata(x)
            self.lines[i].set_ydata(y)
            self.figure.canvas.draw()  
            
            
    def plot(self,X,Y,label=None,line_index=None):
        if label is None:
            label = f"Line {len(self.lines):.0f}"
        
        if line_index is None or line_index >= len(self.lines):
            l, = self.ax.plot(X,Y,label=label)
            self.lines.append(l)
            self.plot_on_button.append(QRadioButton(label))
            self.plot_on_button[-1].setChecked(True)
            self.plot_on_button[-1].setAutoExclusive(False)
            self.plot_on_button[-1].pressed.connect(lambda x=len(self.plot_on_button)-1:self.toggle_line(x))
            self.plot_layout.addWidget(self.plot_on_button[-1])
        else:
            self.lines[line_index].set_xdata(X)
            self.lines[line_index].set_ydata(Y)
        self.ax.legend()
        self.figure.canvas.draw() 
        
        
    def toggle_line(self,i):
        l = self.lines[i]
        if l.get_linestyle()=="None":
            l.set_linestyle('-')
        else:
            l.set_linestyle("None")
        self.figure.canvas.draw()
            
            
            
if __name__=="__main__":
    from PyQt5.QtWidgets import QApplication
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    ex = FigureCanvas(1)
    ex.show()
#    sys.exit(app.exec_())  
      
            
            
            
            
            