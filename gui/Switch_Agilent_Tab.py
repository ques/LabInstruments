# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 08:52:43 2021

@author: ques
"""


import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


from PyQt5.QtWidgets import QPushButton,QGroupBox,QGridLayout,QApplication

from Basic_Tab import BasicTab
from instruments.Agilent_Switch import AgilentSwitch


class SwitchTab(BasicTab):
    
    def __init__(self,switch_instr,input_port_names=[],output_port_names=[],title="FiberSwitch"):
        if switch_instr is None:
            title+=" Virtual"
        super().__init__(title)
        self.input_connected=None
        self.output_connected=None
        self.switch_instr=switch_instr
        
        self.input_port_names = input_port_names
        self.output_port_names = output_port_names
        
        self.initUI(input_port_names=input_port_names,output_port_names=output_port_names)

        
    def initUI(self,input_port_names=[],output_port_names=[]):
        
        self.buttons={}
        grid = QGridLayout()
        self.setLayout(grid)
        
        for i,n in enumerate(input_port_names):
            name = str(i+1)
            
            self.buttons.update({name:QPushButton(n)})
            grid.addWidget(self.buttons[name],0,i) 
            self.buttons[name].pressed.connect(lambda x=name : self.switchTo(x))
            self.buttons[name].setCheckable(True)
        
        

                
                
        for i,n in enumerate(output_port_names):
            name = str(i+1+len(input_port_names))
            self.buttons.update({name:QPushButton(n)})
            grid.addWidget(self.buttons[name],1,i) 
            self.buttons[name].pressed.connect(lambda x=name : self.switchTo(x))
            self.buttons[name].setCheckable(True)
                
                
        self.move(300, 150)        
        
        
        
    def switchTo(self,ID):
        print(ID)
        ID = str(ID)
        
        if self.switch_instr is not None:
            if np.float(ID)<=len(self.input_port_names):
                self.switch_instr.switchTo(1,np.float(ID))
                if self.input_connected is not None:
                    self.buttons[self.input_connected].setChecked(False)
                    self.buttons[self.input_connected].setStyleSheet('QPushButton {background-color: light gray;}')
                    self.buttons[self.input_connected].repaint()
            
            
                self.input_connected = ID
                self.buttons[self.input_connected].setChecked(True)
                self.buttons[self.input_connected].setStyleSheet('QPushButton {background-color: green;}')
                self.buttons[self.input_connected].repaint()                
                
            else:
                self.switch_instr.switchTo(2,np.float(ID)-len(self.input_port_names))
                if self.output_connected is not None:
                    self.buttons[self.output_connected].setChecked(False)
                    self.buttons[self.output_connected].setStyleSheet('QPushButton {background-color: light gray;}')
                    self.buttons[self.output_connected].repaint()
            
            
                self.output_connected = ID
                self.buttons[self.output_connected].setChecked(True)
                self.buttons[self.output_connected].setStyleSheet('QPushButton {background-color: green;}')
                self.buttons[self.output_connected].repaint()  
        else:
            print("Switched to "+ID)
            
            
            
if __name__=="__main__":
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    switch = AgilentSwitch()
    tab = SwitchTab(switch,input_port_names=["Output to DUT","Input to DUT","Output To RX","OSNR"],output_port_names=["MPM","OSA","Waveanalyser"])
    

    tab.show()

    sys.exit(app.exec_())
        
            