# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 08:51:28 2021

@author: ques
"""

import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.dirname(os.path.dirname(__file__)))

import threading


from PyQt5.QtWidgets import QPushButton,QDoubleSpinBox,QLabel,QGroupBox,QCheckBox,QApplication,QLCDNumber,QRadioButton
from PyQt5.QtWidgets import QHBoxLayout,QComboBox,QLineEdit,QFileDialog,QGridLayout,QVBoxLayout
from PyQt5.QtCore import pyqtSignal

from Basic_Tab import BasicTab

from instruments.JDS_Cassette import JDS_Cassette
from instruments.HP_PowerMeter import HP_PowerMeter
from instruments.FullReceiver import RX




class RxTab(BasicTab):
    stop_mon_sig = pyqtSignal(name="Stop Monitoring")
    start_mon_sig = pyqtSignal(name="Start Monitoring")
        
    
    def __init__(self,rx,title="Attenuation Monitoring",sleep=0.1):
        super().__init__(title)
        self.rx
        
        self.sleep = sleep
        
        self.initUI()
        self.thread = None        
        
        
    def initUI(self):
        
        self.main_layout= QVBoxLayout()
        self.setLayout(self.main_layout)
        
        
        #Receiver block
        self.rx_group = QGroupBox("Received power")
        self.main_layout.addWidget(self.rx_group)
        
        
        
        
        
        self.pd_group = QGroupBox("Received power")
        self.main_layout.addWidget(self.pd_group)
#         #Monitor Block
        
#         self.p_mon = QLCDNumber()
#         self.p_mon.setDecMode()
        
#         self.toggle_mon_button = QPushButton("Start Monitoring")
        
#         self.led_active = QRadioButton("")
# #        self.toggle_mon_button.setCheckable(True)
        
#         self.p_mon = []
#         self.p_offsets = []
#         self.psu_layout.addWidget(self.toggle_mon_button,0,0,1,2)


#         if self.powermeter is not None:
#             counter=0
#             for i,chan in enumerate(self.powermeter.channels):
#                 self.p_mon.append(QLCDNumber())
#                 self.p_mon[-1].setDecMode()
#                 self.p_mon[-1].setMinimumSize(100,50)
#                 self.psu_layout.addWidget(QLabel(f"Channel {chan:.0f} [dBm] : "),counter+1,0)
#                 self.psu_layout.addWidget(self.p_mon[-1],counter+1,1)
#                 counter+=1
#                 self.p_offsets.append(QDoubleSpinBox())
#                 self.p_offsets[-1].setValue(self.powermeter.offsets[i])
#                 self.p_offsets[-1].setRange(-100,100)
#                 self.p_offsets[-1].valueChanged.connect(self.update_offsets)
#                 self.psu_layout.addWidget(QLabel(f"Offset {chan:.0f} [dBm] : "),counter+1,0)
#                 self.psu_layout.addWidget(self.p_offsets[-1],counter+1,1)
#                 counter+=1
                
#             self.psu_layout.setRowStretch(len(self.powermeter.channels)+1, 20)
#         else:
#             self.p_mon.append(QLCDNumber())
#             self.p_mon[-1].setDecMode()
#             self.p_mon[-1].setMinimumSize(100,50)
#             self.psu_layout.addWidget(QLabel("Channel 1 [dBm] : "),1,0)
#             self.psu_layout.addWidget(self.p_mon[-1],1,1)
#             self.psu_layout.setRowStretch(2, 20)
        
            
        
        
#         self.toggle_mon_button.pressed.connect(self.toggle_mon)
            
    def toggle_mon(self):
        if self.thread is None or self.thread.running==False:
            self.start_mon()
        else:
            self.stop_mon()
        
    def start_mon(self):
        self.toggle_mon_button.setChecked(True)
        self.toggle_mon_button.setText("Stop Monitoring")
        self.toggle_mon_button.repaint()
        
        def fct(powermeter,p_mon,sleep):
            
            thread = threading.currentThread()
            while thread.running:
                if powermeter is not None:
                    for i,chan in enumerate(self.powermeter.channels):
                        pwr = powermeter.getPower(chan)
                        p_mon[i].display(pwr)
                else:
                    pwr = np.random.uniform(0,4)
                    p_mon[0].display(pwr)
                    
                time.sleep(sleep)
        
        self.thread = threading.Thread(target=fct,name="PowerMonitor",args=(self.powermeter,self.p_mon,self.sleep,))
        self.thread.running = True
        self.thread.start()
        self.start_mon_sig.emit()
    
    def stop_mon(self):
        self.thread.running = False
        self.thread.join()
        
        self.toggle_mon_button.setChecked(False)
        self.toggle_mon_button.setText("Start Monitoring")
        self.toggle_mon_button.repaint()
        
        self.stop_mon_sig.emit()
        
        
    def set_volt(self):
        setVal = self.set_volt_box.value()
        self.psu.setVolt(setVal)
        
if __name__=="__main__":
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
        
    jds = JDS_Cassette()    
    tab = RxTab(jds)
    

    tab.show()

    sys.exit(app.exec_())
    