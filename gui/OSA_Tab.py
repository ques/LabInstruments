# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 08:51:28 2021

@author: ques
"""

import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.dirname(os.path.dirname(__file__)))



from PyQt5.QtWidgets import QPushButton,QDoubleSpinBox,QLabel,QGroupBox,QCheckBox,QApplication
from PyQt5.QtWidgets import QHBoxLayout,QComboBox,QLineEdit,QFileDialog,QGridLayout
from PyQt5.QtCore import pyqtSignal

from Basic_Tab import BasicTab

from instruments.OSA_Anritsu import OSA_Anritsu




class OSATab(BasicTab):
    stop_spectrum = pyqtSignal(name="Stop Spectrum")
    start_spectrum = pyqtSignal(name="Start Spectrum")
    
    def __init__(self,osa_instr,title="OSA Tab",bool_anritsu=False):
        if osa_instr is None:
            title+=" Virtual"
        super().__init__(title)
        
        self.osa = osa_instr
        
        self.bool_anritsu=bool_anritsu
        
        self.center = QDoubleSpinBox()
        self.span = QDoubleSpinBox()
        self.start_wl = QDoubleSpinBox()
        self.stop_wl = QDoubleSpinBox()
        self.res = QDoubleSpinBox()
        self.samp_points = QDoubleSpinBox()
        if self.bool_anritsu:
            self.sens = QDoubleSpinBox()
            
        else:
            self.sens = QComboBox()
            self.sens.addItems(["MID","HIGH1","HIGH2"])
        
        self.center_lab = QLabel("Center[nm] : ")
        self.span_lab = QLabel("Span[nm] : ")
        self.start_wl_lab = QLabel("Start[nm] : ")
        self.stopt_wl_lab = QLabel("Stop[nm] : ")
        self.res_lab = QLabel("Resolution[nm] : ")
        self.point_lab = QLabel("Sample Points : ")
        if self.bool_anritsu:
            self.sens_lab = QLabel("Raw Bandwidth : ")
            
        else: 
            self.sens_lab = QLabel("Sensitivity : ")
        self.center_layout = QHBoxLayout()
        self.span_layout = QHBoxLayout()
        self.start_wl_layout = QHBoxLayout()
        self.stop_wl_layout = QHBoxLayout()
        self.res_layout = QHBoxLayout()
        self.sens_layout = QHBoxLayout()
        self.point_layout = QHBoxLayout()

        
        self.center_layout.addStretch(1)
        self.center_layout.addWidget(self.center_lab)
        self.center_layout.addWidget(self.center)

        self.span_layout.addStretch(1)
        self.span_layout.addWidget(self.span_lab)
        self.span_layout.addWidget(self.span)

        self.start_wl_layout.addStretch(1)
        self.start_wl_layout.addWidget(self.start_wl_lab)
        self.start_wl_layout.addWidget(self.start_wl)
        
        self.stop_wl_layout.addStretch(1)
        self.stop_wl_layout.addWidget(self.stopt_wl_lab)
        self.stop_wl_layout.addWidget(self.stop_wl)
        
        self.res_layout.addStretch(1)
        self.res_layout.addWidget(self.res_lab)
        self.res_layout.addWidget(self.res)

        self.sens_layout.addStretch(1)
        self.sens_layout.addWidget(self.sens_lab)
        self.sens_layout.addWidget(self.sens)


        self.point_layout.addStretch(1)
        self.point_layout.addWidget(self.point_lab)
        self.point_layout.addWidget(self.samp_points)

        
        self.take_spectrum = QPushButton("Take Spectrum")
        self.take_spectrum.setCheckable(True)
        self.continuous = QPushButton("Continous")
        
        self.filename = QLineEdit()
        self.filename.setText("Spectrum")
        
        self.path = QLineEdit()
        self.path.setText("None")
        
        self.getPath = QPushButton("Set Save Path")
        
        
        self.path_layout = QHBoxLayout()
        self.path_layout.addWidget(self.path)
        self.path_layout.addWidget(self.getPath)
        self.path_layout.addWidget(self.filename)
        
        
        self.tls_synch = QCheckBox()
        self.tls_synch.setText("TLS Synch")
        

        
        
        
        grid = QGridLayout()
        self.setLayout(grid)
        grid.addWidget(self.take_spectrum,0,0)
        grid.addWidget(self.tls_synch,0,1)
        grid.addWidget(self.continuous,0,2)
        grid.addLayout(self.center_layout,1,0)
        grid.addLayout(self.span_layout,1,1)
        grid.addLayout(self.start_wl_layout,2,0)
        grid.addLayout(self.stop_wl_layout,2,1)
        grid.addLayout(self.res_layout,3,0)
        grid.addLayout(self.sens_layout,3,1)
        grid.addLayout(self.point_layout,3,2)
        grid.addLayout(self.path_layout,4,0,1,2)
        
        
        
        self.center.setRange(0,10000)
        self.span.setRange(0,1000)
        self.start_wl.setRange(600,4000)
        self.stop_wl.setRange(600,4000)
        if self.bool_anritsu:
            self.res.setRange(0,1)
            self.sens.setRange(10,1000000)
            
            
        self.samp_points.setDecimals(0)
        self.samp_points.setRange(101, 10001)
        
        
        
        self.center.valueChanged.connect(self.updateFromCenterSpan)
        self.span.valueChanged.connect(self.updateFromCenterSpan)
        self.start_wl.valueChanged.connect(self.updateFromStartStop)
        self.stop_wl.valueChanged.connect(self.updateFromStartStop)
        
        self.getPath.pressed.connect(self.updatePath)
        self.take_spectrum.pressed.connect(self.takeSpectrum)
        self.continuous.pressed.connect(self.takeContinuous)
        
        
        self.center.setValue(1550)
        self.span.setValue(100)
        self.res.setValue(2)
        if self.bool_anritsu:
            self.sens.setValue(10)
            self.res.setValue(0.5)
        self.samp_points.setValue(1001)
            
        
    def takeContinuous(self):
        self.osa.takeContinuous(self.center.value(),self.span.value(),sync=self.tls_synch.isChecked(),res=self.res.value(),trace="A",rbw=self.sens.value(),npoints=self.samp_points.value())


    def updateFromCenterSpan(self):
        center = self.center.value()
        span = self.span.value()
        
        self.start_wl.blockSignals(True)
        self.stop_wl.blockSignals(True)
        self.start_wl.setValue(center-span/2)
        self.stop_wl.setValue(center+span/2)
        self.start_wl.blockSignals(False)
        self.stop_wl.blockSignals(False)
        
        
    def updateFromStartStop(self):

        start = self.start_wl.value()
        stop = self.stop_wl.value()
        
        self.center.blockSignals(True)
        self.span.blockSignals(True)
        self.center.setValue(start+(stop-start)/2)
        self.span.setValue(stop-start)
        self.center.blockSignals(False)
        self.span.blockSignals(False)
        
        
    def updatePath(self):
        path = QFileDialog.getExistingDirectory(None, "Set Spectrum Path", "")
        print(path)
        
        self.path.setText(path)
        
        
        
    def takeSpectrum(self,filename=None,path=None):

        self.take_spectrum.setText("OSA Acquisition Ongoing")
        self.take_spectrum.setChecked(True)
        self.setEnabled(False)
        self.repaint()
        
        sync = self.tls_synch.isChecked()
        
        if filename is None:
            filename = self.filename.text()
        if path is None:
            path = self.path.text()
            
        
        if self.osa is None:
            time.sleep(2)
        else:
            if self.bool_anritsu:
                X,Y = self.osa.takeSingle(self.center.value(),self.span.value(),sync=sync,res=self.res.value(),trace="A",rbw=self.sens.value(),npoints=self.samp_points.value())
                if os.path.exists(path):
                    np.savetxt(path+'\\'+filename+'.tab',np.vstack((X,Y)).T,header=f"Center={self.center.value():.3f}nm Span={self.span.value():.3f}nm Resolution={self.res.value():.3f}nm Sensitivity={self.sens.value():.3f}")
                else:
                    print("Invalid Spectrum Path")
            else:
                X,Y = self.osa.takeSingle(self.center.value(),self.span.value(),sync=sync,res=self.res.value(),trace="A",sens=self.sens.currentText(),npoints=self.samp_points.value())
                if os.path.exists(path):
                    np.savetxt(path+'\\'+filename+'.tab',np.vstack((X,Y)).T,header=f"Center={self.center.value():.3f}nm Span={self.span.value():.3f}nm Resolution={self.res.value():.3f}nm Sensitivity={self.sens.currentText():s}")
                else:
                    print("Invalid Spectrum Path")
        print("Taking spectrum")
        
        
        self.take_spectrum.setChecked(False)
        self.take_spectrum.setText("Take Spectrum")
        self.setEnabled(True)
        self.repaint()
        
if __name__=="__main__":
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    osa = OSA_Anritsu()
    tab = OSATab(osa,bool_anritsu=True)
    

    tab.show()

    sys.exit(app.exec_())
    