# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 08:51:28 2021

@author: ques
"""

import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


from PyQt5.QtWidgets import QPushButton,QDoubleSpinBox,QLabel,QGroupBox,QCheckBox,QMessageBox
from PyQt5.QtWidgets import QHBoxLayout,QComboBox,QLineEdit,QFileDialog,QGridLayout,QDialog
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QApplication

from Basic_Tab import BasicTab
from MatPlotLib_Widget import FigureCanvas

from instruments.SantecLaserPowerMeter import getSpectrum,SantecLaser,SantecPowerMeter
from gui.PowerMon_Tab import LCDPowerMonitorTab
from gui.GraphPowerMon_Tab import GraphPowerMonitorTab

import threading


class TLSTab(BasicTab):
    stop_spectrum = pyqtSignal(name="Stop Spectrum")
    start_spectrum = pyqtSignal(name="Start Spectrum")
    
    calibrating_MPM = pyqtSignal(name="MPM Calibration")
    
    def __init__(self,santec_laser,santec_powermeter,title="TLS Tab"):
        if santec_laser is None or santec_powermeter is None:
            title+=" Virtual"
        super().__init__(title)
        
        self.laser = santec_laser
        self.powermeter = santec_powermeter
        
        self.mon_tab = LCDPowerMonitorTab(self.powermeter)
        self.graph_tab = GraphPowerMonitorTab(self.powermeter)
        
        self.running=False
        
        self.center = QDoubleSpinBox()
        self.span = QDoubleSpinBox()
        self.span.setRange(0,130)
        self.start_wl = QDoubleSpinBox()
        self.start_wl.setRange(1500,1630)
        self.stop_wl = QDoubleSpinBox()
        self.stop_wl.setRange(1500,1630)
        self.res = QDoubleSpinBox()
        self.speed = QDoubleSpinBox()
        
        self.center_lab = QLabel("Center[nm] : ")
        self.span_lab = QLabel("Span[nm] : ")
        self.start_wl_lab = QLabel("Start[nm] : ")
        self.stopt_wl_lab = QLabel("Stop[nm] : ")
        self.res_lab = QLabel("Step[nm] : ")
        self.speed_lab = QLabel("Speed [nm/s] : ")
        
        self.center_layout = QHBoxLayout()
        self.span_layout = QHBoxLayout()
        self.start_wl_layout = QHBoxLayout()
        self.stop_wl_layout = QHBoxLayout()
        self.res_layout = QHBoxLayout()
        self.sens_layout = QHBoxLayout()

        
        self.center_layout.addStretch(1)
        self.center_layout.addWidget(self.center_lab)
        self.center_layout.addWidget(self.center)

        self.span_layout.addStretch(1)
        self.span_layout.addWidget(self.span_lab)
        self.span_layout.addWidget(self.span)

        self.start_wl_layout.addStretch(1)
        self.start_wl_layout.addWidget(self.start_wl_lab)
        self.start_wl_layout.addWidget(self.start_wl)
        
        self.stop_wl_layout.addStretch(1)
        self.stop_wl_layout.addWidget(self.stopt_wl_lab)
        self.stop_wl_layout.addWidget(self.stop_wl)
        
        self.res_layout.addStretch(1)
        self.res_layout.addWidget(self.res_lab)
        self.res_layout.addWidget(self.res)

        self.sens_layout.addStretch(1)
        self.sens_layout.addWidget(self.speed_lab)
        self.sens_layout.addWidget(self.speed)


        
        self.take_spectrum_but = QPushButton("Take Spectrum")
        self.take_spectrum_but.setCheckable(True)
        
        self.calibrate_but = QPushButton("Calibrate PowerMeter")
        
        self.filename = QLineEdit()
        self.filename.setText("Spectrum")
        
        self.path = QLineEdit()
        self.path.setText("None")
        
        self.getPath = QPushButton("Set Save Path")
        
        
        self.path_layout = QHBoxLayout()
        self.path_layout.addWidget(self.path)
        self.path_layout.addWidget(self.getPath)
        self.path_layout.addWidget(self.filename)
    
        #Add the Figure Canvas
        self.plot_canvas = FigureCanvas(0,nx=None)

        
        
        
        grid = QGridLayout()
        self.setLayout(grid)
        grid.addWidget(self.take_spectrum_but,0,0)
        grid.addWidget(self.calibrate_but,0,1)
        grid.addLayout(self.center_layout,1,0)
        grid.addLayout(self.span_layout,1,1)
        grid.addLayout(self.start_wl_layout,2,0)
        grid.addLayout(self.stop_wl_layout,2,1)
        grid.addLayout(self.res_layout,3,0)
        grid.addLayout(self.sens_layout,3,1)
        grid.addLayout(self.path_layout,4,0,1,2)
        grid.addWidget(self.plot_canvas,5,0,1,2)
        grid.addWidget(self.mon_tab,6,0,1,2)
        grid.addWidget(self.graph_tab,7,0,1,2)
        
        
        
        self.center.setRange(0,10000)
        self.span.setRange(0,1000)
        self.start_wl.setRange(600,4000)
        self.stop_wl.setRange(600,4000)
        self.res.setRange(0.0001,1)
        self.res.setDecimals(3)
        self.speed.setRange(0.1,100)
        
        
        self.center.valueChanged.connect(self.updateFromCenterSpan)
        self.span.valueChanged.connect(self.updateFromCenterSpan)
        self.start_wl.valueChanged.connect(self.updateFromStartStop)
        self.stop_wl.valueChanged.connect(self.updateFromStartStop)
        
        self.thread = None
        
        self.getPath.pressed.connect(self.updatePath)
        self.take_spectrum_but.pressed.connect(self.startSpectrumThread)
        self.stop_spectrum.connect(self.stopSpectrumThread)
        self.calibrate_but.pressed.connect(lambda x=[1,2] : self.calibrateOffset(x))
        
        self.mon_tab.start_mon_sig.connect(lambda : self.take_spectrum_but.setEnabled(False))
        self.mon_tab.stop_mon_sig.connect(lambda : self.take_spectrum_but.setEnabled(True))
        
        self.center.setValue(1550)
        self.span.setValue(100)
        self.res.setValue(0.1)
        self.speed.setValue(10)
        
        
    def startSpectrumThread(self,emitSignal=True):
        self.setEnabled(False)
        self.take_spectrum_but.setText("TLS Acquisition Ongoing")
        self.take_spectrum_but.setChecked(True)
        self.setEnabled(False)
        self.repaint()
        
        
        self.thread = threading.Thread(target=self.takeSpectrum,name="Take Spectrum",args=(emitSignal,))
        self.thread.data = None
        self.thread.start()
        self.running=True
        
        
        
        
    def stopSpectrumThread(self,filename=None,path=None):
        print("Gather spectrum")
        self.thread.join()
        dat = self.thread.data
        if dat is None:
            ans = QMessageBox.critical(None, "Error in spectrum", "the specturm could not be recovered and hasn't been saved")
            print("Could not get data from thread. Error in spectrum acquisition")
        else:
        
            if filename is None:
                filename = self.filename.text()
            if path is None:
                path = self.path.text()
            
            
            for i,d in enumerate(dat[1:]):
                self.plot_canvas.plot(dat[0], d,label=self.powermeter.channel_names[i],line_index=i)
            self.plot_canvas.auto_scale()
        
            if os.path.exists(path):
                save_dat = dat[0]
                for d in dat[1:]:
                    save_dat = np.vstack((save_dat,d))
                    
                    
                if os.path.exists(path+'\\'+filename+'.tab'):
                    d = QDialog()
                    layout = QGridLayout()
                    d.setLayout(layout)
    
                    layout.addWidget(QLabel("Overwrite file ?"),0,0,1,2)
                    yes = QPushButton("Yes")
                    no = QPushButton("No")
                    layout.addWidget(yes,1,0)
                    layout.addWidget(no,1,1)
                    d.setWindowTitle("Dialog")
                    
                    yes.pressed.connect(d.accept)        
                    no.pressed.connect(d.reject)
                    
                    d.exec_()
                    if d.result()==0 :
                        print("Didn't overwrite")
                        
                    elif d.result()==1:
                        np.savetxt(path+'\\'+filename+'.tab',save_dat.T,header=f"Laser Power={self.laser.getPower():.3f}\nStart WL={self.start_wl.value():.3f}nm\nStop WL={self.stop_wl.value():.3f}nm\nStep={self.res.value():.3f}nm\nSpeed={self.speed.value():.3f}nm/s")
                        print("Saved as : "+path+'\\'+filename+'.tab',)
                    
    
                else:
                    np.savetxt(path+'\\'+filename+'.tab',save_dat.T,header=f"Laser Power={self.laser.getPower():.3f}\nStart WL={self.start_wl.value():.3f}nm\nStop WL={self.stop_wl.value():.3f}nm\nStep={self.res.value():.3f}nm\nSpeed={self.speed.value():.3f}nm/s")
                    print("Saved as : "+path+'\\'+filename+'.tab',)
            else:
                print("Could not save file : wrong path")
        
        self.take_spectrum_but.setChecked(False)
        self.take_spectrum_but.setText("Take Spectrum")
        self.setEnabled(True)
        self.repaint()
        self.running=False


    def updateFromCenterSpan(self):
        center = self.center.value()
        span = self.span.value()
        
        self.start_wl.blockSignals(True)
        self.stop_wl.blockSignals(True)
        self.start_wl.setValue(center-span/2)
        self.stop_wl.setValue(center+span/2)
        self.start_wl.blockSignals(False)
        self.stop_wl.blockSignals(False)
        
        
    def updateFromStartStop(self):

        start = self.start_wl.value()
        stop = self.stop_wl.value()
        
        self.center.blockSignals(True)
        self.span.blockSignals(True)
        self.center.setValue(start+(stop-start)/2)
        self.span.setValue(stop-start)
        self.center.blockSignals(False)
        self.span.blockSignals(False)
        
        
    def updatePath(self):
        path = QFileDialog.getExistingDirectory(None, "Set Spectrum Path", "")
        
        self.path.setText(path)
        
        
    def calibrateOffset(self,channels=[1,2]):
        if len(channels)<2:
            raise RuntimeError("Cannot calibrate with less than 2 channels")
        
        
        self.powermeter.setMeasPower(self.laser.getWavelength())
        off = np.zeros(len(channels))
        off[0] = self.powermeter.offsets[np.argmin(np.abs(channels[0]-self.powermeter.channels))]
                 
        results = self.powermeter.measurePower(self.laser.getWavelength(),channels=channels,offsets=off)
        
        for i,chan in enumerate(channels[1:]):
            for j,chan_0 in enumerate(self.powermeter.channels):
                if chan_0==chan:
                    self.powermeter.offsets[j] = results[0]-results[i+1]
                    print(f"Offset of chan {self.powermeter.channels[j]} set to {self.powermeter.offsets[j]}")
                    
        self.calibrating_MPM.emit()
            
            
        
        
        
    def takeSpectrum(self,emitSignal=True):

        self.start_spectrum.emit()
        
        thread = threading.currentThread()

        old_wl = self.laser.getWavelength()
     
        if self.laser is not None and self.powermeter is not None:
            try:
                
                dat = getSpectrum(self.laser,self.powermeter,
                             self.start_wl.value(),self.stop_wl.value(),self.res.value(),self.speed.value())

            except KeyboardInterrupt as e:
                print("Cancelled by user")
            except Exception as e:
                print(e)
        time.sleep(1)
        self.laser.setWavelength(old_wl)
        if emitSignal:
            self.stop_spectrum.emit()
        
        thread.data = dat
        
        print("Spectrum acquired")
        
        return dat


if __name__=="__main__":
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    laser = SantecLaser(debug=False)   
    power = SantecPowerMeter(debug=False)
    tab = TLSTab(santec_laser=laser,santec_powermeter=power)
    

    tab.show()

    sys.exit(app.exec_())
        
