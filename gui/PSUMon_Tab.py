# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 08:48:14 2021

@author: ques
"""
import numpy as np
import time
import os,sys
sys.path.append(os.path.dirname(__file__))



from PyQt5.QtWidgets import QGridLayout,QLCDNumber,QPushButton,QLabel,QGroupBox,QDoubleSpinBox
from PyQt5.QtCore import pyqtSignal

import threading

from Basic_Tab import BasicTab


       
class PSUMonitorTab(BasicTab):
    
    stop_mon_sig = pyqtSignal(name="Stop Monitoring")
    start_mon_sig = pyqtSignal(name="Start Monitoring")
    
    
    def __init__(self,psu,title="PSU Monitoring",sleep=0.1):
        if psu is None:
            title+=" Virtual"
        super().__init__(title)
        self.psu = psu
        self.sleep=sleep
        
        self.initUI()
        self.thread = None        
        
        
    def initUI(self):
        
        self.psu_layout= QGridLayout()
        self.setLayout(self.psu_layout)
        
        
        #Monitor Block
        self.volt_mon = QLCDNumber()
        self.volt_mon.setDecMode()
        self.volt_mon.setMinimumSize(100,50)
        
        self.cur_mon = QLCDNumber()
        self.cur_mon.setDecMode()
        self.cur_mon.setMinimumSize(100,50)
        
        self.toggle_mon_button = QPushButton("Start Monitoring")
#        self.toggle_mon_button.setCheckable(True)
        
        
        #Set Volt Block
        self.set_volt_box = QDoubleSpinBox()
        self.set_volt_box.setRange(-25,25)
        self.set_volt_box.setSuffix(" V")
        if self.psu is None:
            self.set_volt_box.setValue(0)
        else:
            self.set_volt_box.setValue(self.psu.getSetVolt())
            
            
        self.set_volt_button = QPushButton("Set Volt")
        
        self.psu_layout.addWidget(self.toggle_mon_button,0,0,1,2)
        self.psu_layout.addWidget(QLabel("Voltage [V] : "),1,0)
        self.psu_layout.addWidget(self.volt_mon,1,1)
        self.psu_layout.addWidget(QLabel("Current [mA] : "),2,0)
        self.psu_layout.addWidget(self.cur_mon,2,1)
        self.psu_layout.addWidget(QLabel("Set Voltage :"),3,0)
        self.psu_layout.addWidget(self.set_volt_box,3,1)
        self.psu_layout.addWidget(self.set_volt_button,3,2)
        
        
        self.set_volt_button.pressed.connect(self.set_volt)
        self.toggle_mon_button.pressed.connect(self.toggle_mon)
            
    def toggle_mon(self):
        if self.thread is None or self.thread.running==False:
            self.start_mon()
        else:
            self.stop_mon()
        
    def start_mon(self):
        self.toggle_mon_button.setChecked(True)
        self.toggle_mon_button.setText("Stop Monitoring")
        self.toggle_mon_button.repaint()
        
        def fct(psu,v_mon,c_mon,sleep):
            
            thread = threading.currentThread()
            while thread.running:
                
                if psu is not None:
                    V = psu.getVolt()
                    I = psu.getCur()
                else:
                    V = np.random.uniform(9,10)
                    I = np.random.uniform(1,2)
                    
                v_mon.display(V)
                c_mon.display(I*1000)
                    
                    
                time.sleep(sleep)
        
        self.thread = threading.Thread(target=fct,name="VoltageMonitor",args=(self.psu,self.volt_mon,self.cur_mon,self.sleep,))
        self.thread.running = True
        self.thread.start()
        self.start_mon_sig.emit()
    
    def stop_mon(self):
        self.thread.running = False
        self.thread.join()
        
        self.toggle_mon_button.setChecked(False)
        self.toggle_mon_button.setText("Start Monitoring")
        self.toggle_mon_button.repaint()
        
        self.stop_mon_sig.emit()
        
        
    def set_volt(self):
        setVal = self.set_volt_box.value()
        if self.psu is None:
            print(f"Setting to {setVal:.3}V")
        else:
            self.psu.setVolt(setVal)
        
        
        
if __name__=="__main__":
    from PyQt5.QtWidgets import QApplication
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    ex = PSUMonitorTab(psu=None)
    ex.show()
