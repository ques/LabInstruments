# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 17:15:40 2021

@author: ques
"""

import matplotlib.pyplot as plt
import numpy as np
from DataInstruments import MP1764C,MP1763C
import time
from BasicInstruments import RX,AgilentOSC,SCFiberSwitch,HP_PowerMeter
from OSA import OSA


p_in = HP_PowerMeter("GPIB0::7::INSTR")
rx = RX()
osc = AgilentOSC()
#osa = OSA()
#switch = SCFiberSwitch()

ea = MP1764C()
bpg = MP1763C()
ea.connect()
bpg.connect()


path = r"M:\Experiment\3_OOK_LongOAM\BER_measure\7_Chan567_100uW_in OAM\chan_7_single"

manual_opt = False
plotOnEye=True

wl0 = 1550

def BER(path,meas_name,rx_power,pd_power):
    
    rx.closePD()
#    rx.attn.setAttn(40,2)
    rx.openRX()
    
    rx_power = np.sort(rx_power)
    rx.setPowerRX(rx_power[0],pd_power=pd_power)
    rx.setPowerPD(pd_power)
    
    
    rxPower = np.array([])
    pdPower = np.array([])
    BER = np.array([])
    stdBER = np.array([])
    
    res_name = path + '\\'+meas_name+'_BER_results.txt'
    res_file = open(res_name,'a')
    res_file.write('BER  stdBER   Received Power[dBm]   Target Power[dBm]   PD Power [dBm]\n')
    res_file.close()
    
    for p in rx_power:
        print(f"Step Power : {p:.2f}dBm")
# =============================================================================
#       Adjust power on receiver
# =============================================================================
        rx.setPowerRX(p,pd_power = pd_power)
        time.sleep(1)
        pw_rx = rx.getPowerRX()
        pw_pd = rx.getPowerPD()
        
        rxPower = np.concatenate((rxPower,[pw_rx]))
        pdPower = np.concatenate((pdPower,[pw_pd]))
        
        
# =============================================================================
#       Get the eye diagram
# =============================================================================
        x,y = osc.getEye(40)
        fname  = path + "\\"+meas_name+f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
        np.savetxt(fname,np.vstack((x,y)).T)
        if plotOnEye:
            plt.figure()
            plt.title(f"RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")
            plt.scatter(x*1e12,y,1)
            plt.xlabel("Time [ps]")
            plt.ylabel("Voltage [V]")
#            plt.savefig(path + "\\"+meas_name + f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png')
#            plt.close('all')
#            
            
# =============================================================================
#       Optimize BER
# =============================================================================
        if manual_opt:
            input("Press enter when optimized : ")
        else:
            ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=1)
            ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=5)
            
        n_error = 10
        tmp_BER,int_seq = ea.get_coarse_BER()
        if tmp_BER == 1 : 
            input("Press enter when optimized : ")
            tmp_BER,int_seq = ea.get_coarse_BER()
        
        
        n_iter = 10
        tmp_BER = np.array([])
        for i in np.arange(0,n_iter):
            tmp,int_seq =  ea.get_coarse_BER(int_sec=int_seq)
            tmp_BER = np.concatenate((tmp_BER,[tmp]))
            
        
        BER = np.concatenate((BER,[np.mean(tmp_BER)]))
        stdBER = np.concatenate((stdBER,[np.std(tmp_BER)]))
        
        
        res_file = open(res_name,'a')
        res_file.write(f'{np.mean(tmp_BER):.4e}  {np.std(tmp_BER):.4e}  {pw_rx:.4f}  {p:.4f} {pw_pd:.4f} \n')
        res_file.close()
        print(f"BER : {np.mean(tmp_BER):.4e}  {np.std(tmp_BER):.4e}  RX Target Power : {p:.2f}  RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")

# =============================================================================
#       Acquire OSA
# =============================================================================
#        fname  = path + "\\"+meas_name+f'_OSA_Input_{pw_rx:.3f}dBm.txt'
#        switch.switchTo(2)
#        X,Y = osa.takeSingle(wl0,20,sync=False,res=0.02,trace="A",sens="MID")
#        
#        fname  = path + "\\"+meas_name+f'_OSA_Output_{pw_rx:.3f}dBm.txt'
#        switch.switchTo(3)
#        X,Y = osa.takeSingle(wl0,20,sync=False,res=0.02,trace="A",sens="MID")
#        
#        fname  = path + "\\"+meas_name+f'_OSA_Received_{pw_rx:.3f}dBm.txt'
#        switch.switchTo(4)
#        X,Y = osa.takeSingle(wl0,20,sync=False,res=0.02,trace="A",sens="MID")
    
    return BER,rxPower,pdPower

 #%%   
def trackBER(t=None,p_rec=None,BER=None):
    
    fig,ax=plt.subplots()
    ax2 = ax.twinx()
    
    if t is None:
        t = []
    if p_rec is None:
        p_rec = []
    if BER is None:
        BER = []
    start = time.time()
    line1 = ax.plot([0],[0],'r',label='BER')
    line1 = line1[0]
    line2 = ax2.plot([0],[0],'b',label='P Receive')
    line2 = line2[0]
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    try:
        while True:
            t.append(time.time()-start)
            
            BER.append(ea.get_error_ratio())
            p_rec.append(rx.getPowerRX())
            line1.set_data(t, BER)
            line2.set_data(t,p_rec)
            plt.pause(0.1)
            ax.set_xlim([np.min(t),np.max(t)])
            ax2.set_xlim([np.min(t),np.max(t)])
            ax2.set_ylim([np.min(p_rec),np.max(p_rec)])
            ax.set_ylim([np.min(BER),np.max(BER)])
    except:
        return t,p_rec,BER
    

    return 
        
t,p_rec,BER=trackBER()
fig,ax = plt.subplots()
ax2=ax.twinx()
ax.plot(t,BER,'r',label='BER')
ax2.plot(t,p_rec,labe='P Received')
ax.legend(loc='upper left')
ax2.legend(loc='upper right')
        

 
    
    
#%%   
   

BER,rxPower,pdPower = BER(path,'Meas2',np.arange(-40,-31,1),pd_power=1)


#%%
fig,ax = plt.subplots()

fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\7_Chan567_100uW_in OAM\b2b"+f'\Meas1_BER_results.txt'
data = np.loadtxt(fname,skiprows=1)
BER,pw_rx,pw_pd = data[:,0],data[:,2],data[:,4]
ax.semilogy(pw_rx,-np.log10(BER),'x-')

fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\7_Chan567_100uW_in OAM\b2b"+f'\Meas2_BER_results.txt'
data = np.loadtxt(fname,skiprows=1)
BER,pw_rx,pw_pd = data[:,0],data[:,2],data[:,4]
ax.semilogy(pw_rx,-np.log10(BER),'x-')
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\5_Day3\B2B_Day3"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan5_InChip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')

#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan5_OutChip"+f'\Meas2_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_m5_OutChip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
##
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_5_MUX_L5m5_Outchip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_m5_MUX_L5m5_Outchip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
ax.set_title(f'BER with P_PD={np.mean(pw_pd):.2f}dBm')   
ax.grid(which='both')
ax.invert_yaxis()
    
    