# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 17:15:40 2021

@author: ques
"""

import matplotlib.pyplot as plt
import numpy as np
from DataInstruments import MP1764C,MP1763C
import time
from BasicInstruments import RX,AgilentOSC,SCFiberSwitch,HP_PowerMeter
from OSA import OSA
import pickle


p_in = HP_PowerMeter("GPIB0::7::INSTR")
rx = RX()
osc = AgilentOSC()
osa = OSA()
switch = SCFiberSwitch()

ea = MP1764C()
bpg = MP1763C()
ea.connect()
bpg.connect()


path = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_m5_MUX_L5m5_Outchip"

manual_opt = False
plotOnEye=True

wl0 = 1550

def BER(path,meas_name,rx_power,pd_power):
    
    p_pump = input("Enter pump power in device : ")
    p_probe = input("Enter probe power in device : ")
    
    rx.closePD()
#    rx.attn.setAttn(40,2)
    rx.openRX()
    
    rx_power = np.sort(rx_power)
    rx.setPowerRX(rx_power[0],pd_power=pd_power)
    rx.setPowerPD(pd_power)
    
    
    rxPower = np.array([])
    pdPower = np.array([])
    BER = np.array([])
    
    res_name = path + '\\'+meas_name+f'_BER_results_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
    res_file = open(res_name,'a')
    res_file.write('BER   Received Power[dBm]   Target Power[dBm]   PD Power [dBm]\n')
    res_file.close()
    
    
    
# =============================================================================
#       Acquire OSA
# =============================================================================
    
    pin = p_in.getPower(channel=1)
    
    fname  = path + "\\"+meas_name+f'_OSA_Input_{pin:.2f}dBm.txt'
    switch.switchTo(2)
    X,Y = osa.takeSingle(wl0,20,sync=False,res=0.02,trace="A",sens="MID",npoints=10001)
    plt.figure()
    plt.title(f"Input Spectrum to Device")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    
    fname  = path + "\\"+meas_name+f'_OSA_Output_{pin:.2f}dBm.txt'
    switch.switchTo(3)
    X,Y = osa.takeSingle(wl0,20,sync=False,res=0.02,trace="A",sens="MID",npoints=10001)
    plt.figure()
    plt.title(f"Output Spectrum from Device")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    
    fname  = path + "\\"+meas_name+f'_OSA_Received_{pin:.2f}dBm.txt'
    switch.switchTo(4)
    X,Y = osa.takeSingle(wl0,20,sync=False,res=0.02,trace="A",sens="MID",npoints=10001)    
    plt.figure()
    plt.title(f"Received Spectrum to Rx")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    

    
    
    for p in rx_power:
        print(f"Step Power : {p:.2f}dBm")
# =============================================================================
#       Adjust power on receiver
# =============================================================================
        rx.setPowerRX(p,pd_power = pd_power)
        time.sleep(1)
        pw_rx = rx.getPowerRX()
        pw_pd = rx.getPowerPD()
        
        rxPower = np.concatenate((rxPower,[pw_rx]))
        pdPower = np.concatenate((pdPower,[pw_pd]))
        
        
# =============================================================================
#       Get the eye diagram
# =============================================================================
        x,y = osc.getEye(40)
        fname  = path + "\\"+meas_name+f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
        np.savetxt(fname,np.vstack((x,y)).T)
        if plotOnEye:
            plt.figure()
            plt.title(f"RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")
            plt.scatter(x*1e12,y,1)
            plt.xlabel("Time [ps]")
            plt.ylabel("Voltage [V]")
            plt.savefig(path + "\\"+meas_name + f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png')
            plt.close('all')
#            
            
# =============================================================================
#       Optimize BER
# =============================================================================
        if manual_opt:
            input("Press enter when optimized : ")
        else:
            ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=1)
            ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=5)
            
        n_error = 10
        tmp_BER,int_seq = ea.get_coarse_BER()
        if tmp_BER == 1 : 
            input("Press enter when optimized : ")
            tmp_BER,int_seq = ea.get_coarse_BER()
        
        tmp_BER,int_seq =  ea.get_coarse_BER(int_sec=int_seq*2)
        BER = np.concatenate((BER,[tmp_BER]))
        
        
        res_file = open(res_name,'a')
        res_file.write(f'{tmp_BER:.4e} {pw_rx:.4f}  {p:.4f} {pw_pd:.4f} \n')
        res_file.close()
        print(f"BER : {tmp_BER:.4e}  RX Target Power : {p:.2f}  RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")


        
    return BER,rxPower,pdPower

    


    
#%%

#X,Y = osa.takeSingle(1570,100,True,2,'A',sens='MID',npoints=1001)
#path = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15\2021-01-18\\"
#B2B = None
#    
#B2B = np.loadtxt(r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15\2021-01-18\B2B_OSA.txt")
#X_BK,Y_BK = B2B[:,0],B2B[:,1]
#
#meas = 'H1_02x08'
#fig = plt.figure()
#if 'B2B' in meas or B2B is None: 
#    plt.plot(X,Y)
#else:
#    plt.plot(X,Y-Y_BK)
#    plt.ylim([-90,0])
#plt.xlabel('Wavelength [nm]')
#plt.ylabel("Power [dBm]")
#
#
#plt.savefig(path+meas+'_OSA.png')
#pickle.dump(fig, open(path+meas+'_OSA.pickle', 'wb'))
#np.savetxt(path+meas+'_OSA.txt',np.vstack((X,Y)).T,header=f"Resolution : 2nm\nSensitivity : MID\n")  
##plt.close('all')  


#%%
#BER,rxPower,pdPower = BER(path,'Meas1',np.arange(-39,-25,1),pd_power=3)

#%%
#fig,ax = plt.subplots()
#
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\b2b_2"+f'\B2B_2_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
##fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan5_InChip"+f'\Meas1_BER_results.txt'
##data = np.loadtxt(fname,skiprows=1)
##BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
##ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan5_OutChip"+f'\Meas2_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_m5_OutChip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
##
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_5_MUX_L5m5_Outchip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_m5_MUX_L5m5_Outchip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#ax.set_title(f'BER with P_PD={np.mean(pw_pd):.2f}dBm')   
#ax.grid(which='both')
#ax.invert_yaxis()
    
    