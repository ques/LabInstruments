# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 17:30:22 2022

@author: FTNK-LocalAdm
"""


import matplotlib.pyplot as plt
import numpy as np

import sys,os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from instruments.DataInstruments import MP1764C,MP1763C,generate_bit_pattern
import time

from instruments.OZ_VOA import OZ_VOA
from instruments.Thorlabs_PowerMeter import PM320

from instruments.SantecLaserPowerMeter import SantecLaser,SantecPowerMeter,getSpectrum
import pickle



santec_laser = SantecLaser()
santec_power = SantecPowerMeter()

voa = OZ_VOA("ASRL4::INSTR")  

p_in = PM320()




#%%
path = r"U:\Ftnk\COMMUNICATION\Ques\Bowtie_WavelengthConv\H1_BT_Mol_02x07_Real\NonLinearTransmission_Fine\\"


start_attn=25
end_attn=12
n_point = 50

p = np.linspace(start_attn,end_attn,50)
p = np.concatenate((p,np.flip(p)))

p = np.concatenate((p,p))

probe = np.round(p_in.getPower(1),2)
voa
for attn in p:
    voa.setAttn(attn)
    time.sleep(1)
    done = False
    while not done:
        try:
            res = getSpectrum(laser=santec_laser,power=santec_power,sweep_speed=5,wl_inc=0.01,wl_end = 1533.50,wl_start=1528.50)
            done=True
        except:
            done=False
            
    pump = np.round(p_in.getPower(2),2)
    np.savetxt(path+f"Nonlin_Transmission_PumpPower={pump:.2f}dBm_ProbePower={probe:.2f}dBm.txt",np.vstack((res[0],res[1],res[2])).T)

voa.setAttn(50)
santec_laser.enableShutter()






#%%

path = 