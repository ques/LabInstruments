# -*- coding: utf-8 -*-
"""
Created on Thu May  6 13:25:28 2021

@author: FTNK-LocalAdm
"""

import matplotlib.pyplot as plt
import numpy as np

import sys,os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from instruments.DataInstruments import MP1764C,MP1763C,generate_bit_pattern
import time

from instruments.OZ_VOA import OZ_VOA
from instruments.FullReceiver import RX
from instruments.SCFiberSwitch import SCFiberSwitch
from instruments.HP_PowerMeter import HP_PowerMeter
from instruments.Agilent_DCA import AgilentDCA
from instruments.OSA_Anritsu import OSA_Anritsu
from instruments.Keithley_2450 import Keithley_2450
from instruments.Thorlabs_PowerMeter import PM320,PM100D
from instruments.SantecLaserPowerMeter import SantecLaser,SantecPowerMeter,getSpectrum,getWaveSweep
from instruments.WaveAnalyser import WaveAnalyser
from instruments.Agilent_Switch import AgilentSwitch
from instruments.WaveShapers import WaveShaper_4000A
import pickle
import os,sys
import scipy.fftpack as fft
from scipy.interpolate import interp1d

rx = RX()
osc = AgilentDCA()
osa = OSA_Anritsu()
waveanalyser = WaveAnalyser()
switch = SCFiberSwitch()
# keith = Keithley_2450(debug=True)
# dso = AgilentDSO()
p_in = PM320(addr="USB0::0x1313::0x8022::M00606447::INSTR")
santec_laser = SantecLaser()
santec_power = SantecPowerMeter()
voa = OZ_VOA("ASRL4::INSTR")  

switch = AgilentSwitch()
waveshaper = WaveShaper_4000A()


ea = MP1764C()
bpg = MP1763C()
ea.connect()
bpg.connect()

#%%Set pattern on BPG and EA
n=14
period=4
pattern = generate_bit_pattern(2**n,period,q=0.5)

bpg.set_byte_array(pattern)
ea.set_byte_array(pattern)

#%%



plotOnEye=False

def saveTransmissionSpectrum(filename,tls_power,wl_start,wl_stop,wl_step,wl_speed):
    santec_laser.setPower(tls_power)
    done=False
    while not done:
        try:
            save_dat = getSpectrum(santec_laser, santec_power, wl_start, wl_stop, wl_step, wl_speed)
            done=True
        except KeyboardInterrupt as e:
            done = True
        except:
            done=False
    np.savetxt(filename,np.array(save_dat).T,header=f"Laser Power={tls_power:.3f}dBm\nStart WL={wl_start:.3f}nm\nStop WL={wl_stop:.3f}nm\nStep={wl_step:.3f}nm\nSpeed={wl_speed:.3f}nm/s")


#%%
path = r"U:\Ftnk\COMMUNICATION\Ques\Bowtie_WavelengthConv\1GHz_WavelengthConversion\B2B_H0_03x07\Transmission\\"
filename = "H0_03x07_Meas01.tab"
saveTransmissionSpectrum(path+filename, 0, 1530, 1550, 0.01, 10)

data = np.loadtxt(path+filename)
b2b = np.loadtxt(path+'B2B.tab')
b2b = interp1d(b2b[:,0],b2b[:,1],bounds_error=False)

plt.figure()
plt.plot(data[:,0],data[:,1]-b2b(data[:,0]))

#%%


def getBER(path,meas_name,rx_power,pd_power,wl0,
        osa_span=10,osa_npoints=2001,osa_trace='A',osc_channel=1,osa_sens='MID',
        acqOSA=True,acqEye=True,acqOSNR=False,acqBER=True,
        manual_opt=False,pump_wl=None,setup_comment=""):
    
    if not os.path.exists(path):
        os.mkdir(path)
        
    eye_path = path +"\\Eye_Diagram"
    if not os.path.exists(eye_path):
        os.mkdir(eye_path)
        
    spec_path = path +"\\Spectra"
    if not os.path.exists(spec_path):
        os.mkdir(spec_path)
        
    osnr_path = path+"\\OSNR_Spectra"
    if not os.path.exists(osnr_path):
        os.mkdir(osnr_path)
        
    p_pump = p_in.getPower(2)
    p_probe = p_in.getPower(1)
    
    # rx.closePD()
    # rx.attn.setAttn(40,2)
    # rx.openRX()
    
    if np.abs(rx.getPowerRX()-rx_power[0])>0.1:
        rx.setPowerRX(rx_power[0],pd_power=pd_power)
    else:
        print("No change : already reached")
        rx.openPD()
        rx.setPowerPD(pd_power)
    
    rxPower = np.array([])
    pdPower = np.array([])
    BER = np.array([])
    stdBER = np.array([])
    
    rx.openPD()
# =============================================================================
#     Get the BER file ready
# =============================================================================
    if acqBER:
        res_name = path + '\\' + meas_name + f'_BER_results_InputTotal@{p_pump:.2f}dBm_TLS@{p_probe:.2f}dBm'
        try:
            res_file = open(res_name+'.txt','r')
            res_file.close()
        except:
    
            print('File empty : adding headers')
            res_file = open(res_name+'.txt','a')
            res_file.write('#BER  StdBER   Received Power[dBm]   Target Power[dBm]   PD Power [dBm]  Delay[ps] Volt Threshold[V]\n')
            res_file.close()
            
            
        
        wss_f = float(input("Waveshaper Frequency [THz] : "))
        wss_bw = float(input("Waveshaper Bandwidth [GHz] :"))
        
        tls_setpower = santec_laser.getPower()
        tls_setwavelength = santec_laser.getWavelength()
        
        setup_name= path + '\\' + meas_name + '_OpticalSetup'
        res_file = open(setup_name+'.txt','a')
        res_file.write(f'Waveshaper Frequency : {wss_f:.3f}THz\n')
        res_file.write(f'Waveshaper Bandwidth : {wss_bw:.3f}GHz\n')
        res_file.write(f"TLS SetPower : {tls_setpower:.2f}dBm\n")
        res_file.write(f"TLS SetWavelength : {tls_setwavelength:.3f}nm\n")
        res_file.write(setup_comment)
        res_file.close()
    
    
    
# =============================================================================
#       Acquire OSA
# =============================================================================
        
    if acqOSA:
        
        print("OSA Input")
        fname  = spec_path + "\\"+meas_name+f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(1,2)
        switch.switchTo(2, 2)
        time.sleep(1)
        X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.03,trace=osa_trace,rbw=200,npoints=osa_npoints)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title("Input Spectrum to Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        # plt.ylim(-75,-10)
        plt.savefig(spec_path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
        plt.close()
        
        
        print("Input Waveanalyser")
        fname  = spec_path + "\\"+meas_name+f'_WaveAnalyser_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(1,2)
        switch.switchTo(2, 3)
        time.sleep(1)
        X,Y = waveanalyser.getSpectrum(wl0)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title("Input Spectrum to Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        # plt.ylim(-90,-10)
        plt.savefig(spec_path + "\\"+meas_name + f'_WaveAnalyser_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
        plt.close()
        
        print("OSA Received")
        fname  = spec_path + "\\"+meas_name+f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(1,3)
        switch.switchTo(2, 2)
        time.sleep(1)
        X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.03,trace=osa_trace,rbw=200,npoints=osa_npoints)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title("Input Spectrum to Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        # plt.ylim(-75,-10)
        plt.savefig(spec_path + "\\"+meas_name + f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
        plt.close()
        
        
        print("Received Waveanalyser")
        fname  = spec_path + "\\"+meas_name+f'_WaveAnalyser_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(1,3)
        switch.switchTo(2, 3)
        time.sleep(1)
        X,Y = waveanalyser.getSpectrum(wl0)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title("Input Spectrum to Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        # plt.ylim(-90,-10)
        plt.savefig(spec_path + "\\"+meas_name + f'_WaveAnalyser_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
        plt.close()
        
        
        
        if pump_wl is not None:
            print("OSA Pump Input")
            fname  = spec_path + "\\"+meas_name+f'_OSA_Pump_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
            switch.switchTo(1,2)
            switch.switchTo(2, 2)
            time.sleep(2)
            X,Y = osa.takeSingle(pump_wl,osa_span,sync=False,res=0.03,trace=osa_trace,rbw=200,npoints=osa_npoints)
            np.savetxt(fname,np.vstack((X,Y)).T)
            fig = plt.figure()
            plt.title("Input Pump Spectrum to Device")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            # plt.ylim(-75,-10)
            plt.savefig(spec_path + "\\"+meas_name + f'_OSA_Pump_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
            plt.close()
            
            
            print("Pump Input Waveanalyser")
            fname  = spec_path + "\\"+meas_name+f'_WaveAnalyser_Pump_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
            switch.switchTo(1,2)
            switch.switchTo(2, 3)
            time.sleep(2)
            X,Y = waveanalyser.getSpectrum(pump_wl)
            np.savetxt(fname,np.vstack((X,Y)).T)
            fig = plt.figure()
            plt.title("Input Pump Spectrum to Device")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            # plt.ylim(-90,-10)
            plt.savefig(spec_path + "\\"+meas_name + f'_WaveAnalyser_Pump_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
            plt.close()
            
            
    
        
    # #    
    
    #     print("OSA Output")
    #     fname  = path + "\\"+meas_name+f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
    #     switch.switchTo(4)
    #     X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.02,trace=osa_trace,sens="HIGH2",npoints=osa_npoints)
    #     np.savetxt(fname,np.vstack((X,Y)).T)
    #     fig = plt.figure()
    #     plt.title("Output Spectrum from Device")
    #     plt.xlabel('Wavelength [nm]')
    #     plt.ylabel('Power [dBm]')
    #     plt.plot(X,Y)
    #     plt.ylim(-90,-10)
    #     plt.savefig(path + "\\"+meas_name + f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    # #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
    #     plt.close()
        
    #     if pump_wl is not None:
    #         print("OSA Pump Output")
    #         fname  = path + "\\"+meas_name+f'_OSA_Pump_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
    #         switch.switchTo(4)
    #         X,Y = osa.takeSingle(pump_wl,osa_span,sync=False,res=0.02,trace=osa_trace,sens="HIGH2",npoints=osa_npoints)
    #         np.savetxt(fname,np.vstack((X,Y)).T)
    #         fig = plt.figure()
    #         plt.title("Output Pump Spectrum from Device")
    #         plt.xlabel('Wavelength [nm]')
    #         plt.ylabel('Power [dBm]')
    #         plt.plot(X,Y)
    #         plt.ylim(-90,-10)
    #         plt.savefig(path + "\\"+meas_name + f'_OSA_Pump_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    #     #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
    #         plt.close()
    #    

    #     print("Received Spectrum")
    #     fname  = path + "\\"+meas_name+f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
    #     switch.switchTo(1,3)
    #     switch.switchTo(2, 2)
    #     time.sleep(1)
    #     X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.03,trace=osa_trace,rbw=200,npoints=osa_npoints)    
    #     np.savetxt(fname,np.vstack((X,Y)).T)
    #     fig = plt.figure()
    #     plt.title("Received Spectrum to Rx")
    #     plt.xlabel('Wavelength [nm]')
    #     plt.ylabel('Power [dBm]')
    #     plt.plot(X,Y)
    #     plt.ylim(-75,-10)
    #     plt.savefig(path + "\\"+meas_name + f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    # #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
    #     plt.close() 
        
    #     print("Received Spectrum")
    #     fname  = path + "\\"+meas_name+f'_WaveAnalyser_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
    #     switch.switchTo(1,3)
    #     switch.switchTo(2, 3)
    #     time.sleep(1)
    #     X,Y = waveanalyser.getSpectrum(wl0)
    #     np.savetxt(fname,np.vstack((X,Y)).T)
    #     fig = plt.figure()
    #     plt.title("Received Spectrum to Rx")
    #     plt.xlabel('Wavelength [nm]')
    #     plt.ylabel('Power [dBm]')
    #     plt.plot(X,Y)
    #     plt.ylim(-75,-10)
    #     plt.savefig(path + "\\"+meas_name + f'_WaveAnalyser_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    # #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
    #     plt.close() 
        
        

    
    
    for p in rx_power:
        print(f"Step Power : {p:.2f}dBm")
# =============================================================================
#       Adjust power on receiver
# =============================================================================
        if np.abs(rx.getPowerRX()-p)>0.1:
            rx.setPowerRX(p,pd_power = pd_power)
            
        else:
            print("No change : already reached")
            rx.setPowerPD(pd_power)
        time.sleep(1)
        pw_rx = rx.getPowerRX()
        pw_pd = rx.getPowerPD()
        
        rxPower = np.concatenate((rxPower,[pw_rx]))
        pdPower = np.concatenate((pdPower,[pw_pd]))
        
# =============================================================================
#       Optimize BER
# =============================================================================
        if acqBER:
            if manual_opt:
                input("Press enter when optimized : ")
            else:
                ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=1)
                ea.set_integration_time(0,1)
                ea.single()
                ea.start()
                time.sleep(1)
                tmp_BER = ea.get_error_ratio()
                if tmp_BER < 1e-8:
                    print("Further optimization")
                    ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=5)
                
            tmp_BER,int_seq = ea.get_coarse_BER()
            if tmp_BER == 1 : 
                input("Press enter when optimized : ")
                tmp_BER,int_seq = ea.get_coarse_BER()
            
            
            
            n_iter = 4
            tmp_BER = np.array([])
            int_seq = 2
            for i in np.arange(0,n_iter):
                print(30*'\b'+f'Iter : {i+1:.0f}/{n_iter:.0f}',end='')
                tmp,int_seq =  ea.get_accurate_BER(min_error=10,int_sec=int_seq,max_int_sec=2*60)
                tmp_BER = np.concatenate((tmp_BER,[tmp]))
                
                
            
            BER = np.concatenate((BER,[np.mean(tmp_BER)]))
            stdBER = np.concatenate((stdBER,[np.std(tmp_BER)]))
            pw_rx = rx.getPowerRX()
            pw_pd = rx.getPowerPD()
            delay = ea.get_clock_phase_delay()
            thrs_volt = ea.get_data_threshold_voltage()
            
            res_file = open(res_name+'.txt','a')
            res_file.write(f'{np.mean(tmp_BER):.4e}  {np.std(tmp_BER):.4e}  {pw_rx:.4f}  {p:.4f} {pw_pd:.4f} {delay:.0f} {thrs_volt:.3f} \n')
            res_file.close()
            print(30*'\b'+f"BER : {np.mean(tmp_BER):.4e}  {np.std(tmp_BER):.4e}  RX Target Power : {p:.2f}  RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")
        
            ea.set_integration_time(0,1)
            ea.continuous()
            ea.start()
# =============================================================================
#       Get the eye diagram
# =============================================================================
        if acqEye:
            x,y = osc.getEyeMask(channel=osc_channel,intTime=30)
            fname  = eye_path + "\\"+meas_name+f'_Eye_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
            np.savetxt(fname,np.vstack((x,y)).T)
            
            
            # plt.figure()
            # plt.title(f"RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")
            # plt.scatter(x*1e12,y,0.2)
            # plt.xlabel("Time [ps]")
            # plt.ylabel("Voltage [V]")
            # plt.savefig(path + "\\"+meas_name + f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png')
            osc.saveScreenImage(eye_path + "\\"+meas_name + f'_Eye_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_Screen.png')
    #        pickle.dump(fig, open(path + "\\"+meas_name + f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.pickle', 'wb'))
            
            osc.oscMode()
            osc.run()
            plt.close('all')   
                
                
                
# =============================================================================
#       OSA Acquisition for OSNR
# =============================================================================
        if acqOSNR:
            fname  = osnr_path + "\\"+meas_name+f'_OSA_0d1nm_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
            switch.switchTo(1,4)
            switch.switchTo(2, 2)
            time.sleep(1)
            X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.1,rbw=1000,trace=osa_trace,npoints=osa_npoints) 
            np.savetxt(fname,np.vstack((X,Y)).T)
            plt.figure()
            plt.title(f"Received Spectrum to Rx")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            # plt.ylim(-75,-10)
            plt.savefig(osnr_path + "\\"+meas_name + f'_OSA_0d1nm_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png',format='png')
            plt.close()  
            
            fname  = osnr_path + "\\"+meas_name+f'_OSA_1nm_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
            X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=1,rbw=1000,trace=osa_trace,npoints=osa_npoints) 
            np.savetxt(fname,np.vstack((X,Y)).T)
            plt.figure()
            plt.title(f"Received Spectrum to Rx")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            # plt.ylim(-75,-10)
            plt.savefig(osnr_path + "\\"+meas_name + f'_OSA_1nm_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png',format='png')
            plt.close()  
            
            
            print("Received Spectrum")
            fname  = osnr_path + "\\"+meas_name+f'_WaveAnalyser_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_1d2pm.txt'
            switch.switchTo(1,4)
            switch.switchTo(2, 3)
            time.sleep(1)
            X,Y = waveanalyser.getSpectrum(wl0)
            np.savetxt(fname,np.vstack((X,Y)).T)
            fig = plt.figure()
            plt.title("Received Spectrum to Rx")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            # plt.ylim(-75,-10)
            plt.savefig(osnr_path + "\\"+meas_name + f'_WaveAnalyser_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_1d2pm.png',format='png')
            plt.close() 
            
    return BER,stdBER,rxPower,pdPower
            
   
#%%
path = r"U:\Ftnk\COMMUNICATION\Ques\D267001_21-12-10\H0_Mol_01x05_OTDM_Demux_4\B2B_Meas01_SkewedFilter_-30dBmPreAmp_BW@35GHz"
setup_comment=""
setup_comment+="TX->\n"
setup_comment+="->EDFA->Filter->PC->Polarizer->OTDM_1->OTDM_2->PM-EDFA->Filter->\n"
setup_comment+="->VOA->PC->20dB->3dBCombiner->PC->\n"
setup_comment+="->ByPassFiber->\n"
setup_comment+="->20dB->EDFA->OTF->20dB->\n"
setup_comment+="->RX"
# setup_comment+="TX->RX"
wl_probe = 1549
wl_pump = 1570
pd_power=0
BER,stdBER,rxPower,pdPower = getBER(path,'Data_10GHz',np.arange(-34,-48,-1),osa_span=10,pd_power=pd_power,wl0=wl_probe,pump_wl=wl_pump,manual_opt=True,
                                  acqOSA=False,acqEye=False,acqOSNR=True,acqBER=True,osa_trace='A',osc_channel=4,osa_npoints=2001,setup_comment=setup_comment)


#%%
BER,stdBER,rxPower,pdPower = getBER(path,'Data_10GHz',np.arange(-34,-44,-1),osa_span=10,pd_power=pd_power,wl0=wl_probe,pump_wl=wl_pump,manual_opt=True,
                                  acqOSA=False,acqEye=True,acqOSNR=False,acqBER=False,osa_trace='A',osc_channel=4,osa_npoints=2001)

#%%
BER,stdBER,rxPower,pdPower = getBER(path,'Data_70GHz',np.arange(-25,-38,-5),osa_span=10,pd_power=pd_power,wl0=wl_probe,pump_wl=wl_pump,manual_opt=True,
                                  acqOSA=True,acqEye=True,acqOSNR=False,acqBER=False,osa_trace='A',osc_channel=3,osa_npoints=2001)






#%%
wl = np.arange(1530.70,1531,0.01)
path = r"U:\Ftnk\COMMUNICATION\Ques\D267001_21-12-10\H0_Mol_OTDM_Demux_1\ProbeSweep_Eye10G_70GHzPD_Pump@3.41dBm_Probe@-5.3dBm"
if not os.path.exists(path):
    os.mkdir(path)
pw_rx = -35
pw_pd=-3
osa_span=5
osa_center = 1530.7
osa_pump_center = 1552
speed = "10G_Pulse"
pd = "70GHzPD"

switch.switchTo(1,2)
switch.switchTo(2, 3)
X,Y = waveanalyser.getSpectrum(osa_center)
fname = path + "\\"+f'InputSpectrum_Waveanalyser_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
np.savetxt(fname,np.vstack((X,Y)).T)
fig = plt.figure()
plt.title("Received Spectrum to Rx")
plt.xlabel('Wavelength [nm]')
plt.ylabel('Power [dBm]')
plt.plot(X,Y)
fname = path + "\\"+f'InputSpectrum_Waveanalyser_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png'
plt.savefig(fname,format='png')
plt.close()
switch.switchTo(1,2)
switch.switchTo(2, 3)
X,Y = waveanalyser.getSpectrum(osa_pump_center)
fname = path + "\\"+f'InputSpectrum_Pump_Waveanalyser_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
np.savetxt(fname,np.vstack((X,Y)).T)
fig = plt.figure()
plt.title("Received Spectrum to Rx")
plt.xlabel('Wavelength [nm]')
plt.ylabel('Power [dBm]')
plt.plot(X,Y)
fname = path + "\\"+f'InputSpectrum_Pump_Waveanalyser_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png'
plt.savefig(fname,format='png')
plt.close() 
for w in wl:
    print(w)
    
    santec_laser.setWavelength(w)
    rx.setPowerRX(pw_rx,pd_power=pw_pd,maxiter=20)
    # input("Press enter when ready : ")
    
    record_point(path,pw_rx,pw_pd,osa_span,osa_center,osa_pump_center,speed,pd,osc_channel=3)
    


    
#%%
pw_rx = -20
pw_pd=0
osa_span=5
osa_center = 1531
osa_pump_center = 1551.5
speed = "10G"
pd = "10GHzPD"
probe_in = p_in.getPower(1)
pump_in = p_in.getPower(2)
path = r"U:\Ftnk\COMMUNICATION\Ques\Bowtie_WavelengthConv\H0_Mol_02x07_BisBis\MiscWavelengthConversion_"+f"Pump={pump_in:.2f}dBm_Probe={probe_in:.2f}dBm"
if not os.path.exists(path):
    os.mkdir(path)
record_point(path,pw_rx,pw_pd,osa_span,osa_center,osa_pump_center,speed,pd)
    
#%%

pump_power = -5
path = r"U:\Ftnk\COMMUNICATION\Ques\Bowtie_WavelengthConv\H0_Mol_02x07\10GHz_NonLinearTransmission\\"
res = getSpectrum(laser=santec_laser,power=santec_power,sweep_speed=5,wl_inc=0.01,wl_end = 1562,wl_start=1542)
np.savetxt(path+f"ProbeResonance_Pump={pump_power:.1f}dBm.txt",np.vstack((res[0],res[1],res[2])).T)
plt.plot(res[0],res[1],label=f"Pump={pump_power:.1f}dBm")
plt.legend()





#%%

def record_point(path,meas_name,pw_rx,pw_pd,osa_span,osa_center,osa_pump_center,speed,pd,osc_channel=3):
    rx.setPowerRX(pw_rx,pd_power=pw_pd)
    
    w = santec_laser.getWavelength()
    
    x,y = osc.getEyeMask(channel=osc_channel,intTime=30)
    fname  = path + "\\"f'{meas_name:s}_Eye_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{w:.2f}nm.txt'
    np.savetxt(fname,np.vstack((x,y)).T)
    
    osc.saveScreenImage(path + "\\"+f'{meas_name:s}_Eye_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{w:.2f}nm_Screen.png')
    
    

    # fname = path + "\\"+f'InputSpectrum_WaveAnalyser_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{w:.2f}nm.txt'
    # X,Y = waveanalyser.getSpectrum(osa_center)
    # np.savetxt(fname,np.vstack((X,Y)).T)
    

    # X,Y = osa.takeSingle(osa_pump_center,osa_span,sync=False,res=0.03,trace='A',rbw=10000,npoints=2001)
    # fname = path + "\\"+f'InputPumpSpectrum_OSA_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{w:.2f}nm.txt'
    # np.savetxt(fname,np.vstack((X,Y)).T)

    
    switch.switchTo(1,4)
    switch.switchTo(2, 3)
    # X,Y = osa.takeSingle(osa_center,osa_span,sync=False,res=0.03,trace='A',rbw=10000,npoints=2001)
    # fname = path + "\\"+f'OutputSpectrum_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{w:.2f}nm.txt'
    # np.savetxt(fname,np.vstack((X,Y)).T)
    fname = path + "\\"+f'{meas_name:s}_OutputSpectrum_WaveAnalyser_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{w:.2f}nm.txt'
    X,Y = waveanalyser.getSpectrum(osa_center)
    np.savetxt(fname,np.vstack((X,Y)).T)
    fig = plt.figure()
    plt.title("Received Spectrum to Rx")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    fname = path + "\\"+f'{meas_name:s}_OutputSpectrum_WaveAnalyser_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{w:.2f}nm.png'
    plt.savefig(fname,format='png')
    plt.close() 
    
    osc.oscMode()
    
    osc.oscMode()
    osc.run()
    
    # switch.switchTo(1)
    
    
#%%
wl_start = 1522.6
wl_stop = 1524.2
wl_step = 0.001
wl_array = np.arange(wl_start,wl_stop+wl_step/2,wl_step)

P = []
santec_laser.setPower(-14)

for wl in wl_array:
    santec_laser.setWavelength(wl)
    print(30*'\b'+str(wl),end='')
    time.sleep(0.5)
    p = santec_power.getPower(1)
    
    P.append(p)
    
P = np.array(P)
plt.figure()
plt.plot(wl_array,10**(P/10))


fname = r"U:\Ftnk\COMMUNICATION\Ques\D267001_H0_H1_Bowtie\H0IntrinsicMeasurement\H0In_spacing3_02x06_Stepping.tab"
np.savetxt(fname,np.vstack((wl_array,P)).T,comments=f"Laser Power={santec_laser.getPower():.2f}dBm\nStart WL={wl_start:.3f}nm\nStop WL={wl_stop:.3f}nm\nStep={wl_step:.3f}nm")
    




#%%
osa_center = 1531.3
path = r"U:\Ftnk\COMMUNICATION\Ques\Bowtie_WavelengthConv\H1_BT_Mol_02x07_Real\Modulated_Spectrum_Meas01"

pump_in = []
for i in np.arange(0,40):
    voa.addAttn(-0.1)
    time.sleep(1)
    fname = path + "\\"+f'OutputSpectrum_WaveAnalyser_{i:.0f}.txt'
    X,Y = waveanalyser.getSpectrum(osa_center)
    np.savetxt(fname,np.vstack((X,Y)).T)
    pump_in.append(p_in.getPower(2))
    
    
    
    
np.savetxt(path+"\\"+f"Input_Pump_Power_Probe@{p_in.getPower(1):.2f}dBm.txt",np.array(pump_in))
    



#%%
path = r"U:\Ftnk\COMMUNICATION\Ques\D267001_21-12-10\H0_Mol_WavelengthConversion_5\PumpPowerSweep_Spectra\\"


for i in range(0,20):
    voa.addAttn(-0.1)
    time.sleep(1)
    pump_in = p_in.getPower(2)
    X,Y = waveanalyser.getSpectrum(1530.8)
    fname = path+f"Spectra_PumpPower={pump_in:.2f}dBm"
    np.savetxt(fname+'.txt',np.vstack((X,Y)).T)
    
    fig = plt.figure()
    plt.title("Received Spectrum to Rx")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    plt.ylim(np.min(Y),np.max(Y))
    plt.xlim(1530.0,1531.5)
    plt.savefig(fname+'.png',format='png')
    plt.close() 
    
    
    
#%%
import datetime
try:
    path = r"U:\Ftnk\COMMUNICATION\Ques\D267001_21-12-10\H0_Mol_WavelengthConversion_5\PumpRamping_H0Mol_02x07_Meas01\\"
    os.mkdir(path)
except:
    pass

delta_voa = 5
n_point=20
# voa.addAttn(delta_voa)
start = datetime.datetime.now()
timedelay = 5


for i in range(5):
    
    res = getWaveSweep(santec_laser, santec_power, 1527.5, 1532.5, 0.01, 1,lev=5)
    X = res[0]
    Y = res[1]
    pump_in = p_in.getPower(2)
    t = datetime.datetime.now()-start
    t = t.total_seconds()
    
    fname = path+f"TLS_Spectrum_Ref_Ppump={pump_in:.2f}dBm_Iteration={i:.0f}_Time={t:.3f}s"
    np.savetxt(fname+'.txt',np.vstack((X,Y)).T)
    fig = plt.figure()
    plt.title("TLS ResonanceTransmission")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    plt.ylim(np.min(Y),np.max(Y))
    # plt.xlim(1527..0,1531.5)
    plt.savefig(fname+'.png',format='png')
    plt.close() 
    time.sleep(3)


print("Ramping")
for i in range(n_point):
    voa.addAttn(-delta_voa/n_point)
    time.sleep(1)
    
    res = getWaveSweep(santec_laser, santec_power, 1527.5, 1532.5, 0.01, 1,lev=5)
    X = res[0]
    Y = res[1]
    pump_in = p_in.getPower(2)
    t = datetime.datetime.now()-start
    t = t.total_seconds()
    
    fname = path+f"TLS_Spectrum_Ramp_Ppump={pump_in:.2f}dBm_Iteration={0:.0f}_Time={t:.3f}s"
    np.savetxt(fname+'.txt',np.vstack((X,Y)).T)
    fig = plt.figure()
    plt.title("TLS ResonanceTransmission")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    plt.ylim(np.min(Y),np.max(Y))
    # plt.xlim(1530.0,1531.5)
    plt.savefig(fname+'.png',format='png')
    plt.close() 

    
    # X,Y = waveanalyser.getSpectrum(1530.8)
    # fname = path+f"Spectra_PumpPower={pump_in:.2f}dBm"
    # np.savetxt(fname+'.txt',np.vstack((X,Y)).T)
    
    # fig = plt.figure()
    # plt.title("ModulatedSpectrum")
    # plt.xlabel('Wavelength [nm]')
    # plt.ylabel('Power [dBm]')
    # plt.plot(X,Y)
    # plt.ylim(np.min(Y),np.max(Y))
    # plt.xlim(1530.0,1531.5)
    # plt.savefig(fname+'.png',format='png')
    # plt.close() 


running=True
counter=1
while running:
    try:
        res = getWaveSweep(santec_laser, santec_power, 1527.5, 1532.5, 0.01, 1,lev=5)
        X = res[0]
        Y = res[1]
        pump_in = p_in.getPower(2)
        t = datetime.datetime.now()-start
        t = t.total_seconds()
        
        fname = path+f"TLS_Spectrum_Final_Ppump={pump_in:.2f}dBm_Iteration={0:.0f}_Time={t:.3f}s"
        np.savetxt(fname+'.txt',np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title("TLS ResonanceTransmission")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        plt.ylim(np.min(Y),np.max(Y))
        # plt.xlim(1530.0,1531.5)
        plt.savefig(fname+'.png',format='png')
        plt.close()
        
        time.sleep(5)
        
        
        
        
        
    except:
        running=False
        
        
        
#%%

path = r"U:\Ftnk\COMMUNICATION\Ques\D267001_21-12-10\H0_Mol_WavelengthConversion_6\\"
p_calib = PM100D(addr="USB0::0x1313::0x8078::P0024464::INSTR")


TruePower = []
ReadPower =  []
step = 0.2
tmp_p = p_in.getPower(2)
while tmp_p > -10:
    voa.addAttn(step)
    time.sleep(1)
    TruePower.append(p_calib.getPower())
    ReadPower.append(p_in.getPower(2))
    tmp_p = ReadPower[-1]
    print(tmp_p)
    
    
TruePower = np.array(TruePower)
ReadPower = np.array(ReadPower)


np.savetxt(path+"InputPower_Calibration.txt",np.vstack((TruePower,ReadPower)).T,comments="TruePower Read at Input[dBm]      ReadPower for BER Measurement [dBm]")
    
    

    
    

    

