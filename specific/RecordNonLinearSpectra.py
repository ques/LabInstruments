# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 12:19:59 2022

@author: FTNK-LocalAdm
"""

import numpy as np
import sys,os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


from instruments.SantecLaserPowerMeter import SantecLaser,SantecPowerMeter,getSpectrum
from instruments.Thorlabs_PowerMeter import PM320



laser = SantecLaser()
pw = SantecPowerMeter()

powermeter = PM320()


#%%
path = r"U:\Ftnk\COMMUNICATION\Ques\Bowtie_WavelengthConv\NonLinearTransmissionMeasurement\H0_03x07_Bis\\"

p_in = np.arange(0,15,1)
p_in = np.concatenate((p_in,np.flip(p_in)))
p_in = np.concatenate((p_in,p_in))
wl_start = 1530
wl_stop = 1550
wl_step = 0.01
wl_speed = 10

meas_pin = []

for i,laser_power in enumerate(p_in):
    
    laser.setPower(laser_power)
    done = False
    while not done:
        try:
            save_dat = getSpectrum(laser, pw, wl_start, wl_stop, wl_step, wl_speed)
            done=True
        except:
            done=False
    np.savetxt(path+f'\\NonLinearTransmission_{i:.0f}.tab',np.array(save_dat).T,header=f"Laser Power={laser_power:.3f}dBm\nStart WL={wl_start:.3f}nm\nStop WL={wl_stop:.3f}nm\nStep={wl_step:.3f}nm\nSpeed={wl_speed:.3f}nm/s")
    
    meas_pin.append(powermeter.getPower(1))
    
np.savetxt(path+'\\NonLinearTransmission_Powers.tab',np.array(meas_pin).T)