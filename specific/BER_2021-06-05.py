# -*- coding: utf-8 -*-
"""
Created on Thu May  6 13:25:28 2021

@author: FTNK-LocalAdm
"""

import matplotlib.pyplot as plt
import numpy as np

import sys,os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from instruments.DataInstruments import MP1764C,MP1763C,generate_bit_pattern
import time

from instruments.FullReceiver import RX
from instruments.SCFiberSwitch import SCFiberSwitch
from instruments.HP_PowerMeter import HP_PowerMeter
from instruments.Agilent_DCA import AgilentDCA
from instruments.OSA import OSA
from instruments.Keithley_2450 import Keithley_2450

import pickle
import os,sys
import scipy.fftpack as fft

p_in = HP_PowerMeter("GPIB0::13::INSTR")
rx = RX()
osc = AgilentDCA("GPIB0::2::INSTR")
osa = OSA("GPIB0::21::INSTR")
switch = SCFiberSwitch()
keith = Keithley_2450(debug=True)
# dso = AgilentDSO()

ea = MP1764C()
bpg = MP1763C()
ea.connect()
bpg.connect()



plotOnEye=False



def getBER(path,meas_name,rx_power,pd_power,wl0,
        osa_span=10,osa_npoints=2001,osa_trace='A',osa_sens='MID',
        acqOSA=True,acqEye=True,acqOSNR=False,acqBER=True,
        manual_opt=False,pump_wl=None):
    
    if not os.path.exists(path):
        os.mkdir(path)
    
    p_pump = p_in.getPower(1)
    p_probe = p_in.getPower(2)
    
    rx.closePD()
    rx.attn.setAttn(40,2)
    rx.openRX()
    
    if np.abs(rx.getPowerRX()-rx_power[0])>0.1:
        rx.setPowerRX(rx_power[0],pd_power=pd_power)
    else:
        print("No change : already reached")
        rx.openPD()
        rx.setPowerPD(pd_power)
    
    rxPower = np.array([])
    pdPower = np.array([])
    BER = np.array([])
    stdBER = np.array([])
    
    rx.openPD()
    
# =============================================================================
#     Get the BER file ready
# =============================================================================
    if acqBER:
        res_name = path + '\\' + meas_name + f'_BER_results_InputTotal@{p_pump:.2f}dBm_TLS@{p_probe:.2f}dBm'
        try:
            res_file = open(res_name+'.txt','r')
            res_file.close()
        except:
    
            print('File empty : adding headers')
            res_file = open(res_name+'.txt','a')
            res_file.write('#BER  StdBER   Received Power[dBm]   Target Power[dBm]   PD Power [dBm]\n')
            res_file.close()
    
    
    
# =============================================================================
#       Acquire OSA
# =============================================================================
        
    if acqOSA:
        print("OSA WSS Output")
        fname  = path + "\\"+meas_name+f'_OSA_Waveshaper_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(1)
        if pump_wl is not None:
            X,Y = osa.takeSingle(pump_wl,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints) 
        else:
            X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title("Input Spectrum to Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        plt.ylim(-75,-10)
        plt.savefig(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
        plt.close()
        
        print("OSA Input")
        fname  = path + "\\"+meas_name+f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(3)
        X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title("Input Spectrum to Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        plt.ylim(-75,-10)
        plt.savefig(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
        plt.close()
        
        if pump_wl is not None:
            print("OSA Pump Input")
            fname  = path + "\\"+meas_name+f'_OSA_Pump_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
            switch.switchTo(3)
            X,Y = osa.takeSingle(pump_wl,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)
            np.savetxt(fname,np.vstack((X,Y)).T)
            fig = plt.figure()
            plt.title("Input Pump Spectrum to Device")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            plt.ylim(-75,-10)
            plt.savefig(path + "\\"+meas_name + f'_OSA_Pump_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
        #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
            plt.close()
    #    
    
        print("OSA Output")
        fname  = path + "\\"+meas_name+f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(4)
        X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.02,trace=osa_trace,sens="HIGH2",npoints=osa_npoints)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title("Output Spectrum from Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        plt.ylim(-75,-10)
        plt.savefig(path + "\\"+meas_name + f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
        plt.close()
        
        if pump_wl is not None:
            print("OSA Pump Output")
            fname  = path + "\\"+meas_name+f'_OSA_Pump_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
            switch.switchTo(4)
            X,Y = osa.takeSingle(pump_wl,osa_span,sync=False,res=0.02,trace=osa_trace,sens="HIGH2",npoints=osa_npoints)
            np.savetxt(fname,np.vstack((X,Y)).T)
            fig = plt.figure()
            plt.title("Output Pump Spectrum from Device")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            plt.ylim(-75,-10)
            plt.savefig(path + "\\"+meas_name + f'_OSA_Pump_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
        #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
            plt.close()
    #    

        print("Received Spectrum")
        fname  = path + "\\"+meas_name+f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(5)
        X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)    
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title("Received Spectrum to Rx")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        plt.ylim(-75,-10)
        plt.savefig(path + "\\"+meas_name + f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
        plt.close() 
        
        

    
    
    for p in rx_power:
        print(f"Step Power : {p:.2f}dBm")
# =============================================================================
#       Adjust power on receiver
# =============================================================================
        if np.abs(rx.getPowerRX()-p)>0.1:
            rx.setPowerRX(p,pd_power = pd_power)
            
        else:
            print("No change : already reached")
            rx.setPowerPD(pd_power)
        time.sleep(1)
        pw_rx = rx.getPowerRX()
        pw_pd = rx.getPowerPD()
        
        rxPower = np.concatenate((rxPower,[pw_rx]))
        pdPower = np.concatenate((pdPower,[pw_pd]))
        
# =============================================================================
#       Optimize BER
# =============================================================================
        if acqBER:
            if manual_opt:
                input("Press enter when optimized : ")
            else:
                ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=1)
                ea.set_integration_time(0,1)
                ea.single()
                ea.start()
                time.sleep(1)
                tmp_BER = ea.get_error_ratio()
                if tmp_BER < 1e-8:
                    print("Further optimization")
                    ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=5)
                
            tmp_BER,int_seq = ea.get_coarse_BER()
            if tmp_BER == 1 : 
                input("Press enter when optimized : ")
                tmp_BER,int_seq = ea.get_coarse_BER()
            
            
            
            n_iter = 4
            tmp_BER = np.array([])
            int_seq = 2
            for i in np.arange(0,n_iter):
                print(30*'\b'+f'Iter : {i+1:.0f}/{n_iter:.0f}',end='')
                tmp,int_seq =  ea.get_accurate_BER(min_error=10,int_sec=int_seq,max_int_sec=2*60)
                tmp_BER = np.concatenate((tmp_BER,[tmp]))
                
                
            
            BER = np.concatenate((BER,[np.mean(tmp_BER)]))
            stdBER = np.concatenate((stdBER,[np.std(tmp_BER)]))
            
            
            res_file = open(res_name+'.txt','a')
            res_file.write(f'{np.mean(tmp_BER):.4e}  {np.std(tmp_BER):.4e}  {pw_rx:.4f}  {p:.4f} {pw_pd:.4f} \n')
            res_file.close()
            print(30*'\b'+f"BER : {np.mean(tmp_BER):.4e}  {np.std(tmp_BER):.4e}  RX Target Power : {p:.2f}  RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")
        
            ea.set_integration_time(0,1)
            ea.continuous()
            ea.start()
# =============================================================================
#       Get the eye diagram
# =============================================================================
        if acqEye:
            x,y = osc.getEye(40)
            fname  = path + "\\"+meas_name+f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
            np.savetxt(fname,np.vstack((x,y)).T)
            
            plt.figure()
            plt.title(f"RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")
            plt.scatter(x*1e12,y,0.2)
            plt.xlabel("Time [ps]")
            plt.ylabel("Voltage [V]")
            plt.savefig(path + "\\"+meas_name + f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png')
    #        pickle.dump(fig, open(path + "\\"+meas_name + f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.pickle', 'wb'))
            if not plotOnEye:
                plt.close('all')   
                
                
                
# =============================================================================
#       OSA Acquisition for OSNR
# =============================================================================
        if acqOSNR:
            fname  = path + "\\"+meas_name+f'_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
            switch.switchTo(6)
            X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.1,trace=osa_trace,sens="MID",npoints=osa_npoints) 
            np.savetxt(fname,np.vstack((X,Y)).T)
            plt.figure()
            plt.title(f"Received Spectrum to Rx")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            plt.ylim(-75,-10)
            plt.savefig(path + "\\"+meas_name + f'_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png',format='png')
    #        pickle.dump(fig, open(path + "\\"+meas_name + f'_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.pickle', 'wb'))
            plt.close()  
            
    return BER,stdBER,rxPower,pdPower
            
            
# path = r"D:\HERO_Characterization\WavelengthConv_H0_Passive_05x02_2021-05-10\Meas02"
# BER,stdBER,rxPower,pdPower = getBER(path,'H0_05x02_10G_70GPD',np.arange(-30,-42,-2),pd_power=0,wl0=1550.29,pump_wl=None,manual_opt=True,
#                                  acqOSA=False,acqEye=True,acqOSNR=False,acqBER=False,osa_trace='A',osa_npoints=2001)








