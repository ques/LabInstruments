# -*- coding: utf-8 -*-
"""
Created on Tue Feb  1 10:47:41 2022

@author: FTNK-LocalAdm
"""

import os,sys
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
import numpy as np
import time

from instruments.Agilent_DCA import AgilentDCA
from instruments.

dca = AgilentDCA()

#%%
path = r"U:\Ftnk\COMMUNICATION\Ques\Bowtie_WavelengthConv\Calibration_Transmitter\MLL_FilteredPulses_EyeDiagram_70GHzPD\\"

fname = "PulsedSignal_NonModulated_1530nm_220GHzSpan_-2dBm"
inttime = 10
X,Y = dca.getEyeMask(2,inttime)
np.savetxt(path+fname+".txt",np.vstack((X,Y)).T,header=f"Integration time = {inttime:.2f}s\n# PD Power = -2dBm\n# CenterWavelength = 1530nm\n # Filter BW = 220GHz")
dca.saveScreenImage(path+fname+".jpg")