# -*- coding: utf-8 -*-
"""
Created on Tue Feb  1 09:05:09 2022

@author: FTNK-LocalAdm
"""

import os,sys
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
import numpy as np
import time


from instruments.OSA_Anritsu import OSA_Anritsu
from instruments.SantecLaserPowerMeter import SantecLaser

laser = SantecLaser()
osa = OSA_Anritsu()
#%%

path = r"U:\Ftnk\COMMUNICATION\Ques\Bowtie_WavelengthConv\SantecLaserLinewidth\\"

tls_power = 0
laser.setPower(tls_power)
for wl in np.arange(1480,1621,10):
    laser.setWavelength(wl)
    time.sleep(2)
    X,Y = osa.takeSingle(wl, 5,False,0.03,rbw=200,npoints=1001)
    np.savetxt(path+f"LaserLinewidth_{wl:.0f}nm.txt",np.vstack((X,Y)).T,header=f"Center = {wl:.2f}nm\n# Span = 10nm\n# Resolution = 0.03nm\n# Bandwidth = 200Hz")
    