# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 12:25:25 2022

@author: FTNK-LocalAdm
"""

import numpy as np
import matplotlib.pyplot as plt
import time

import sys,os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from instruments.SantecLaserPowerMeter import SantecLaser,SantecPowerMeter,getSpectrum
from instruments.OZ_VOA import OZ_VOA
from instruments.SantecLaserPowerMeter import SantecLaser,SantecPowerMeter,getSpectrum
from instruments.OSA_Anritsu import OSA_Anritsu
from instruments.Agilent_Switch import AgilentSwitch


osa = OSA_Anritsu()
santec_laser = SantecLaser()
santec_power = SantecPowerMeter()
switch = AgilentSwitch()


#%%
path = r"U:\Ftnk\COMMUNICATION\Ques\L7_QMeasurement\\"
meas_name = "L7_NonOpt_02x02_Meas02"

tls_power = -14
wl_start = 1580.25
wl_stop = 1581
wl_step = 0.002
wl_sweep = np.arange(wl_start,wl_stop+wl_step/2,wl_step)
wl_res = []
P_osa = []
dwell = 0.1


osa_points = 51
osa_res = 1
osa_center = (wl_start+wl_stop)/2
osa_span = 2

osa.setCenter(osa_center)
osa.setBandwidth(100)
osa.setRes(osa_res)
osa.setSpan(osa_span)
osa.setSampPoints(osa_points)

switch.switchTo(1, 1)
switch.switchTo(2, 2)

santec_laser.setPower(tls_power)

for i,w in enumerate(wl_sweep):
    santec_laser.setWavelength(w)
    print(30*'\b'+str(i+1)+"/"+str(len(wl_sweep)),end='')
    time.sleep(dwell)
    osa.single()
    osa.wait()
    Y = osa.getY()
    P_osa.append(np.max(Y))
    
    
np.savetxt(path+meas_name+'.txt',np.vstack((wl_sweep,P_osa)).T)

