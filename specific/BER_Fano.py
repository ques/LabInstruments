# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 17:15:40 2021

@author: ques
"""

import matplotlib.pyplot as plt
import numpy as np
from DataInstruments import MP1764C,MP1763C,generate_bit_pattern
import time
from BasicInstruments import RX,AgilentOSC,SCFiberSwitch,HP_PowerMeter,AgilentDSO
from OSA import OSA
import pickle
import os,sys
import scipy.fftpack as fft


p_in = HP_PowerMeter("GPIB0::7::INSTR")
rx = RX()
osc = AgilentOSC()
osa = OSA()
switch = SCFiberSwitch()
dso = AgilentDSO()

ea = MP1764C()
bpg = MP1763C()
ea.connect()
bpg.connect()



#%%Set pattern on BPG and EA
n=15
period=10
pattern = generate_bit_pattern(2**n,period,q=0.5)

bpg.set_byte_array(pattern)
ea.set_byte_array(pattern)




#%%


plotOnEye=False


def saveEye(path,meas_name,n_iter=40):

        
        
    x,y = osc.getEye(n_iter)
    fname  = path + "\\"+meas_name+f'_Eye_10G.txt'
    np.savetxt(fname,np.vstack((x,y)).T)
    fig = plt.figure()
    plt.title(meas_name)
    plt.scatter(x*1e12,y,1)
    plt.xlabel("Time [ps]")
    plt.ylabel("Voltage [V]")
    plt.savefig(path + "\\"+meas_name + f'_Eye_10G.png',format='png')
#    pickle.dump(fig, open(path + "\\"+meas_name + f'_Eye_10G.pickle', 'wb'))
    plt.close()
    
def mapBER(n_int,n_volt=21,n_time=21,delta_volt=0.001,delta_time=1):
    ea.set_integration_time(0,n_int)
    ea.single()
    cur_volt = ea.get_data_threshold_voltage()
    cur_time = ea.get_clock_phase_delay()
    
    test_v = np.arange(cur_volt-np.floor(n_volt/2)*delta_volt,cur_volt+np.floor(n_volt/2)*delta_volt+0.5*delta_volt,delta_volt)
    test_t = np.arange(cur_time-np.floor(n_time/2)*delta_time,cur_time+np.floor(n_time/2)*delta_time+0.5*delta_time,delta_time)
    
    BER = np.zeros((len(test_v),len(test_t)))
    counter=0
    try:
        for i,v in enumerate(test_v):
            for j,t in enumerate(test_t):
                counter+=1
                ea.set_clock_phase_delay(t)
                ea.set_data_threshold_voltage(v)
                ea.start()
                time.sleep(n_int)
                ER = ea.get_error_ratio()
                BER[i,j] = ER
                print(30*'\b'+f'Iter : {counter:.0f}/{len(test_v)*len(test_t):.0f}',end='')
    except Exception as e:
        print("Exciting mapping : "+str(e))
            
    fig = plt.figure()
    TT,VV = np.meshgrid(test_t,test_v)
    plt.pcolor(TT,VV,-np.log10(BER))
    plt.ylabel('Voltage [V]')
    plt.xlabel('Time [ps]')
    plt.colorbar()
    
    maxBER = np.max(-np.log10(BER))
    
    ea.set_integration_time(0,1)
    ea.continuous()
    ea.start()
    
    
    
            
    
def BER(path,meas_name,rx_power,pd_power,wl0,
        osa_span=10,osa_npoints=2001,osa_trace='A',osa_sens='HIGH1',
        acqOSA=True,acqEye=True,acqOSNR=True,acqBER=True,
        manual_opt=False,pump_wl=None):
    
    if not os.path.exists(path):
        os.mkdir(path)
    
    p_pump = np.float(input("Enter input to Fano power in device : "))
    p_probe = np.float(input("Enter TLS power in device : "))
    
    rx.closePD()
#    rx.attn.setAttn(40,2)
    rx.openRX()
    
    if np.abs(rx.getPowerRX()-rx_power[0])>0.1:
        rx.setPowerRX(rx_power[0],pd_power=pd_power)
    else:
        print("No change : already reached")
        rx.openPD()
        rx.setPowerPD(pd_power)
    
    rxPower = np.array([])
    pdPower = np.array([])
    BER = np.array([])
    stdBER = np.array([])
    
    rx.openPD()
    
# =============================================================================
#     Get the BER file ready
# =============================================================================
    if acqBER:
        res_name = path + '\\' + meas_name + f'_BER_results_InputTotal@{p_pump:.2f}dBm_TLS@{p_probe:.2f}dBm'
        try:
            res_file = open(res_name+'.txt','r')
            res_file.close()
        except:
    
            print('File empty : adding headers')
            res_file = open(res_name+'.txt','a')
            res_file.write('#BER  StdBER   Received Power[dBm]   Target Power[dBm]   PD Power [dBm]\n')
            res_file.close()
    
    
    
# =============================================================================
#       Acquire OSA
# =============================================================================
    
    pin = p_in.getPower(channel=1)
    
    if acqOSA:
        print("OSA WSS Output")
        fname  = path + "\\"+meas_name+f'_OSA_Waveshaper_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(1)
        if pump_wl is not None:
            X,Y = osa.takeSingle(pump_wl,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints) 
        else:
            X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title(f"Input Spectrum to Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        plt.ylim(-75,-10)
        plt.savefig(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
        plt.close()
        
        print("OSA Input")
        fname  = path + "\\"+meas_name+f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(2)
        X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title(f"Input Spectrum to Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        plt.ylim(-75,-10)
        plt.savefig(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
        plt.close()
        
        if pump_wl is not None:
            print("OSA Pump Input")
            fname  = path + "\\"+meas_name+f'_OSA_Pump_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
            switch.switchTo(2)
            X,Y = osa.takeSingle(pump_wl,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)
            np.savetxt(fname,np.vstack((X,Y)).T)
            fig = plt.figure()
            plt.title(f"Input Pump Spectrum to Device")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            plt.ylim(-75,-10)
            plt.savefig(path + "\\"+meas_name + f'_OSA_Pump_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
        #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Input_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
            plt.close()
    #    
    
        print("OSA Output")
        fname  = path + "\\"+meas_name+f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(3)
        X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title(f"Output Spectrum from Device")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        plt.ylim(-75,-10)
        plt.savefig(path + "\\"+meas_name + f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
        plt.close()
        
        if pump_wl is not None:
            print("OSA Pump Output")
            fname  = path + "\\"+meas_name+f'_OSA_Pump_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
            switch.switchTo(3)
            X,Y = osa.takeSingle(pump_wl,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)
            np.savetxt(fname,np.vstack((X,Y)).T)
            fig = plt.figure()
            plt.title(f"Output Pump Spectrum from Device")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            plt.ylim(-75,-10)
            plt.savefig(path + "\\"+meas_name + f'_OSA_Pump_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
        #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Output_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
            plt.close()
    #    

        print("Received Spectrum")
        fname  = path + "\\"+meas_name+f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.txt'
        switch.switchTo(4)
        X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.02,trace=osa_trace,sens=osa_sens,npoints=osa_npoints)    
        np.savetxt(fname,np.vstack((X,Y)).T)
        fig = plt.figure()
        plt.title(f"Received Spectrum to Rx")
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.plot(X,Y)
        plt.ylim(-75,-10)
        plt.savefig(path + "\\"+meas_name + f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.png',format='png')
    #    pickle.dump(fig, open(path + "\\"+meas_name + f'_OSA_Received_{p_pump:.2f}dBm_{p_probe:.2f}dBm.pickle', 'wb'))
        plt.close() 
        
        

    
    
    for p in rx_power:
        print(f"Step Power : {p:.2f}dBm")
# =============================================================================
#       Adjust power on receiver
# =============================================================================
        if np.abs(rx.getPowerRX()-p)>0.1:
            rx.setPowerRX(p,pd_power = pd_power)
            
        else:
            print("No change : already reached")
            rx.setPowerPD(pd_power)
        time.sleep(1)
        pw_rx = rx.getPowerRX()
        pw_pd = rx.getPowerPD()
        
        rxPower = np.concatenate((rxPower,[pw_rx]))
        pdPower = np.concatenate((pdPower,[pw_pd]))
        
# =============================================================================
#       Optimize BER
# =============================================================================
        if acqBER:
            if manual_opt:
                input("Press enter when optimized : ")
            else:
                ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=1)
                ea.set_integration_time(0,1)
                ea.single()
                ea.start()
                time.sleep(1)
                tmp_BER = ea.get_error_ratio()
                if tmp_BER < 1e-8:
                    print("Further optimization")
                    ea.optimize_BER(volt_step=0.001,time_step=1,int_seq=5)
                
            tmp_BER,int_seq = ea.get_coarse_BER()
            if tmp_BER == 1 : 
                input("Press enter when optimized : ")
                tmp_BER,int_seq = ea.get_coarse_BER()
            
            
            
            n_iter = 4
            tmp_BER = np.array([])
            int_seq = 2
            for i in np.arange(0,n_iter):
                print(30*'\b'+f'Iter : {i+1:.0f}/{n_iter:.0f}',end='')
                tmp,int_seq =  ea.get_accurate_BER(min_error=10,int_sec=int_seq,max_int_sec=2*60)
                tmp_BER = np.concatenate((tmp_BER,[tmp]))
                
                
            
            BER = np.concatenate((BER,[np.mean(tmp_BER)]))
            stdBER = np.concatenate((stdBER,[np.std(tmp_BER)]))
            
            
            res_file = open(res_name+'.txt','a')
            res_file.write(f'{np.mean(tmp_BER):.4e}  {np.std(tmp_BER):.4e}  {pw_rx:.4f}  {p:.4f} {pw_pd:.4f} \n')
            res_file.close()
            print(30*'\b'+f"BER : {np.mean(tmp_BER):.4e}  {np.std(tmp_BER):.4e}  RX Target Power : {p:.2f}  RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")
        
            ea.set_integration_time(0,1)
            ea.continuous()
            ea.start()
# =============================================================================
#       Get the eye diagram
# =============================================================================
#        if acqEye:
#            x,y = osc.getEye(40)
#            fname  = path + "\\"+meas_name+f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
#            np.savetxt(fname,np.vstack((x,y)).T)
#            
#            plt.figure()
#            plt.title(f"RX Power : {pw_rx:.2f}dBm  PD Power : {pw_pd:.2f}dBm")
#            plt.scatter(x*1e12,y,0.2)
#            plt.xlabel("Time [ps]")
#            plt.ylabel("Voltage [V]")
#            plt.savefig(path + "\\"+meas_name + f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png')
#    #        pickle.dump(fig, open(path + "\\"+meas_name + f'_Eye_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.pickle', 'wb'))
#            if not plotOnEye:
#                plt.close('all')
                
                
# =============================================================================
#       Get the waveform from DSO
# =============================================================================
            
        if acqEye:
            x,y = dso.getWaveform(3,100000)
            fname  = path + "\\"+meas_name+f'_WaveForm_10G_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
            np.savetxt(fname,np.vstack((x,y)).T)   
            dso.run()
            
# =============================================================================
#       OSA Acquisition for OSNR
# =============================================================================
        if acqOSNR:
            fname  = path + "\\"+meas_name+f'_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
            switch.switchTo(5)
            X,Y = osa.takeSingle(wl0,osa_span,sync=False,res=0.1,trace=osa_trace,sens="MID",npoints=osa_npoints) 
            np.savetxt(fname,np.vstack((X,Y)).T)
            plt.figure()
            plt.title(f"Received Spectrum to Rx")
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Power [dBm]')
            plt.plot(X,Y)
            plt.ylim(-75,-10)
            plt.savefig(path + "\\"+meas_name + f'_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png',format='png')
    #        pickle.dump(fig, open(path + "\\"+meas_name + f'_OSNR_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.pickle', 'wb'))
            plt.close()             
            

#        try:
#            data = np.loadtxt(res_name+'.txt',skiprows=1)
#            BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
#            fig,ax = plt.subplots()
#            ax.semilogy(pw_rx,-np.log10(BER),'x-')
#            ax.set_title(f'BER with P_PD={np.mean(pw_pd):.2f}dBm')   
#            ax.grid(which='both')
#            ax.invert_yaxis()
#            plt.savefig(res_name+'.png',format='png')
#        except Exception as e:
#            print("Couln't plot BER : "+str(e))
        
    return BER,stdBER,rxPower,pdPower


#%%
path = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\B2B_5G_1567nm"
BER,stdBER,rxPower,pdPower = BER(path,'B2B_5G_1567nm_FastPD',np.arange(-30,-47,-1),pd_power=0,wl0=1567,pump_wl=None,manual_opt=True,
                                 acqOSA=False,acqEye=True,acqOSNR=False,acqBER=False,osa_trace='A',osa_npoints=2001)





#%% Get Eye diagram

data = np.loadtxt(r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-03\B2B_Meas01\H1_02x11_Pump_FastPD_WaveForm_10G_-36.047dBm_1.500dBm.txt")
win_period=8
X = data[:,0]
X = X-np.min(X)
Y = data[:,1]

nbin = 2**25

spec = np.abs(fft.fft(Y,n=nbin))
freq = fft.fftfreq(nbin,d=np.abs(np.mean(X[0:-2]-X[1:-1])))

spec = spec[np.bitwise_and(freq>8e6,freq<10e9)]
freq = freq[np.bitwise_and(freq>8e6,freq<10e9)]

f_mod = freq[np.argmax(spec)]
p_mod = 1/f_mod

plt.figure()
plt.plot(freq,spec)
plt.plot(f_mod,spec[np.argmax(spec)],'xr')

fig,ax = plt.subplots()
index = 0
while index<len(X)-1:
    next_index = np.argmin(np.abs(X[index]+p_mod*win_period-X))
    ax.scatter(X[index:next_index]*1e12-X[index]*1e12,Y[index:next_index],s=0.2,color='b')
    index=next_index
    

    
#%%

X,Y = osa.takeSingle(1548,50,True,2,'A',sens='MID',npoints=1001)
path = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-06\Transmission\\"
B2B = None
    
#B2B = np.loadtxt(r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\Transmission\B2B_1548nm_Wide_0dBm_OSA.txt")
#X_BK,Y_BK = B2B[:,0],B2B[:,1]

meas = 'H1_01x02_WG_0dBm'
#meas = 'B2B_1548nm_Wide_0dBm'
fig = plt.figure()
if 'B2B' in meas or B2B is None or len(X_BK)!=len(X): 
    plt.title('Transmission of '+meas+'  -  B2B not removed')
    plt.plot(X,Y)
else:
    print("B2B removed")
    plt.title('Transmission of '+meas+'  -  B2B removed')
    plt.plot(X,Y-Y_BK)
    plt.ylim([-90,0])
plt.xlabel('Wavelength [nm]')
plt.ylabel("Power [dBm]")
plt.grid(which='both')


plt.savefig(path+meas+'_OSA.png')
#pickle.dump(fig, open(path+meas+'_OSA.pickle', 'wb'))
np.savetxt(path+meas+'_OSA.txt',np.vstack((X,Y)).T,header=f"Resolution : 2nm\nSensitivity : MID\n")  
#plt.close('all')  




#%%
plt.close('all')
fig,ax = plt.subplots()

if True:
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\10G_WavelengthConversion_H0_02x09\H0_02x09_Signal_Meas05\H0_02x09_ConvSignal_BER_results_InputTotal@4.03dBm_TLS@6.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='Meas05',marker='.')
    
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\10G_WavelengthConversion_H0_02x09\H0_02x09_Signal_Meas06\H0_02x09_ConvSignal_BER_results_InputTotal@7.75dBm_TLS@6.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='Meas06',marker='.')
    
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\10G_WavelengthConversion_H0_02x09\H0_02x09_Signal_Meas07\H0_02x09_ConvSignal_BER_results_InputTotal@9.30dBm_TLS@6.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='Meas07',marker='.')
    
    #
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\10G_WavelengthConversion_H0_02x09\B2B_Meas01\H0_02x09_Pump_BER_results_InputTotal@-0.20dBm_TLS@-200.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='B2B Pump 10G',marker='.')

if False:
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-03\B2B_Meas01\H1_02x11_Pump_BER_results_InputTotal@9.20dBm_TLS@-200.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='B2B Pump 2.5G',marker='.')
    
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-03\H1_02x11_Signal_Meas01\H1_02x11_ConvSignal_BER_results_InputTotal@10.50dBm_TLS@6.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='H1 Meas1 Auto',marker='.')
    
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-03\H1_02x11_Signal_Meas02\H1_02x11_ConvSignal_BER_results_InputTotal@6.58dBm_TLS@6.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='H1 Meas1 Cross',marker='.')

if False:
    
#    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-03\B2B_Meas01\H1_02x11_Pump_BER_results_InputTotal@9.20dBm_TLS@-200.00dBm.txt"
#    data = np.loadtxt(fname,skiprows=1)
#    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
#    indx = np.argsort(pw_rx)
#    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
#    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='B2B Pump 2.5G 1533nm',marker='.')
##    
#    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-04\B2B_Meas01\H1_01x10_Pump_BER_results_InputTotal@5.70dBm_TLS@-200.00dBm.txt"
#    data = np.loadtxt(fname,skiprows=1)
#    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
#    indx = np.argsort(pw_rx)
#    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
#    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='B2B Pump 2.5G 1567nm',marker='.')
#    
#    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-04\B2B_Meas02\H1_01x10_Pump_BER_results_InputTotal@5.20dBm_TLS@-200.00dBm.txt"
#    data = np.loadtxt(fname,skiprows=1)
#    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
#    indx = np.argsort(pw_rx)
#    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
#    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='B2B Pump 2.5G 1564nm',marker='.')
    
#    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\B2B_NoHighPower_1567nm\B2B_Pump_NoHighPowerEDFA_BER_results_InputTotal@-10.00dBm_TLS@-200.00dBm.txt"
#    data = np.loadtxt(fname,skiprows=1)
#    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
#    indx = np.argsort(pw_rx)
##    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
#    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='B2B 1567nm 100GHz',marker='.')
#    
#    
    
#    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-04\H1_01x10_Meas01\H1_01x10_Signal_BER_results_InputTotal@10.86dBm_TLS@6.00dBm.txt"
#    data = np.loadtxt(fname,skiprows=1)
#    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
#    indx = np.argsort(pw_rx)
#    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
#    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='H1 01x10 Cross Mode',marker='.')
#    
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\H1_01x10_Meas02\H1_01x10_Signal_BER_results_InputTotal@11.40dBm_TLS@0.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='H1 01x10 Cross Mode 100GHz',marker='.')
    
#    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\H1_01x10_Meas03\H1_01x10_Signal_BER_results_InputTotal@13.40dBm_TLS@0.00dBm.txt"
#    data = np.loadtxt(fname,skiprows=1)
#    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
#    indx = np.argsort(pw_rx)
##    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
#    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='H1 01x10 Cross Mode 100GHz ',marker='.')
#    
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\H1_01x10_Meas04\H1_01x10_Signal_BER_results_InputTotal@12.42dBm_TLS@0.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
#    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='H1 01x10 Cross Mode 200GHz',marker='.')
    
    
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\H1_01x10_Meas5\H1_01x10_Auto_BER_results_InputTotal@8.99dBm_TLS@0.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
#    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='H1 01x10 Auto Mode 100GHz',marker='.')
    
    
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\H1_01x10_Meas06\H1_01x10_Signal_BER_results_InputTotal@10.60dBm_TLS@0.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
#    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='H1 01x10 Auto Mode 100GHz',marker='.')
    
    
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\B2B_HighPower_1567nm\H1_01x10_Pump_B2B_BER_results_InputTotal@2.20dBm_TLS@-200.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='B2B 100GHz 1567nm Through Device',marker='.')
    
        
    fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\B2B_HighPowerLEDFA_NoDevice\B2B_NoDevice_HighPowerLEDFA_BER_results_InputTotal@-0.60dBm_TLS@-200.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='B2B 100GHz 1567nm No Device',marker='.')
    
if False: 
    fname = r"U:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\H1_01x10_Meas07\H1_01x10_Signal_5G_BER_results_InputTotal@10.20dBm_TLS@6.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='H1 01x10 Cross Mode 200GHz 5G',marker='.')
    
    fname = r"U:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-02-05\B2B_5G_1567nm\B2B_5G_1567nm_BER_results_InputTotal@11.50dBm_TLS@-200.00dBm.txt"
    data = np.loadtxt(fname,skiprows=1)
    BER,stdBER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,2],data[:,4]
    indx = np.argsort(pw_rx)
    BER,stdBER,pw_rx,pw_pd = BER[indx],stdBER[indx],pw_rx[indx],pw_pd[indx]
    ax.errorbar(pw_rx,-np.log10(BER),yerr=-1/BER*np.log10(np.exp(1))*stdBER*1.96,fmt='-',label='B2B no Device 200GHz 5G',marker='.')
    


ax.invert_yaxis()
ax.set_yscale('log')
ax.set_yticks(np.arange(1,14))
ax.set_yticklabels([str(i) for i in np.arange(1,14)])
ax.grid(which='both')
ax.legend()
ax.set_ylabel('-log(BER)')
ax.set_xlabel('Received Power [dBm]')


#%%

fig,ax= plt.subplots()
fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\10G_WavelengthConversion_H0_02x09\H0_02x09_Signal_Meas05\H0_02x09_ConvSignal_OSA_Input_4.03dBm_6.00dBm.txt"
data = np.loadtxt(fname,skiprows=1)
wl,t = data[:,0],data[:,1]
ax.plot(wl,t,label='Meas05')

fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\10G_WavelengthConversion_H0_02x09\H0_02x09_Signal_Meas06\H0_02x09_ConvSignal_OSA_Input_7.75dBm_6.00dBm.txt"
data = np.loadtxt(fname,skiprows=1)
wl,t = data[:,0],data[:,1]
ax.plot(wl,t,label='Meas06')
fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\10G_WavelengthConversion_H0_02x09\H0_02x09_Signal_Meas07\H0_02x09_ConvSignal_OSA_Input_9.08dBm_6.00dBm.txt"
data = np.loadtxt(fname,skiprows=1)
wl,t = data[:,0],data[:,1]
ax.plot(wl,t,label='Meas07')
#fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\10G_WavelengthConversion_H0_02x09\H0_02x09_Signal_Meas09\H0_02x09_ConvSignal_OSA_Input_6.40dBm_6.00dBm.txt"
#data = np.loadtxt(fname,skiprows=1)
#wl,t = data[:,0],data[:,1]
#ax.plot(wl,t,label='Meas09')
fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\10G_WavelengthConversion_H0_02x09\B2B_Meas01\H0_02x09_Pump_OSA_Input_-0.20dBm_-200.00dBm.txt"
data = np.loadtxt(fname,skiprows=1)
wl,t = data[:,0],data[:,1]
ax.plot(wl,t,label='B2B')
ax.legend()




#%% BER versus OSNR


fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-01-28\BER_Pump_H0_02x08_PreAmp_Meas1\H0_02x08_PreAmp_BER_results_-6.60dBm_-200.00dBm.txt"
data = np.loadtxt(fname,skiprows=1)
BER,pw_rx,pw_pd = data[:,0],data[:,2],data[:,4]


for rx,pd in zip(pw_rx,pw_pd):
    try:
        data = np.loadtxt(os.path.dirname(fname)+'\H0_02x08_PreAmp'+f'_OSNR_{rx:.3f}dBm_{pd:.3f}dBm.txt')
    except:

    wl = data[:,0]
    power = data[:,1]
    
    indx = np.argmax(power)



#plt.close('all')
#fig,ax = plt.subplots()
#
#fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-01-26\B2B_BER_TRxOnly_LowerRxEDFA\B2B_TRx_BER_results_-200.00dBm_-200.00dBm.txt"
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,2],data[:,4]
#ax.semilogy(pw_rx,-np.log10(BER),'x-',label='Half Power on EDFA')
#fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-01-26\B2B_BER_TRxOnly\B2B_TRx_BER_results_-200.00dBm_-200.00dBm.txt"
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,2],data[:,4]
#ax.semilogy(pw_rx,-np.log10(BER),'x-',label='Max Power on EDFA')
#
#fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-01-26\B2B_BER_TRxOnly_LowerRxEDFA_2\B2B_TRx_BER_results_-200.00dBm_-200.00dBm.txt"
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,2],data[:,4]
#ax.semilogy(pw_rx,-np.log10(BER),'x-',label='Half Power on EDFA NoAttenuator')
#
#fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-01-26\B2B_BER_TRxOnly_LowerRxEDFA_3\B2B_TRx_BER_results_-200.00dBm_-200.00dBm.txt"
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,2],data[:,4]
#ax.semilogy(pw_rx,-np.log10(BER),'x-',label='Half Power on EDFA NoAttenuator 0.8nm filter')
#
#fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-01-27\B2B_TRx\B2B_TRx_BER_results_-200.00dBm_-200.00dBm.txt"
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,2],data[:,4]
#ax.semilogy(pw_rx,-np.log10(BER),'x-',label='Half Power on EDFA NoAttenuator 0.8nm filter')
#
#fname = r"O:\Ftnk\COMMUNICATION-ULTRA-FAST\_Personal folders\FANO\FanoMeasurements\H0_H1_Fano_2020-02-15_Bis2021\2021-01-27\B2B_TRx_2\B2B_TRx_BER_results_-200.00dBm_-200.00dBm.txt"
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,2],data[:,4]
#ax.semilogy(pw_rx,-np.log10(BER),'x-',label='Half Power on EDFA NoAttenuator 0.8nm filter')


#
##fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan5_InChip"+f'\Meas1_BER_results.txt'
##data = np.loadtxt(fname,skiprows=1)
##BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
##ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan5_OutChip"+f'\Meas2_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_m5_OutChip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
##
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_5_MUX_L5m5_Outchip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#fname = r"M:\Experiment\3_OOK_LongOAM\BER_measure\chan_m5_MUX_L5m5_Outchip"+f'\Meas1_BER_results.txt'
#data = np.loadtxt(fname,skiprows=1)
#BER,pw_rx,pw_pd = data[:,0],data[:,1],data[:,3]
#ax.semilogy(pw_rx,-np.log10(BER),'x-')
#
#ax.set_title(f'BER with P_PD={np.mean(pw_pd):.2f}dBm')   
#ax.grid(which='both')
#ax.invert_yaxis()
    
    