# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 16:12:46 2022

@author: FTNK-LocalAdm
"""

import matplotlib.pyplot as plt
import numpy as np

import sys,os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from instruments.DataInstruments import MP1764C,MP1763C,generate_bit_pattern
import time

from instruments.OZ_VOA import OZ_VOA
from instruments.FullReceiver import RX
from instruments.SCFiberSwitch import SCFiberSwitch
from instruments.HP_PowerMeter import HP_PowerMeter
from instruments.Agilent_DCA import AgilentDCA
from instruments.OSA_Anritsu import OSA_Anritsu
from instruments.Keithley_2450 import Keithley_2450
from instruments.Thorlabs_PowerMeter import PM320,PM100D
from instruments.SantecLaserPowerMeter import SantecLaser,SantecPowerMeter,getSpectrum,getWaveSweep
from instruments.WaveAnalyser import WaveAnalyser
from instruments.Agilent_Switch import AgilentSwitch
from instruments.WaveShapers import WaveShaper_4000A
import pickle
import os,sys
import scipy.fftpack as fft
from scipy.interpolate import interp1d

rx = RX()
osc = AgilentDCA()
osa = OSA_Anritsu()
waveanalyser = WaveAnalyser()
switch = SCFiberSwitch()
# keith = Keithley_2450(debug=True)
# dso = AgilentDSO()
p_in = PM320(addr="USB0::0x1313::0x8022::M00606447::INSTR")
santec_laser = SantecLaser()
santec_power = SantecPowerMeter()
voa = OZ_VOA("ASRL4::INSTR")  

switch = AgilentSwitch()
waveshaper = WaveShaper_4000A()


ea = MP1764C()
bpg = MP1763C()
ea.connect()
bpg.connect()


#%%
def record_point(path,meas_name,pw_rx,pw_pd,osa_span,osa_center,osa_pump_center,speed,pd,osc_channel=3,intTime=30):
    rx.setPowerRX(pw_rx,pd_power=pw_pd)
    
    if not os.path.exists(path):
        os.mkdir(path)
    
    instrument = "WaveAnalyser"
    # instrument = "OSA"
    
    if instrument == "WaveAnalyser":
        switch.switchTo(2, 3)
    elif instrument=="OSA":
        switch.switchTo(2, 2)
    
    x,y = osc.getEyeMask(channel=osc_channel,intTime=intTime)
    fname  = path + "\\"+f'Eye_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{meas_name:s}.txt'
    np.savetxt(fname,np.vstack((x,y)).T)
    
    osc.saveScreenImage(path + "\\"+f'Eye_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_Screen_{meas_name:s}.png')
    
    
    switch.switchTo(1,2)
    
    fname = path + "\\"+f'InputSpectrum_{instrument:s}_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{meas_name:s}.txt'
    if instrument == "WaveAnalyser":
        X,Y = waveanalyser.getSpectrum(osa_center)
    elif instrument=="OSA":
        X,Y = osa.takeSingle(osa_center,osa_span,sync=False,res=0.03,rbw=100,trace='A',npoints=1001) 

    np.savetxt(fname,np.vstack((X,Y)).T)
    fig = plt.figure()
    plt.title("Input Spectrum to Rx")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    fname = path + "\\"+f'InputSpectrum_{instrument:s}_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{meas_name:s}.png'
    plt.savefig(fname,format='png')
    plt.close() 

    
    switch.switchTo(1,4)
    
    fname = path + "\\"+f'ReceivedSpectrum_{instrument:s}_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{meas_name:s}.txt'
    if instrument == "WaveAnalyser":
        X,Y = waveanalyser.getSpectrum(osa_center)
    elif instrument=="OSA":
        X,Y = osa.takeSingle(osa_center,osa_span,sync=False,res=0.03,rbw=100,trace='A',npoints=1001) 

    np.savetxt(fname,np.vstack((X,Y)).T)
    fig = plt.figure()
    plt.title("Received Spectrum to Rx")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    fname = path + "\\"+f'ReceivedSpectrum_{instrument:s}_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm_{meas_name:s}.png'
    plt.savefig(fname,format='png')
    plt.close() 
    
    osc.oscMode()
    
    osc.oscMode()
    osc.run()
    
    
    
    
    
#%% Record OTDM Demux Sweep of 40GProbe
freq = np.arange(193.540,193.600,0.002)
freq_span = 0.055
skew=-4
path = r"U:\Ftnk\COMMUNICATION\Ques\D267001_21-12-10\H0_Mol_01x05_OTDM_Demux_4\40G_Demux_ProbeSweep_Meas01"
if not os.path.exists(path):
    os.mkdir(path)
pw_rx = -25
pw_pd=0
osa_span=5
osa_center = 1548
osa_pump_center = 1570
speed = "10G"
pd = "70GHzPD"


switch.switchTo(1,2)
switch.switchTo(2, 2)
X,Y = osa.takeSingle(osa_pump_center,osa_span,sync=False,res=0.03,rbw=100,trace='A',npoints=1001) 
fname = path + "\\"+f'InputSpectrum_Pump_OSA_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
np.savetxt(fname,np.vstack((X,Y)).T)
fig = plt.figure()
plt.title("Received Spectrum to Rx")
plt.xlabel('Wavelength [nm]')
plt.ylabel('Power [dBm]')
plt.plot(X,Y)
fname = path + "\\"+f'InputSpectrum_Pump_OSA_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png'
plt.savefig(fname,format='png')
plt.close() 
for i,f in enumerate(freq):
    print(30*'\b'+f"{i+1:.0f}/{len(freq):.0f}")
    
    waveshaper.setSkewedGaussPass(f, freq_span, 3,skew)
    rx.setPowerRX(pw_rx,pd_power=pw_pd,maxiter=20)
    # input("Press enter when ready : ")
    meas_name = f"WSS@{f:.3f}THz"
    record_point(path,meas_name,pw_rx,pw_pd,osa_span,osa_center,osa_pump_center,speed,pd,osc_channel=3)
    
#%%

path = r"U:\Ftnk\COMMUNICATION\Ques\D267001_21-12-10\H0_Mol_02x05_OTDM_Demux_4\Attempt_Meas03_Pulse#4_BER@1e-5"
if not os.path.exists(path):
    os.mkdir(path)
    
pw_rx = -20
pw_pd=0
osa_span=10
intTime=30
osa_center = 1552
osa_pump_center = 1573
speed = "40G_to_10G"
pd = "70GHzPD"

switch.switchTo(1,2)
switch.switchTo(2, 2)
X,Y = osa.takeSingle(osa_pump_center,osa_span,sync=False,res=0.03,rbw=100,trace='A',npoints=1001) 
fname = path + "\\"+f'InputSpectrum_Pump_OSA_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
np.savetxt(fname,np.vstack((X,Y)).T)
fig = plt.figure()
plt.title("Received Spectrum to Rx")
plt.xlabel('Wavelength [nm]')
plt.ylabel('Power [dBm]')
plt.plot(X,Y)
fname = path + "\\"+f'InputSpectrum_Pump_OSA_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png'
plt.savefig(fname,format='png')
plt.close() 


switch.switchTo(1,1)
switch.switchTo(2, 2)
X,Y = osa.takeSingle(osa_pump_center,osa_span,sync=False,res=0.03,rbw=100,trace='A',npoints=1001) 
fname = path + "\\"+f'OutputSpectrum_Pump_OSA_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.txt'
np.savetxt(fname,np.vstack((X,Y)).T)
fig = plt.figure()
plt.title("Received Spectrum to Rx")
plt.xlabel('Wavelength [nm]')
plt.ylabel('Power [dBm]')
plt.plot(X,Y)
fname = path + "\\"+f'OutputSpectrum_Pump_OSA_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png'
plt.savefig(fname,format='png')
plt.close() 


fig = plt.figure()
plt.title("Waveshaper Filter")
plt.xlabel('Wavelength [nm]')
plt.ylabel('IL [dB]')
plt.plot(waveshaper.current_filter[:,0],-waveshaper.current_filter[:,3])
fname = path + "\\"+f'WSS_FilterProbe_{pd:s}_{speed:s}_{pw_rx:.3f}dBm_{pw_pd:.3f}dBm.png'
plt.savefig(fname,format='png')
plt.close() 
np.savetxt(path+"\\"+"WSS_Filter.txt", waveshaper.current_filter)

p_pump = p_in.getPower(2)
p_probe = p_in.getPower(1)


record_point(path,meas_name=f"Pump@{p_pump:.2f}dBm_Probe@{p_probe:.2f}dBm",pw_rx=pw_rx,pw_pd=pw_pd,osa_span=osa_span,osa_center=osa_center,osa_pump_center=osa_pump_center,speed=speed,pd=pd,intTime=intTime)


# freq = np.arange(190.615,190.750,0.005)
# for i,f in enumerate(freq):
#     print(30*'\b'+f"{i+1:.0f}/{len(freq):.0f}")
#     waveshaper.setSkewedGaussPass(f, 0.055, 3,-4)
# record_point(path,meas_name=f"WSS@{f:.3f}THz_Pump@{p_pump:.2f}dBm_Probe@{p_probe:.2f}dBm",pw_rx=pw_rx,pw_pd=pw_pd,osa_span=osa_span,osa_center=osa_center,osa_pump_center=osa_pump_center,speed=speed,pd=pd,intTime=120)
# voa.addAttn(30)








#%%
path = r"U:\Ftnk\COMMUNICATION\Ques\D267001_21-12-10\H0_Mol_02x05_OTDM_Demux_2\Pump_Ramp_Meas01"
if not os.path.exists(path):
    os.mkdir(path)

switch.switchTo(1,3)
switch.switchTo(2, 3)
osa_center=1552
for i in range(20):
    voa.addAttn(-0.1)
    p_pump = p_in.getPower(2)
    X,Y = waveanalyser.getSpectrum(osa_center)
    fname = path + "\\"+f'ReceivedSpectrum_Pump@{p_pump:.3f}dBm.txt'
    np.savetxt(fname,np.vstack((X,Y)).T)
    fig = plt.figure()
    plt.title("Received Spectrum to Rx")
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Power [dBm]')
    plt.plot(X,Y)
    fname = path + "\\"+f'ReceivedSpectrum_Pump@{p_pump:.3f}dBm.png'
    plt.savefig(fname,format='png')
    plt.close() 
    
    
    
    
#%%

path = r"U:\Ftnk\COMMUNICATION\Ques\D267001_21-12-10\H0_Mol_02x05_OTDM_Demux_4\Bistablility_EyeDiagram_Meas02"
if not os.path.exists(path):
    os.mkdir(path)
    
pw_rx = -20
pw_pd=0
osa_span=10
intTime=30
osa_center = 1552
osa_pump_center = 1573
speed = "40G_to_10G"
pd = "70GHzPD"


cur_att = voa.getAttn()


sweep_att = np.arange(0,3,0.2)
sweep_att = np.concatenate((sweep_att,np.flip(sweep_att)))
p_pump = p_in.getPower(2)
p_probe = p_in.getPower(1)
done = []
for att in sweep_att:
    iteration = len(np.where(np.array(done)==att)[0])+1
    voa.setAttn(cur_att+att)
    time.sleep(1)
    p_pump = p_in.getPower(2)
    print(p_pump,iteration)
    record_point(path,meas_name=f"Iter{iteration:.0f}_Pump@{p_pump:.2f}dBm_Probe@{p_probe:.2f}dBm",pw_rx=pw_rx,pw_pd=pw_pd,osa_span=osa_span,osa_center=osa_center,osa_pump_center=osa_pump_center,speed=speed,pd=pd,intTime=intTime)
    done.append(att)
 

    
    
    